<?php
require('secure.inc.php');
if(!is_object($thisclient) || !$thisclient->isValid()) die('Access denied'); 
require('client.inc.php');
include('generalconfig.php');

$nav->setActiveNav('dashboard');


require_once(CLIENTINC_DIR.'header.inc.php');
require(CLIENTINC_DIR.'dashboard.inc.php');
require_once(CLIENTINC_DIR.'footer.inc.php');

?>