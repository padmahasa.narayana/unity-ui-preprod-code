<?php
/*************************************************************************
    tickets.php

    Handles all tickets related actions.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/

require('staff.inc.php');
require_once(INCLUDE_DIR.'class.ticket.php');
require_once(INCLUDE_DIR.'class.dept.php');
require_once(INCLUDE_DIR.'class.filter.php');
require_once(INCLUDE_DIR.'class.canned.php');
require_once(INCLUDE_DIR.'class.json.php');
require_once(INCLUDE_DIR.'class.dynamic_forms.php');
require_once(INCLUDE_DIR.'class.export.php');       // For paper sizes

$page='';
$ticket = $user = null; //clean start.
//LOCKDOWN...See if the id provided is actually valid and if the user has access.
if($_REQUEST['id']) {
    if(!($ticket=Ticket::lookup($_REQUEST['id'])))
         $errors['err']=sprintf(__('%s: Unknown or invalid ID.'), __('ticket'));
    elseif(!$ticket->checkStaffAccess($thisstaff)) {
        $errors['err']=__('Access denied. Contact admin if you believe this is in error');
        $ticket=null; //Clear ticket obj.
    }
}

//Lookup user if id is available.
if ($_REQUEST['uid']) {
    $user = User::lookup($_REQUEST['uid']);
}
elseif (!isset($_REQUEST['advsid']) && @$_REQUEST['a'] != 'search'
    && !isset($_REQUEST['status']) && isset($_SESSION['::Q'])
) {
    $_REQUEST['status'] = $_SESSION['::Q'];
}
// Configure form for file uploads
$response_form = new Form(array(
    'attachments' => new FileUploadField(array('id'=>'attach',
        'name'=>'attach:response',
        'configuration' => array('extensions'=>'')))
));
$note_form = new Form(array(
    'attachments' => new FileUploadField(array('id'=>'attach',
        'name'=>'attach:note',
        'configuration' => array('extensions'=>'')))
));

//At this stage we know the access status. we can process the post.
if($_POST && !$errors):

    if($ticket && $ticket->getId()) {
        if($_POST['ticket_transfer_to'] != null)
        {
            $res=$ticket->getTransfer($_POST);
           // header("Location:/scp/tickets.php?".LANGUAGE."=".$_REQUEST[LANGUAGE]);
           // exit();
        }
        else
        {
            //More coffee please.
        $errors=array();
        $lock=$ticket->getLock(); //Ticket lock if any
        switch(strtolower($_POST['a'])):
        case 'reply':
            if(!$thisstaff->canPostReply())
                $errors['err'] = __('Action denied. Contact admin for access');
            else {
                $vars = $_POST;
                $vars['cannedattachments'] = $response_form->getField('attachments')->getClean();
                $vars['response'] = ThreadBody::clean($vars['response']);
                if(!$vars['response'])
                    //$errors['response']=__('Response required');
                    $vars['response']="Staff Responded";
                //Use locks to avoid double replies
                if($lock && $lock->getStaffId()!=$thisstaff->getId())
                    $errors['err']=__('Action Denied. Ticket is locked by someone else!');

                //Make sure the email is not banned
                if(!$errors['err'] && TicketFilter::isBanned($ticket->getEmail()))
                    $errors['err']=__('Email is in banlist. Must be removed to reply.');
            }

            if(!$errors && ($response=$ticket->postReply($vars, $errors, $_POST['emailreply']))) {
                $msg = sprintf(__('%s: Reply posted successfully'),
                        sprintf(__('Ticket #%s'),
                            sprintf('<a href="tickets.php?id=%d"><b>%s</b></a>',
                                $ticket->getId(), $ticket->getNumber()))
                        );

                // Clear attachment list
                $response_form->setSource(array());
                $response_form->getField('attachments')->reset();

                // Remove staff's locks
                TicketLock::removeStaffLocks($thisstaff->getId(),
                        $ticket->getId());

                // Cleanup response draft for this user
                Draft::deleteForNamespace(
                    'ticket.response.' . $ticket->getId(),
                    $thisstaff->getId());

                // Go back to the ticket listing page on reply
                $ticket = null;

            } elseif(!$errors['err']) {
                $errors['err']=__('Unable to post the reply. Correct the errors below and try again!');
            }
            break;
        case 'transfer': /** Transfer ticket **/
            //Check permission
            if(!$thisstaff->canTransferTickets())
                $errors['err']=$errors['transfer'] = __('Action Denied. You are not allowed to transfer tickets.');
            else {

                //Check target dept.
                if(!$_POST['deptId'])
                    $errors['deptId'] = __('Select department');
                elseif($_POST['deptId']==$ticket->getDeptId())
                    $errors['deptId'] = __('Ticket already in the department');
                elseif(!($dept=Dept::lookup($_POST['deptId'])))
                    $errors['deptId'] = __('Unknown or invalid department');

                //Transfer message - required.
                if(!$_POST['transfer_comments'])
                    $errors['transfer_comments'] = __('Transfer comments required');
                elseif(strlen($_POST['transfer_comments'])<5)
                    $errors['transfer_comments'] = __('Transfer comments too short!');

                //If no errors - them attempt the transfer.
                if(!$errors && $ticket->transfer($_POST['deptId'], $_POST['transfer_comments'])) {
                    $msg = sprintf(__('Ticket transferred successfully to %s'),$ticket->getDeptName());
                    //Check to make sure the staff still has access to the ticket
                    if(!$ticket->checkStaffAccess($thisstaff))
                        $ticket=null;

                } elseif(!$errors['transfer']) {
                    $errors['err'] = __('Unable to complete the ticket transfer');
                    $errors['transfer']=__('Correct the error(s) below and try again!');
                }
            }
            break;
        case 'assign':

             if(!$thisstaff->canAssignTickets())
                 $errors['err']=$errors['assign'] = __('Action Denied. You are not allowed to assign/reassign tickets.');
             else {

                 $id = preg_replace("/[^0-9]/", "",$_POST['assignId']);
                 $claim = (is_numeric($_POST['assignId']) && $_POST['assignId']==$thisstaff->getId());
                 $dept = $ticket->getDept();

                 if (!$_POST['assignId'] || !$id)
                     $errors['assignId'] = __('Select assignee');
                 elseif ($_POST['assignId'][0]!='s' && $_POST['assignId'][0]!='t' && !$claim)
                     $errors['assignId']= sprintf('%s - %s',
                             __('Invalid assignee'),
                             __('get technical support'));
                 elseif ($_POST['assignId'][0]=='s'
                         && $dept->assignMembersOnly()
                         && !$dept->isMember($id)) {
                     $errors['assignId'] = sprintf('%s. %s',
                             __('Invalid assignee'),
                             __('Must be department member'));
                 } elseif($ticket->isAssigned()) {
                     if($_POST['assignId'][0]=='s' && $id==$ticket->getStaffId())
                         $errors['assignId']=__('Ticket already assigned to the agent.');
                     elseif($_POST['assignId'][0]=='t' && $id==$ticket->getTeamId())
                         $errors['assignId']=__('Ticket already assigned to the team.');
                 }
              
                 //Comments are not required on self-assignment (claim)
                 if($claim && !$_POST['assign_comments'])
                     $_POST['assign_comments'] = sprintf(__('Ticket claimed by %s'),$thisstaff->getName());
                 elseif(!$_POST['assign_comments'])
                    $_POST['assign_comments']=sprintf(__('Ticket assigned successfully to %s'), Staff::getStaffName($_POST['assignId']));
                /* elseif(strlen($_POST['assign_comments'])<5)
                         $errors['assign_comments'] = __('Comment too short');*/

                 if(!$errors && $ticket->assign($_POST['assignId'], $_POST['assign_comments'], !$claim)) {
                     if($claim) {
                         $msg = __('Ticket is NOW assigned to you!');
                     } else {
                         $msg=sprintf(__('Ticket assigned successfully to %s'), $ticket->getAssigned());
                         TicketLock::removeStaffLocks($thisstaff->getId(), $ticket->getId());
                         $ticket=null;
                     }
                 } elseif(!$errors['assign']) {
                     $errors['err'] = __('Unable to complete the ticket assignment');
                     $errors['assign'] = __('Correct the error(s) below and try again!');
                 }
             }
            break;
        case 'postnote': /* Post Internal Note */
            $vars = $_POST;
            $attachments = $note_form->getField('attachments')->getClean();
            $vars['cannedattachments'] = array_merge(
                $vars['cannedattachments'] ?: array(), $attachments);
            $vars['note'] = ThreadBody::clean($vars['note']);

            $wasOpen = ($ticket->isOpen());
            if(($note=$ticket->postNote($vars, $errors, $thisstaff))) {

                $msg=__('Internal note posted successfully');
                // Clear attachment list
                $note_form->setSource(array());
                $note_form->getField('attachments')->reset();

                if($wasOpen && $ticket->isClosed())
                    $ticket = null; //Going back to main listing.
                else
                    // Ticket is still open -- clear draft for the note
                    Draft::deleteForNamespace('ticket.note.'.$ticket->getId(),
                        $thisstaff->getId());

            } else {

                if(!$errors['err'])
                    $errors['err'] = __('Unable to post internal note - missing or invalid data.');

                $errors['postnote'] = __('Unable to post the note. Correct the error(s) below and try again!');
            }
            break;
        case 'edit':
        case 'update':
            if(!$ticket || !$thisstaff->canEditTickets())
                $errors['err']=__('Permission Denied. You are not allowed to edit tickets');
            elseif($ticket->update($_POST,$errors)) {
                $msg=__('Ticket updated successfully');
                $_REQUEST['a'] = null; //Clear edit action - going back to view.
                //Check to make sure the staff STILL has access post-update (e.g dept change).
                if(!$ticket->checkStaffAccess($thisstaff))
                    $ticket=null;
            } elseif(!$errors['err']) {
                $errors['err']=__('Unable to update the ticket. Correct the errors below and try again!');
            }
            break;
        case 'process':
            switch(strtolower($_POST['do'])):
                case 'release':
                    if(!$ticket->isAssigned() || !($assigned=$ticket->getAssigned())) {
                        $errors['err'] = __('Ticket is not assigned!');
                    } elseif($ticket->release()) {
                        $msg=sprintf(__(
                            /* 1$ is the current assignee, 2$ is the agent removing the assignment */
                            'Ticket released (unassigned) from %1$s by %2$s'),
                            $assigned, $thisstaff->getName());
                        $ticket->logActivity(__('Ticket unassigned'),$msg);
                    } else {
                        $errors['err'] = __('Problems releasing the ticket. Try again');
                    }
                    break;
                case 'claim':
                    if(!$thisstaff->canAssignTickets()) {
                        $errors['err'] = __('Permission Denied. You are not allowed to assign/claim tickets.');
                    } elseif(!$ticket->isOpen()) {
                        $errors['err'] = __('Only open tickets can be assigned');
                    } elseif($ticket->isAssigned()) {
                        $errors['err'] = sprintf(__('Ticket is already assigned to %s'),$ticket->getAssigned());
                    } elseif ($ticket->claim()) {
                        $msg = __('Ticket is now assigned to you!');
                    } else {
                        $errors['err'] = __('Problems assigning the ticket. Try again');
                    }
                    break;
                case 'overdue':
                    $dept = $ticket->getDept();
                    if(!$dept || !$dept->isManager($thisstaff)) {
                        $errors['err']=__('Permission Denied. You are not allowed to flag tickets overdue');
                    } elseif($ticket->markOverdue()) {
                        $msg=sprintf(__('Ticket flagged as overdue by %s'),$thisstaff->getName());
                        $ticket->logActivity(__('Ticket Marked Overdue'),$msg);
                    } else {
                        $errors['err']=__('Problems marking the the ticket overdue. Try again');
                    }
                    break;
                case 'answered':
                    $dept = $ticket->getDept();
                    if(!$dept || !$dept->isManager($thisstaff)) {
                        $errors['err']=__('Permission Denied. You are not allowed to flag tickets');
                    } elseif($ticket->markAnswered()) {
                        $msg=sprintf(__('Ticket flagged as answered by %s'),$thisstaff->getName());
                        $ticket->logActivity(__('Ticket Marked Answered'),$msg);
                    } else {
                        $errors['err']=__('Problems marking the the ticket answered. Try again');
                    }
                    break;
                case 'unanswered':
                    $dept = $ticket->getDept();
                    if(!$dept || !$dept->isManager($thisstaff)) {
                        $errors['err']=__('Permission Denied. You are not allowed to flag tickets');
                    } elseif($ticket->markUnAnswered()) {
                        $msg=sprintf(__('Ticket flagged as unanswered by %s'),$thisstaff->getName());
                        $ticket->logActivity(__('Ticket Marked Unanswered'),$msg);
                    } else {
                        $errors['err']=__('Problems marking the ticket unanswered. Try again');
                    }
                    break;
                case 'banemail':
                    if(!$thisstaff->canBanEmails()) {
                        $errors['err']=__('Permission Denied. You are not allowed to ban emails');
                    } elseif(BanList::includes($ticket->getEmail())) {
                        $errors['err']=__('Email already in banlist');
                    } elseif(Banlist::add($ticket->getEmail(),$thisstaff->getName())) {
                        $msg=sprintf(__('Email %s added to banlist'),$ticket->getEmail());
                    } else {
                        $errors['err']=__('Unable to add the email to banlist');
                    }
                    break;
                case 'unbanemail':
                    if(!$thisstaff->canBanEmails()) {
                        $errors['err'] = __('Permission Denied. You are not allowed to remove emails from banlist.');
                    } elseif(Banlist::remove($ticket->getEmail())) {
                        $msg = __('Email removed from banlist');
                    } elseif(!BanList::includes($ticket->getEmail())) {
                        $warn = __('Email is not in the banlist');
                    } else {
                        $errors['err']=__('Unable to remove the email from banlist. Try again.');
                    }
                    break;
                case 'changeuser':
                    if (!$thisstaff->canEditTickets()) {
                        $errors['err']=__('Permission Denied. You are not allowed to edit tickets');
                    } elseif (!$_POST['user_id'] || !($user=User::lookup($_POST['user_id']))) {
                        $errors['err'] = __('Unknown user selected');
                    } elseif ($ticket->changeOwner($user)) {
                        $msg = sprintf(__('Ticket ownership changed to %s'),
                            Format::htmlchars($user->getName()));
                    } else {
                        $errors['err'] = __('Unable to change ticket ownership. Try again');
                    }
                    break;
                default:
                    $errors['err']=__('You must select action to perform');
            endswitch;
            break;
            case 'manualcheck':
                    $ticket->insertManualCheck($_POST);
                    if($_POST['reassign']!="Yes")
                        $msg = "Manual Check is submitted successfully";
                    else
                        $msg = "Ticket Reassigned successfully";
                    break;
            case 'manualticket':
                    $vars = $_POST;
                    $vars['cannedattachments'] = $response_form->getField('attachments')->getClean();
                    $ticket->insertManualTicketCheck($vars);
                        // Clear attachment list
                    $response_form->setSource(array());
                    $response_form->getField('attachments')->reset();

                    if($_POST['reassign']!="Yes")
                        $msg = "Manual Check is submitted successfully";
                    else
                        $msg = "Ticket Reassigned successfully";
                    break;
            
        default:
            $errors['err']=__('Unknown action');
        endswitch;
        if($ticket && is_object($ticket))
            $ticket->reload();//Reload ticket info following post processing
        }
        
    }elseif($_POST['a']) {

        switch($_POST['a']) {
            case 'open':
                $ticket=null;
                if(!$thisstaff || !$thisstaff->canCreateTickets()) {
                     $errors['err'] = sprintf('%s %s',
                             sprintf(__('You do not have permission %s.'),
                                 __('to create tickets')),
                             __('Contact admin for such access'));
                } else {
                    $vars = $_POST;
                    $vars['uid'] = $user? $user->getId() : 0;

                    $vars['cannedattachments'] = $response_form->getField('attachments')->getClean();

                    if(($ticket=Ticket::open($vars, $errors))) {
                        $msg=__('Ticket created successfully');
                        $_REQUEST['a']=null;
                        if (!$ticket->checkStaffAccess($thisstaff) || $ticket->isClosed())
                            $ticket=null;
                        Draft::deleteForNamespace('ticket.staff%', $thisstaff->getId());
                        // Drop files from the response attachments widget
                        $response_form->setSource(array());
                        $response_form->getField('attachments')->reset();
                        unset($_SESSION[':form-data']);
                    } elseif(!$errors['err']) {
                        $errors['err']=__('Unable to create the ticket. Correct the error(s) and try again');
                    }
                }
                break;
        }
    }
    if(!$errors)
        $thisstaff ->resetStats(); //We'll need to reflect any changes just made!
endif;

/*... Quick stats ...*/
$stats= $thisstaff->getTicketsStats();
$url_string="";
//Navigation
if ((isset($_REQUEST[LANGUAGE]))&&($_REQUEST[LANGUAGE])) {
    $language =  $_REQUEST[LANGUAGE];
    $url_string=LANGUAGE."=".$language;
} else {
    $language = ENGLISH;
    $url_string=LANGUAGE."=".$language;
}

//remod Navigation
if ((isset($_REQUEST[REMOD]))&&($_REQUEST[REMOD]==1)) {
    $remod =  "rm=1";
    $url_string=$remod;
    $language="RM";
} else {
    $remod = "";
    $url_string=LANGUAGE."=".$language;
}
//end
if (!isset($_REQUEST[REMOD])){
    $nav_tab = $language.'tickets';
}
else
{
    $nav_tab = REMOD.'tickets'; 
}

$nav->setTabActive($nav_tab);
            
$open_name = _P('queue-name',
    /* This is the name of the open ticket queue */
    'Open-Q');
if($cfg->showAnsweredTickets()) {
    $nav->addSubMenu(array('desc'=>$open_name.' ('.number_format($stats['open']+$stats['answered']).')',
                            'title'=>__('Open-Q-').$language,
                            'href'=>'tickets.php?status=open&'.$url_string,
                            'iconclass'=>'Ticket'),
                        (!$_REQUEST['status'] || $_REQUEST['status']=='open'));
} else {

    if ($stats) {

        $nav->addSubMenu(array('desc'=>$open_name.' ('.number_format($stats['open']).')',
                               'title'=>__('Open-Q-').$language,
                               'href'=>'tickets.php?status=open&'.$url_string,
                               'iconclass'=>'Ticket'),
                            (!$_REQUEST['status'] || $_REQUEST['status']=='open'));
    }

    if($stats['answered']) {
        $nav->addSubMenu(array('desc'=>__('Query-Q').' ('.number_format($stats['answered']).')',
                               'title'=>__('Query-Q-').$language,
                               'href'=>'tickets.php?status=answered&'.$url_string,
                               'iconclass'=>'answeredTickets'),
                            ($_REQUEST['status']=='answered'));
    }
}

if($stats['assigned']) {

    $nav->addSubMenu(array('desc'=>__('My-Q').' ('.number_format($stats['assigned']).')',
                           'title'=>__('Assigned-Q-').$language,
                           'href'=>'tickets.php?status=assigned&'.$url_string,
                           'iconclass'=>'assignedTickets'),
                        ($_REQUEST['status']=='assigned'));
}

/*if($stats['overdue']) {
    $nav->addSubMenu(array('desc'=>__('Overdue').' ('.number_format($stats['overdue']).')',
                           'title'=>__('Stale Tickets'),
                           'href'=>'tickets.php?status=overdue',
                           'iconclass'=>'overdueTickets'),
                        ($_REQUEST['status']=='overdue'));

    if(!$sysnotice && $stats['overdue']>10)
        $sysnotice=sprintf(__('%d overdue tickets!'),$stats['overdue']);
}*/

if($stats['allassigned']) {
    $nav->addSubMenu(array('desc'=>__('Assigned-Q').' ('.number_format($stats['allassigned']).')',
                           'title'=>__('Assigned-Q-').$language,
                           'href'=>'tickets.php?status=allassigned&'.$url_string,
                           'iconclass'=>'teams'),
                        ($_REQUEST['status']=='allassigned'));

    if(!$sysnotice && $stats['overdue']>10)
        $sysnotice=sprintf(__('%d overdue tickets!'),$stats['overdue']);
}
if($stats['resolved']) {
    $nav->addSubMenu(array('desc'=>__('Resolved').' ('.number_format($stats['resolved']).')',
                           'title'=>__('Resolved-').$language,
                           'href'=>'tickets.php?status=resolved&'.$url_string,
                           'iconclass'=>'answeredTickets'),
                        ($_REQUEST['status']=='resolved'));
}
if($thisstaff->showAssignedOnly() && $stats['closed']) {
    $nav->addSubMenu(array('desc'=>__('My Closed Tickets').' ('.number_format($stats['closed']).')',
                           'title'=>__('My Closed Tickets-').$language,
                           'href'=>'tickets.php?status=closed&'.$url_string,
                           'iconclass'=>'closedTickets'),
                        ($_REQUEST['status']=='closed'));
} else {

    $nav->addSubMenu(array('desc' => __('Closed-Q').' ('.number_format($stats['closed']).')',
                           'title'=>__('Closed-Q-').$language,
                           'href'=>'tickets.php?status=closed&'.$url_string,
                           'iconclass'=>'closedTickets'),
                        ($_REQUEST['status']=='closed'));
}
if($stats['manual']) {
    $nav->addSubMenu(array('desc'=>__('Manual-Q').' ('.number_format($stats['manual']).')',
                           'title'=>__('Manual-Q-').$language,
                           'href'=>'tickets.php?status=manual&'.$url_string,
                           'iconclass'=>'teams'),
                        ($_REQUEST['status']=='manual'));

    if(!$sysnotice && $stats['overdue']>10)
        $sysnotice=sprintf(__('%d overdue tickets!'),$stats['overdue']);
}

if ($stats) {

        $nav->addSubMenu(array('desc'=>'Failed-Q'.' ('.number_format($stats['failed']).')',
                               'title'=>__('Failed-Q-').$language,
                               'href'=>'tickets.php?status=failed&'.$url_string,
                               'iconclass'=>'Ticket'),
                            (!$_REQUEST['status'] || $_REQUEST['status']=='failed'));
    }
    //added by rajneesh for app check tab
    if (($_REQUEST[LANGUAGE]==ENGLISH)||($_REQUEST[LANGUAGE]=='')) {
        $nav->addSubMenu(array('desc'=>__('App-Q').' ('.number_format($stats['app_check']).')',
                               'title'=>__('App-Q-').$language,
                               'href'=>'tickets.php?status=app_check&'.$url_string,
                               'iconclass'=>'Ticket'),
                            ($_REQUEST['status']=='app_check'));

        if(!$sysnotice && $stats['overdue']>10)
            $sysnotice=sprintf(__('%d overdue tickets!'),$stats['overdue']);
    }
//end
//added by rajneesh for video check tab
if (($_REQUEST[LANGUAGE]==ENGLISH)||($_REQUEST[LANGUAGE]=='')) {
    $nav->addSubMenu(array('desc'=>__('Video-Q').' ('.number_format($stats['video_check']).')',
                           'title'=>__('Video-Q-').$language,
                           'href'=>'tickets.php?status=video_check&'.$url_string,
                           'iconclass'=>'Ticket'),
                        ($_REQUEST['status']=='video_check'));

    if(!$sysnotice && $stats['overdue']>10)
        $sysnotice=sprintf(__('%d overdue tickets!'),$stats['overdue']);
}
//end
/*if($thisstaff->canCreateTickets()) {
    $nav->addSubMenu(array('desc'=>__('New Ticket'),
                           'title'=> __('Open a New Ticket'),
                           'href'=>'tickets.php?a=open',
                           'iconclass'=>'newTicket',
                           'id' => 'new-ticket'),
                        ($_REQUEST['a']=='open'));
}*/


$ost->addExtraHeader('<script type="text/javascript" src="js/ticket.js?70898b3"></script>');
$ost->addExtraHeader('<meta name="tip-namespace" content="tickets.queue" />',
    "$('#content').data('tipNamespace', 'tickets.queue');");

$inc = 'tickets.inc.php';
if($ticket) {
    $ost->setPageTitle(sprintf(__('Ticket #%s'),$ticket->getNumber()));
    $nav->setActiveSubMenu(-1);
    $inc = 'ticket-view.inc.php';
    if($_REQUEST['a']=='edit' && $thisstaff->canEditTickets()) {
        $inc = 'ticket-edit.inc.php';
        if (!$forms) $forms=DynamicFormEntry::forTicket($ticket->getId());
        // Auto add new fields to the entries
        foreach ($forms as $f) $f->addMissingFields();
    } elseif($_REQUEST['a'] == 'print' && !$ticket->pdfExport($_REQUEST['psize'], $_REQUEST['notes']))
        $errors['err'] = __('Internal error: Unable to export the ticket to PDF for print.');
} else {
    $inc = 'tickets.inc.php';
    if($_REQUEST['a']=='open' && $thisstaff->canCreateTickets())
        $inc = 'ticket-open.inc.php';
    elseif($_REQUEST['a'] == 'export') {
        $ts = strftime('%Y%m%d');
        if (!($token=$_REQUEST['h']))
            $errors['err'] = __('Query token required');
        elseif (!($query=$_SESSION['search_'.$token]))
            $errors['err'] = __('Query token not found');
        elseif (!Export::saveTickets($query, "tickets-$ts.csv", 'csv'))
            $errors['err'] = __('Internal error: Unable to dump query results');
    }

    //Clear active submenu on search with no status
    if($_REQUEST['a']=='search' && !$_REQUEST['status'])
        $nav->setActiveSubMenu(-1);

    //set refresh rate if the user has it configured
    if(!$_POST && !$_REQUEST['a'] && ($min=$thisstaff->getRefreshRate())) {
        $js = "clearTimeout(window.ticket_refresh);
               window.ticket_refresh = setTimeout($.refreshTicketView,"
            .($min*60000).");";
        $ost->addExtraHeader('<script type="text/javascript">'.$js.'</script>',
            $js);
    }
}

require_once(STAFFINC_DIR.'header.inc.php');
require_once(STAFFINC_DIR.$inc);
print $response_form->getMedia();
require_once(STAFFINC_DIR.'footer.inc.php');
