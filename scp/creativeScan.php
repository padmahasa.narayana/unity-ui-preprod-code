<?php
/**
* Class to Re-scan Creatives for Alerts
*/

class CreativeScan
{   
    var $servername;
    var $username;
    var $password;
    var $dbname;
    var $creativeid;

    function __construct()
    {
        $this->servername = "api3db.ctzwque3zyis.us-east-1.rds.amazonaws.com";
        $this->username = "apiproduser";
        $this->password = "#api4db135!";
        $this->dbname = "NewCreativeQA_structure_new";
        $this->creativeid = $_GET['creativeid'];
    }

    function connection()
    {
        // Create connection
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        return $conn; 
    }

    function getProjectByCreativeid()
    {
        $connection = $this->connection();
        $sql = "SELECT creative_project_id from creative_scan_response where creative_id='".$this->creativeid."'";
        $result = $connection->query($sql);
        $row = $result->fetch_assoc();  
        return $row['creative_project_id']; 
    }

    function deleteProjectInGeoedge()
    {        
        $creativeProjectId = $this->getProjectByCreativeid();

        if ($creativeProjectId != null) 
        {
            // THIS IS YOUR REAL API-KEY, YOU DON'T HAVE TO CHANGE ANYTHING HERE
            define('API_KEY', '7d60699f7b6e8c08c2176624f88e19ef');
            define('API_URL', 'https://api.geoedge.com/rest/analytics/v3');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '. API_KEY));
            curl_setopt($ch, CURLOPT_URL, API_URL . '/projects/'.$creativeProjectId);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $curlExe = curl_exec($ch);
            return $curlExe;
        }
    }

    function deleteExistingScanRecords()
    {
        $connection = $this->connection();
        $csr = "DELETE FROM creative_scan_response where creative_id='".$this->creativeid."'";
        $spn = "DELETE FROM scan_pixel_notification where creative_id='".$this->creativeid."'";
        $connection->query($csr);
        $connection->query($spn);      
    }

    function updateStatusToCreative()
    {
        $connection = $this->connection();
        $csr = "UPDATE creative_tag_details SET status=0 where creative_id='".$this->creativeid."'";
        $connection->query($csr);
    }
}

//create object of creative scan
$creativeScanObj = new CreativeScan();

//delete project from GeoEdge
$creativeScanObj->deleteProjectInGeoedge();

//delete existing scan records
$creativeScanObj->deleteExistingScanRecords();

//update status of an creative
$creativeScanObj->updateStatusToCreative();

$response = array('Msg' => 'Scanning is In-Progress');
echo json_encode($response); exit;
?>
