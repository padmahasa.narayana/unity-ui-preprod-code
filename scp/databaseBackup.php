<?php
include('/var/www/html/phpmailer/class.phpmailer.php');

/**
* 1. Run a Scheduler weekly once, to check if database contains 50k tickets.
  2.  If database contains 75k tickets, then take backup of existing database with timestamp attached to its name and save it in a backup folder.
  3.  Cleanup the existing database by removing only closed tickets data of one month old.
**/

class databaseBackup
{   
    var $servername;
    var $username;
    var $password;
    var $dbname;
    var $creativeid;
	var $tcBeforeBackup;
	var $tcAfterBackup;

    /* Constructor to initialize database variables */
	function __construct()
    {
        $this->apiaccesscheck();
		$this->servername = "api2db.ctzwque3zyis.us-east-1.rds.amazonaws.com";
		$this->username = "apiproduser";
		$this->password = "#api4db135!";
		$this->dbname = "NewCreativeQA_structure_new";
	}
	
	/* API access check */
	function apiaccesscheck()
	{
		$day = date('w'); 
		$hour  = date("H");		
		if ( ($day != 6) && ($hour != 23))
		{
			$response = array('Msg' => 'Authentication Error');
            echo json_encode($response); exit;
		}
		
	}

    /* Function to connect to a database */
    function connection()
    {
        // Create connection
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $conn->set_charset("utf8");
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        return $conn; 
    }

    /* function to get total closed tickets count */
    function getClosedTicketCount()
    {
        $connection = $this->connection();
        $sql = "SELECT  COUNT( ticket.ticket_id ) AS tickets FROM ost_ticket ticket 
                INNER JOIN ost_ticket_status status ON (ticket.status_id=status.id AND status.state='closed' ) 
                WHERE 1 AND ( (ticket.staff_id=1 AND status.state='open') OR ticket.dept_id IN(1,2,3) )";
        $result = $connection->query($sql);
        $row = $result->fetch_assoc();

		return $row;		       
    }

    /*
     *  command to take db backup
    */
    public function runback() 
	{
        $filename = '/var/www/html/scp/database_backup/unity_database_backup_' . date("Y-m-d-H-i-s") . '.sql';
        $command = "mysqldump --single-transaction -h $this->servername -u$this->username -p$this->password  $this->dbname > $filename";
        system($command);
        if ($command == '') {
            /* no output is good */
            return 0;
        } else {
           /* we have something to log the output here */
            return 1;
        }
    }

	/* Delete Data of One Month old in database */
    function clearDatabase()
    {
        $connection = $this->connection();
        $connection->query("CALL clear_database()");
    }
    
	/* function to take current database backup */
    function getCurrentDatabaseBackup()
    {      
		$ticketCount = $this->getClosedTicketCount(); // Get current closed tickets count
        if ($ticketCount['tickets'] >= 50000) //check the condition for closed tickets count
        {
            $this->tcBeforeBackup = $ticketCount['tickets'];
			$status = $this->runback(); //take backup of current database in to /scp/database_backup folder
            if ($status_id = 1) 
            {
                $response = array('Msg' => 'Backup is taken successfully');
				$this->clearDatabase(); //delete tickets of one month old
				$ticketCount = $this->getClosedTicketCount(); // Get current closed tickets count
				$this->tcAfterBackup = $ticketCount['tickets'];
				$this->sendMail(); //send mails when tickets are closed
                echo json_encode($response); exit;
            }
            else
            {
                $response = array('Msg' => 'There was an Error Occured');
                echo json_encode($response); exit;
            }
        } 
        else
        {
            $response = array('Msg' => 'Closed Tickets have not Crossed 50k, The current closed tickets count is '.$ticketCount['tickets']);
            echo json_encode($response); exit;
        }
    }

	/* Send Emails to Slack on Ticket close */
   function sendMail()
   {
		$ticketCount = $this->getClosedTicketCount();
			
		//require_once('class.phpmailer.php');
		$mail = new PHPMailer(true);
		$mail->IsSMTP();                       // telling the class to use SMTP
		$mail->SMTPDebug = 0;                  

		// 0 = no output, 1 = errors and messages, 2 = messages only.
		$mail->SMTPSecure = "tls";              // sets the prefix to the servier
		$mail->Host = "bsf01.theoreminc.net";        // sets Gmail as the SMTP server
		$mail->Port = 25;                     // set the SMTP port for the GMAIL 

		$mail->Username = "creatives.info";  //  username
		$mail->Password = "theorem_123";      //  password

		$mail->From = 'creatives.info@theoreminc.net';
		
		if ($this->tcBeforeBackup == $this->tcAfterBackup)
		{
			$mail->Subject = "Unity Backup is Taken but No old Tickets to Delete!!!";
		}
		else
		{			
			$mail->Subject = "Unity  Backup is Taken and Cleanup is Done!!!";
		}
		
		$mail->ContentType = 'text/plain'; 
		$mail->IsHTML(false);

		$mail->Body = "The Current Closed Tickets in database is : ".$ticketCount['tickets'];
		// you may also use $mail->Body = file_get_contents('your_mail_template.html');

		$mail->AddAddress ('manjunatha.lakshminarayan@theoreminc.net', 'Manju');    
		$error_message = "";
		// you may also use this format $mail->AddAddress ($recipient);
		if(!$mail->Send()) 
		{
			$error_message = "Mailer Error: " . $mail->ErrorInfo;
		} else {
			$error_message = "Successfully sent!";
		}
   }	
}

//create object of creative scan
$databaseBackupObj = new databaseBackup();

$databaseBackupObj->getCurrentDatabaseBackup();


?>
