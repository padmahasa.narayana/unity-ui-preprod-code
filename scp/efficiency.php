<?php
require_once('../main.inc.php');
require('staff.inc.php');

$nav->setTabActive('reports');
require(STAFFINC_DIR.'header.inc.php');
if($thisstaff->isAdmin())
	require(STAFFINC_DIR.'efficiency.inc.php');
else
	require(STAFFINC_DIR.'reports.inc.php');
include(STAFFINC_DIR.'footer.inc.php');

?>