<?php
// include DB connection
//include('generalconfig.php');
include('unity_dashboard_reporting.php'); 
// check start and end date in post request
if($_POST && isset($_POST['start']) && isset($_POST['end'])){
	$start = $_POST['start'];
	$end = $_POST['end'];
	// sql statement to retrive input and output data
	$sql = "SELECT input.input_count input, 
					output.output_count output, 
					input.creative_submit_date
			FROM (
					SELECT 
						CASE 
							WHEN TIME(creative_submit_date) BETWEEN '00:00:00' AND '13:59:59' 
								THEN DATE_ADD(DATE(creative_submit_date),INTERVAL -1 DAY) 
							ELSE DATE(creative_submit_date) 
						END AS creative_submit_date, COUNT(creative_id) AS input_count 
					FROM creative_tag_details
					WHERE DATE(creative_submit_date) BETWEEN '$start' AND DATE_ADD('$end',INTERVAL 1 DAY)
					GROUP BY 
						CASE 
							WHEN TIME(creative_submit_date) BETWEEN '00:00:00' AND '13:59:59' 
								THEN DATE_ADD(DATE(creative_submit_date),INTERVAL -1 DAY) 
							ELSE DATE(creative_submit_date) 
						END
				) AS input
			LEFT JOIN (
					SELECT 
						CASE 
							WHEN TIME(response_date) BETWEEN '00:00:00' AND '13:59:59' 
								THEN DATE_ADD(DATE(response_date),INTERVAL -1 DAY) 
							ELSE DATE(response_date) 
						END AS response_date, COUNT(creative_id) AS output_count 
					FROM creative_scan_decision
					WHERE STATUS='sent' AND DATE(response_date) BETWEEN '$start' AND DATE_ADD('$end',INTERVAL 1 DAY)
					GROUP BY 
						CASE 
							WHEN TIME(response_date) BETWEEN '00:00:00' AND '13:59:59' 
								THEN DATE_ADD(DATE(response_date),INTERVAL -1 DAY) 
							ELSE DATE(response_date) 
						END
					) AS output
			ON CAST(input.creative_submit_date AS DATE) = CAST(output.response_date AS DATE)";
	// execute query
	$result=$conn->query($sql);
	$response_result = array();
	// get data from result query and return response
	if ($result->num_rows > 0) {
		$response_result = array();
		$count = 0;
	    // get data from each row
	    while($input_row = $result->fetch_assoc()) {
	    	if ($count==0) {
	    		$count++;
	    		continue;
	    	}
	    	$count++;
	    	if ($count==$result->num_rows) {
	    		/*if (date_create($end)==date('Y-m-d',strtotime("-1 days"))) {
	    			//continue;
	    		}else{*/
	    			continue;
	    		//}	
	    	}
	    	$data['input'] = $input_row["input"];
	    	$data['output'] = $input_row["output"];
	    	$data['created_at'] = $input_row["creative_submit_date"];
	    	$response_result[] = $data;  
	    }
	    echo json_encode($response_result);
	} else {
		// return empty data if data is not available in DB
		$data['input'] = 0;
		$data['output'] = 0;
		$data['created_at'] = "No data";
		$response_result[] = $data;
		echo json_encode($response_result);
	}
	mysqli_close($conn);
}

?>
