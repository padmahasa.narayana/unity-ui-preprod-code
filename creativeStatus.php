<?php 
// include DB connection
//include('generalconfig.php');
include('unity_dashboard_reporting.php');
// check start and end date in post request
if($_POST && isset($_POST['status'])){
	$status = $_POST['status'];
	// sql statement to retrive creative status data
	switch ($status) {
		case '1':
			$sql = "SELECT cdata.ticket_id, cdata.subject, ticket.created, 'Open Queue' AS status
					FROM ost_ticket__cdata as cdata
				    join ost_ticket as ticket
				    on ticket.ticket_id=cdata.ticket_id
					WHERE cdata.manual_submission=1
				    AND (ticket.staff_id=0 OR ticket.staff_id IS NULL) 
				    AND ticket.status_id=1
				    order by ticket.created desc
				    limit 300";
			break;
		
		case '2':
			$sql = "SELECT cdata.ticket_id, cdata.subject, ticket.closed, 'Closed' AS status FROM ost_ticket__cdata as cdata join ost_ticket as ticket on ticket.ticket_id=cdata.ticket_id WHERE cdata.manual_submission=1 AND (ticket.staff_id!=0 OR ticket.staff_id IS NOT NULL) AND ticket.status_id=3 order by ticket.closed desc limit 300";
			break;

		case '3':
			$sql = "SELECT cdata.ticket_id, cdata.subject, ticket.assigned_date, 'Work in Progress' AS status
					FROM ost_ticket__cdata as cdata
				    join ost_ticket as ticket
				    on ticket.ticket_id=cdata.ticket_id
					WHERE cdata.manual_submission=1
				    AND (ticket.staff_id!=0 OR ticket.staff_id IS NOT NULL) 
				    AND ticket.status_id=1
				    order by ticket.assigned_date desc
				    limit 300";
			break;
	}

	// execute query

	$result=$conn->query($sql);
	$response_result = array();
	// get data from result query and return response
	if ($result->num_rows > 0) {
		$response_result = array();
	    // get data from each row
	    while($input_row = $result->fetch_assoc()) {
	    	$data['ticket_id'] = $input_row["ticket_id"];
	    	$data['creative_id'] = $input_row["subject"];
	    	$data['status'] = $input_row["status"];
	    	$response_result[] = $data;  
	    }
	    //echo $response_result;
	    echo json_encode($response_result);
	} else {
		$data['ticket_id'] = 0;
		$data['creative_id'] = "No data";
	    $data['status'] = "Open Queue";
	    $response_result[] = $data;
	    echo json_encode($response_result);
	}
	mysqli_close($conn);
}
?>
