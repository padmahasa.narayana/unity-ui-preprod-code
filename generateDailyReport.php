<?php
// include DB connection
include('report_prod_gereneralconfig.php');
// check start and end date in post request
$table_name = 'tmp_table_'.date('FY').'_reports_by_lang';
//$table_name = 'temp_table_'.date('FY').'_report';
$sql = "select 1 from $table_name";
$result=$conn->query($sql);
if($result == FALSE)
{
    $sql = "CREATE TABLE $table_name (
    `seq_id` int(11) NOT NULL AUTO_INCREMENT,
    `Dates` DATE,
    `Language` varchar(10) DEFAULT NULL,
    `Input` int(11) NOT NULL,
    `Output` int(11) DEFAULT NULL,
    `No_Go` int(11) DEFAULT NULL,
    `Duplicate` int(11) DEFAULT NULL,
    `Failed` int(11) DEFAULT NULL,
    `Missed` int(11) DEFAULT NULL,
    PRIMARY KEY (`seq_id`)
    )";

    if ($conn->query($sql) === TRUE) {
        echo "Table $table_name created successfully";
    } else {
        echo "Error creating table: " . $conn->error;
    }
}

$today = date('Y-m-d',strtotime("-1 days"));
#$today = date('Y-m-05',strtotime("-1 days"));
$filename = "daily_reports_".date('d_m_Y',strtotime("-1 days"))."_by_languages.xls";

$select_query = "SELECT 
    input.created_date AS DATE,
    input.language,
    input.input_count input,
    COALESCE(output.output_count, 0) output,
    COALESCE(rejection.rejection_count, 0) no_go,
    COALESCE(duplicate_data.duplicate_count, 0) duplicate_count,
    COALESCE(failed.failed_ticket_count, 0) failed,
    (COALESCE(duplicate_data.duplicate_count, 0) + COALESCE(failed.failed_ticket_count, 0)) missed
FROM
    (SELECT 
        CASE
                WHEN TIME(creative_submit_date) BETWEEN '00:00:00' AND '13:59:59' THEN DATE_ADD(DATE(creative_submit_date), INTERVAL - 1 DAY)
                ELSE DATE(creative_submit_date)
            END AS created_date,
            COUNT(creative_id) AS input_count,
            LANGUAGE
    FROM
        creative_tag_details
    WHERE
        DATE(creative_submit_date) BETWEEN '$today' AND DATE_ADD('$today', INTERVAL 1 DAY)
    GROUP BY CASE
        WHEN TIME(creative_submit_date) BETWEEN '00:00:00' AND '13:59:59' THEN DATE_ADD(DATE(creative_submit_date), INTERVAL - 1 DAY)
        ELSE DATE(creative_submit_date)
    END , LANGUAGE) AS input
        LEFT JOIN
    (SELECT 
        CASE
                WHEN TIME(csd.response_date) BETWEEN '00:00:00' AND '13:59:59' THEN DATE_ADD(DATE(csd.response_date), INTERVAL - 1 DAY)
                ELSE DATE(csd.response_date)
            END AS created_date,
            COUNT(csd.creative_id) AS output_count,
            ctd.LANGUAGE
    FROM
        creative_scan_decision AS csd
    INNER JOIN creative_tag_details AS ctd ON csd.creative_id = ctd.creative_id 
    WHERE
        csd.STATUS = 'sent'
            AND DATE(csd.response_date) BETWEEN '$today' AND DATE_ADD('$today', INTERVAL 1 DAY)
    GROUP BY CASE
        WHEN TIME(csd.response_date) BETWEEN '00:00:00' AND '13:59:59' THEN DATE_ADD(DATE(csd.response_date), INTERVAL - 1 DAY)
        ELSE DATE(csd.response_date)
    END,ctd.LANGUAGE) AS output ON input.created_date = output.created_date AND input.language=output.language
        LEFT JOIN
    (SELECT 
        CASE
                WHEN TIME(csd.response_date) BETWEEN '00:00:00' AND '13:59:59' THEN DATE_ADD(DATE(csd.response_date), INTERVAL - 1 DAY)
                ELSE DATE(csd.response_date)
            END AS created_date,
            COUNT(1) AS rejection_count, ctd.language
    FROM
        creative_scan_decision AS csd
    INNER JOIN creative_tag_details AS ctd ON csd.creative_id = ctd.creative_id 
    WHERE
        csd.STATUS = 'sent'
            AND csd.scan_decision = 'No-Go'
            AND DATE(csd.response_date) BETWEEN '$today' AND DATE_ADD('$today', INTERVAL 1 DAY)
    GROUP BY CASE
        WHEN TIME(csd.response_date) BETWEEN '00:00:00' AND '13:59:59' THEN DATE_ADD(DATE(csd.response_date), INTERVAL - 1 DAY)
        ELSE DATE(csd.response_date)
    END,ctd.LANGUAGE) AS rejection ON input.created_date = rejection.created_date AND input.language=rejection.language
        LEFT JOIN
    (SELECT 
        CASE
                WHEN TIME(creative_submit_date) BETWEEN '00:00:00' AND '13:59:59' THEN DATE_ADD(DATE(creative_submit_date), INTERVAL - 1 DAY)
                ELSE DATE(creative_submit_date)
            END AS created_date,
            COUNT(1) AS duplicate_count,LANGUAGE
    FROM
        creative_tag_details
    WHERE
        fail_code = 'Invalid value for field \'name\' - Project name already used'
            AND DATE(creative_submit_date) BETWEEN '$today' AND DATE_ADD('$today', INTERVAL 1 DAY)
    GROUP BY CASE
        WHEN TIME(creative_submit_date) BETWEEN '00:00:00' AND '13:59:59' THEN DATE_ADD(DATE(creative_submit_date), INTERVAL - 1 DAY)
        ELSE DATE(creative_submit_date)
    END,LANGUAGE) AS duplicate_data ON input.created_date = duplicate_data.created_date AND input.language=duplicate_data.language
        LEFT JOIN
    (SELECT 
        CASE
                WHEN TIME(created) BETWEEN '00:00:00' AND '13:59:59' THEN DATE_ADD(DATE(created), INTERVAL - 1 DAY)
                ELSE DATE(created)
            END AS created_date,
            COUNT(1) AS failed_ticket_count,LANGUAGE
    FROM
        ost_ticket
    WHERE
        status_id = 6
            AND DATE(created) BETWEEN '$today' AND DATE_ADD('$today', INTERVAL 1 DAY)
    GROUP BY CASE
        WHEN TIME(created) BETWEEN '00:00:00' AND '13:59:59' THEN DATE_ADD(DATE(created), INTERVAL - 1 DAY)
        ELSE DATE(created)
    END,LANGUAGE) AS failed ON input.created_date = failed.created_date AND input.language=failed.language";

$result=$conn->query($select_query);
// get data from result query and return response
if ($result->num_rows > 0) {
    $response_result = array();
    // get data from each row
    while($input_row = $result->fetch_assoc()) {
        if ($input_row["DATE"]==$today) { 
            $created_date = $input_row["DATE"];
            $language = $input_row["language"];
            $input = $input_row["input"];
            $output = $input_row["output"];
            $no_go = $input_row["no_go"];
            $duplicate_count = $input_row["duplicate_count"];
            $failed = $input_row["failed"];
            $missed = $input_row["missed"];

            if($input){
                if ($language) {
                    $check_data = "SELECT * FROM $table_name WHERE Dates = '$today' AND Language = '$language'";
                    $check_data_result=$conn->query($check_data);
                    // get data from result query and return response
                    if ($check_data_result->num_rows == 0) {
                        $sql = "INSERT INTO $table_name (Dates, Language, Input, Output, No_Go, Duplicate, Failed, Missed)
                        VALUES ('$created_date','$language', $input, $output, $no_go, $duplicate_count, $failed, $missed)";
                    }else{
                        $sql = "UPDATE $table_name SET Input=$input, Output=$output, No_Go=$no_go, Duplicate=$duplicate_count, Failed=$failed, Missed=$missed WHERE Dates = '$today' AND Language = '$language'";
                    }
                    $conn->query($sql);
                }
            }
        }
    }       
}

$content = '';
$flag = false;
$export_data = "SELECT Dates,Language,Input,Output,No_Go,Duplicate,Failed,Missed FROM $table_name ORDER BY Dates";
$export_data_result=$conn->query($export_data);
while($value = $export_data_result->fetch_assoc()) {                
    if(!$flag) {
      $content = implode("\t", array_keys($value)) . "\r\n";
      $flag = true;
    }    
    $content .= implode("\t", array_values($value)) . "\r\n";
}
if(file_exists("uploads/".$filename)){
    unlink("uploads/".$filename);
}
file_put_contents("uploads/".$filename,$content);
echo $content;
