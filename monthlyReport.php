<?php 
// include DB connection
//include('generalconfig.php');
include('unity_dashboard_reporting.php');
// check start and end date in post request
if($_POST && isset($_POST['start']) && isset($_POST['end'])){
	$start = $_POST['start'];
	$end = $_POST['end'];
	// sql statement to retrive processed vs No-go 
	$sql = "SELECT output.output_count as output, 
				rejection.rejection_count as rejection, 
			date(output.response_date) as response_date
	        FROM (SELECT 
					DATE(t.created) 
					 AS response_date, COUNT(t.ticket_id) AS output_count 
				FROM ost_ticket t LEFT JOIN ost_ticket__cdata c ON(t.ticket_id=c.ticket_id)
				WHERE c.manual_submission=1  AND DATE(t.created) BETWEEN '$start' AND '$end'
				GROUP BY 
					 DATE(t.created) 
					
	          		) AS output
			LEFT JOIN
				(SELECT 
					date(t.closed) AS response_date, COUNT(t.ticket_id) AS rejection_count 
				FROM ost_ticket t LEFT JOIN ost_ticket__cdata c ON(t.ticket_id=c.ticket_id) WHERE c.manual_submission=1 AND t.status_id=3 AND DATE(t.closed) BETWEEN '$start' AND '$end'
				GROUP BY 
					DATE(t.closed) 
					) AS rejection
			ON (output.response_date=rejection.response_date) GROUP BY response_date,output,rejection";
			//echo $sql;
			
	// execute query
	$result=$conn->query($sql);
	$response_result = array();
	// get data from result query and return response
	if ($result->num_rows > 0) {//echo "if"; 
		$response_result = array();
	    // get data from each row
	    while($input_row = $result->fetch_assoc()) {
	    	$data['output'] = $input_row["output"];
	    	$data['rejection'] = $input_row["rejection"];
	    	$data['response_date'] = $input_row["response_date"];
	    	$response_result[] = $data;  
	    }
	    echo json_encode($response_result);
	} else { //echo "else";
		// return empty data if data is not available in DB
		$data['output'] = 0;
		$data['rejection'] = 0;
		$data['response_date'] = "No data";
		$response_result[] = $data;
		echo json_encode($response_result);
	}
	mysqli_close($conn);
}

?>
