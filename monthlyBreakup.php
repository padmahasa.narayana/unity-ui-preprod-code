<?php 
// include DB connection
//include('generalconfig.php');
include('unity_dashboard_reporting.php');
// check start and end date in post request
//if($_POST){
	// sql statement to retrive creative status data
	$sql = "SELECT count(cdata.ticket_id) creative_count, monthname(ticket.created) months
					FROM ost_ticket__cdata as cdata
				    join ost_ticket as ticket
				    on ticket.ticket_id=cdata.ticket_id
					WHERE cdata.manual_submission=1 AND year(ticket.created)=YEAR(CURDATE())
                    group by monthname(ticket.created) ORDER BY
FIELD(
    DATE_FORMAT('date', '%M'),
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
)";

	// execute query

	$result=$conn->query($sql);
	$response_result = array();
	// get data from result query and return response
	if ($result->num_rows > 0) {
		$response_result = array();
	    // get data from each row
	    while($input_row = $result->fetch_assoc()) {
	    	$data['months'] = $input_row["months"];
	    	$data['creative_count'] = $input_row["creative_count"];
	    	$response_result[] = $data;  
	    }
	    echo json_encode($response_result);
	} else {
		$data['months'] = 0;
		$data['creative_count'] = "No data";
	    $response_result[] = $data;
	    echo json_encode($response_result);
	}
	mysqli_close($conn);
//}
?>
