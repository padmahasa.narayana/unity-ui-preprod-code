<?php
$file = fopen("geoEdgeDeleteApi.log","a");
fwrite($file,"Started the Cron".PHP_EOL);
$max_date=date('Y-m-d 00:00:00',strtotime("-5 day"));
fwrite($file,"Max Date".$max_date.PHP_EOL );
// Please change it with your real api-key
define('API_KEY', '7d60699f7b6e8c08c2176624f88e19ef');
define('API_URL', 'https://api.geoedge.com/rest/analytics/v3');

$ch = curl_init();

curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '. API_KEY));

curl_setopt($ch, CURLOPT_URL, API_URL . '/projects/bulk_delete');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

#add POST data
$post_data = array(
	'max_datetime' => $max_date,
);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$result= curl_exec($ch);
fwrite($file,"OutPut:".$result.PHP_EOL );
fclose($file);

?>
