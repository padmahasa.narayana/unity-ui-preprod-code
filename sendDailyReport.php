<?php
// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
// include DB connection
include('report_prod_gereneralconfig.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once('PHPMailer_n/PHPMailer.php');
include_once("PHPMailer_n/SMTP.php");
require 'PHPMailer_n/Exception.php';

$mail = new PHPMailer(true);
$mail->SMTPDebug = 2;
$mail->isSMTP();                                // Set mailer to use SMTP
$mail->Host       = 'bsf01.theoreminc.net';  // Specify main and backup SMTP servers
// $mail->Host       = 'cas.theoreminc.net';  // Specify main and backup SMTP servers
$mail->SMTPAuth   = true;                  // Enable SMTP authentication
$mail->Username   = 'unity.alerts';        // SMTP username
$mail->Password   = 'Uni_th2022';           // SMTP password
$mail->SMTPSecure = 'tls';                 // Enable TLS encryption, `ssl` also accepted
//$mail->Port       = 587;
$mail->Port       = 25;

//Recipients
$mail->setFrom('unity.alerts@theoreminc.net', 'Theorem Unity Alerts');
$mail->addAddress('unity.support@theoreminc.net', 'Unity Support');     // Add a recipient
//$mail->addAddress('padmahasa.narayana@theoreminc.net', 'Unity Support');
//$mail->addAddress('pramod.choudhary@theoreminc.net', 'Unity Support');
//$mail->addAddress('ellen@example.com');               // Name is optional
//$mail->addAddress('ellen@example.com');               // Name is optional
$mail->addReplyTo('unity.support@theoreminc.net', 'Unity Support');
//$mail->addCC('unity.support@theoreminc.net', 'Unity Support');
$mail->addAddress('rajneesh.sunder@theoreminc.net', 'Rajneesh M');
//$mail->addCC('rajneesh.sunder@theoreminc.net');
//$mail->addCC('padmahasa.narayana@theoreminc.net');
//$mail->addCC('gmallan@theoreminc.net');

$mail->Subject    =  "Unity-Theorem Daily Creative Report  - ".date("M d, Y"); 
$mail->isHTML(true); 
	
//Mail body
$body = '<div style="font-family: Calibri;">Hi Team, <br><br><p>Attached is the programmatic creative count report across all languages for today. For any further assistance or queries, reach out to unity.support@theoreminc.net</p>';
	
$body.="Thank you<br>Theorem Support for Unity</div>";

$mail->MsgHTML($body);
//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
$filename = "daily_reports_".date('d_m_Y',strtotime("-1 days"))."_by_languages.xls";
$mail->addAttachment('uploads/'.$filename);    // Optional name
//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

if(!$mail->Send()) {
    echo 'Message could not be sent.';
} else {
    echo 'Message has been sent';
}    
mysqli_close($conn);
?>
