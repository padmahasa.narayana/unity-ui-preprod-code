<?php
require('secure.inc.php');
if(!is_object($thisclient) || !$thisclient->isValid()) die('Access denied'); 
require('client.inc.php');
include('generalconfig.php');

$nav->setActiveNav('report');


require_once(CLIENTINC_DIR.'header.inc.php');
require(CLIENTINC_DIR.'clientreport.inc.php');
require_once(CLIENTINC_DIR.'footer.inc.php');

?>