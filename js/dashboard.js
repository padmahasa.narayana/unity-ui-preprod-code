// define a global variable to set start date and format
var FROM_DATE = 30;
var FORMAT = 'MMM D, YYYY';
// report start and end date
var today_date = 10;//moment().startOf('day').format('D');
var REPORT_START_DATE = moment().startOf('day').subtract(today_date-1, 'days');
var REPORT_END_DATE = moment().startOf('day');

// custom date range picker
$(function() {
  var no_of_day = parseInt(new Date().getDate());
  $('input[name="custom_date"]').daterangepicker({
    opens: 'left',
    startDate: moment().startOf('day').subtract(FROM_DATE, 'days'),
    endDate: moment().startOf('day'),
    minDate: moment().startOf('day').subtract(3, 'months'),
    maxDate:moment().startOf('day').subtract(1, 'days'),
    autoApply: true,
    locale: {
      format: FORMAT
    },
  }, function(start, end, label) {
    // change date in all date picker
    var date_range = start.format(FORMAT)+' - '+end.format(FORMAT);
    $("input[name='input_output_date']").val(date_range);
    $("input[name='rejection_date']").val(date_range);
    $("input[name='approval_date']").val(date_range);
    $("input[name='dashboard_report_date']").val(date_range);
    $("input[name='monthly_report']").val(date_range);
    // Generate chart with data
    generateMonthlyReport(start, end);
    generateBarChartTable(start, end);
    generateInputOutputGraph(start, end);
    generateRejectionReasonGraph(start, end);
    generateDailyReport(start, end);
  });
});


function DisableWeekDays(date) {
 
 var weekenddate = $.datepicker.noWeekends(date);
 
// In order to disable weekdays, we will invert the value returned by noWeekends functions.
// noWeekends return an array with the first element being true/false.. So we will invert the first element

 var disableweek = [!weekenddate[0]]; 
 return disableweek;
}

//Input output date range picker
$(function() {
  var no_of_day = parseInt(new Date().getDate());
  $('input[name="input_output_date"]').daterangepicker({
    opens: 'left',
    startDate: moment().startOf('day').subtract(FROM_DATE, 'days'),
    endDate: moment().startOf('day'),
    minDate: moment().startOf('day').subtract(3, 'months'),
    maxDate:moment().startOf('day').subtract(1, 'days'),
    autoApply: true,
    locale: {
      format: FORMAT
    },
  }, function(start, end, label) {
    generateInputOutputGraph(start, end);
  });
});

//custom date range picker
$(function() {
  var no_of_day = parseInt(new Date().getDate());
  $('input[name="rejection_date"]').daterangepicker({
    opens: 'left',
    startDate: moment().startOf('day').subtract(FROM_DATE, 'days'),
    endDate: moment().startOf('day'),
    minDate: moment().startOf('day').subtract(3, 'months'),
    maxDate:moment().startOf('day').subtract(1, 'days'),
    autoApply: true,
    locale: {
      format: FORMAT
    },
  }, function(start, end, label) {
    generateRejectionReasonGraph(start, end);
  });
});

$(function() {
  var no_of_day = parseInt(new Date().getDate());
  $('input[name="approval_date"]').daterangepicker({
    opens: 'left',
    startDate: moment().startOf('day').subtract(FROM_DATE, 'days'),
    endDate: moment().startOf('day'),
    minDate: moment().startOf('day').subtract(3, 'months'),
    maxDate:moment().startOf('day').subtract(1, 'days'),
    autoApply: true,
    locale: {
      format: FORMAT
    },
  }, function(start, end, label) {
    generateBarChartTable(start, end);
  });
});

// Input output date range picker
$(function() {
  var no_of_day = parseInt(new Date().getDate());
  $('input[name="monthly_report"]').daterangepicker({
    opens: 'left',
    startDate: moment().startOf('day').subtract(FROM_DATE, 'days'),
    endDate: moment().startOf('day'),
    minDate: moment().startOf('day').subtract(3, 'months'),
    maxDate:moment().startOf('day').subtract(1, 'days'),
    autoApply: true,
    locale: {
      format: FORMAT
    },
  }, function(start, end, label) {
    generateMonthlyReport(start, end);
  });
});

// Report date range picker
$(function() {
  var no_of_day = parseInt(new Date().getDate());
  $('input[name="dashboard_report_date"]').daterangepicker({
    opens: 'left',
    /*startDate: REPORT_START_DATE,
    endDate: REPORT_END_DATE,*/
    startDate: moment().startOf('day').subtract(FROM_DATE, 'days'),
    endDate: moment().startOf('day'),
    minDate: moment().startOf('day').subtract(3, 'months'),
    maxDate:moment().startOf('day').subtract(1, 'days'),
    autoApply: true,
    locale: {
      format: FORMAT
    },
  }, function(start, end, label) {
    generateDailyReport(start, end);
  });
});

// generate input output graph
function generateInputOutputGraph(start, end) {
  // get start and end date
  var start_date = start.format('YYYY-MM-DD');
  var end_date = end.format('YYYY-MM-DD');
  // initialide date to send start and end date in ajax
  var post_data = {'start':start_date, 'end':end_date};
  
  $.ajax({
    type: 'POST',
    url: 'inputOutput.php',
    data: post_data,
    success: function(data){
      var xAxis_data = new Array();
      var input_data = new Array();
      var output_data = new Array();
      // parse response json data
      var res = $.parseJSON(data);
      // store response result into graph variable
      res.forEach(myFunction)
      function myFunction(item, index, arr) {
        input_data.push([Date.parse(item.created_at),parseInt(item.input)]);
        output_data.push([Date.parse(item.created_at),parseInt(item.output)]);
      }
      
      Highcharts.chart('1', {
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: {
                month: '%e. %b',
                year: '%b'
            },
            title: {
              text: 'Date'
            }
        },
        yAxis: {
          allowDecimals: false,
          title: {
            text: 'Creative Count'
          }
        },
        tooltip: {
            crosshairs: true,
           shared: true
        },
        plotOptions: {
            spline: {
                marker: {
                    radius: 4,
                   lineColor: '#666666',
                   lineWidth: 1
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: "Received",
            marker: {
               symbol: 'square'
            },
            data: input_data
        }, {
            name: "Processed",
            marker: {
               symbol: 'diamond'
            },
            data: output_data
        }],
        exporting: {
          buttons: {
            contextButton: {
              menuItems: ["viewFullscreen", "printChart", "separator", "downloadPNG", "downloadJPEG", "downloadPDF", "downloadSVG", "separator", "downloadCSV", "downloadXLS", "viewData"]
            }
          }
        }
    });


    }
  }); 
}

// generate input output graph
function generateMonthlyReport(start, end) { 
  // get start and end date
  var start_date = start.format('YYYY-MM-DD');
  var end_date = end.format('YYYY-MM-DD');
  // initialide date to send start and end date in ajax
  var post_data = {'start':start_date, 'end':end_date};
  
  $.ajax({
    type: 'POST',
    url: 'monthlyReport.php',
    data: post_data,
    success: function(data){
      var xAxis_data = new Array();
      var input_data = new Array();
      var output_data = new Array();
      // parse response json data
      var res = $.parseJSON(data);
      // store response result into graph variable
      res.forEach(myFunction)
      function myFunction(item, index, arr) {
        input_data.push([Date.parse(item.response_date),parseInt(item.output)]);
        output_data.push([Date.parse(item.response_date),parseInt(item.rejection)]);
      }
      
      Highcharts.chart('22', {
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: {
                month: '%e. %b',
                year: '%b'
            },
            title: {
              text: 'Date'
            }
        },
        yAxis: {
          allowDecimals: false,
          title: {
            text: 'Creative Count'
          }
        },
        tooltip: {
            crosshairs: true,
           shared: true
        },
        plotOptions: {
            spline: {
                marker: {
                    radius: 4,
                   lineColor: '#666666',
                   lineWidth: 1
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: "Input",
            marker: {
               symbol: 'square'
            },
            data: input_data
        }, {
            name: "Output",
            marker: {
               symbol: 'diamond'
            },
            data: output_data
        }],
        exporting: {
          buttons: {
            contextButton: {
              menuItems: ["viewFullscreen", "printChart", "separator", "downloadPNG", "downloadJPEG", "downloadPDF", "downloadSVG", "separator", "downloadCSV", "downloadXLS", "viewData"]
            }
          }
        }
    });


    }
  }); 
}

function generateRejectionReasonGraph(start, end) {
  var start_date = start.format('YYYY-MM-DD');
  var end_date = end.format('YYYY-MM-DD');
  var post_data = {'start':start_date, 'end':end_date};
  
  $.ajax({
    type: 'POST',
    url: 'RejectionReason.php',
    data: post_data,
    success: function(data){
      
      var result_data = $.parseJSON(data);
      // Build the chart
      Highcharts.chart('2', {
        /*colors: ['#7cb5ec', '#f7a35c', '#90ee7e', '#7798BF', '#aaeeee', '#ff0066',
        '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee', '#7FFFD4', '#0000FF', '#8A2BE2', '#A52A2A', '#DEB887',
        '#5F9EA0', '#7FFF00', ' #DC143C','#FFD700', '#ADFF2F', '#FF00FF'],*/
        colors: ['#6610f2', '#dc3545', '#343a40', '#ffc107', '#dc3545', 
                  '#8A2BE2', '#A52A2A', '#DEB887', '#5F9EA0', 
                 '#7FFF00', ' #DC143C','#FFD700', '#ADFF2F', '#FF00FF'],
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        exporting: {
          buttons: {
            contextButton: {
              menuItems: ["viewFullscreen", "printChart", "separator", "downloadPNG", "downloadJPEG", "downloadPDF", "downloadSVG", "separator", "downloadCSV", "downloadXLS", "viewData"]
            }
          }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: false
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Rejection Count',
            colorByPoint: true,
            data: result_data
        }]
      });
    }
  });
}

//Generate table for Aproval rejection bar chart
function generateBarChartTable(start, end) {
  var start_date = start.format('YYYY-MM-DD');
  var end_date = end.format('YYYY-MM-DD');
  var post_data = {'start':start_date, 'end':end_date};
  
  $.ajax({
    type: 'POST',
    url: 'approvalRejection.php',
    data: post_data,
    success: function(data){
      var res = $.parseJSON(data);
      var customers = new Array();
      customers.push(["", "Go", "No-Go"]);
      res.forEach(myFunction)
      function myFunction(item, index, arr) {
        customers.push([item.response_date, item.approval, item.rejection]);
      }

      //Create a HTML Table element.
      var table = document.createElement("TABLE");
      table.border = "1";

      //Get the count of columns.
      var columnCount = customers[0].length;

      //Add the header row.
      var row = table.insertRow(-1);
      for (var i = 0; i < columnCount; i++) {
          var headerCell = document.createElement("TH");
          headerCell.innerHTML = customers[0][i];
          row.appendChild(headerCell);
      }

      //Add the data rows.
      for (var i = 1; i < customers.length; i++) {
          row = table.insertRow(-1);
          for (var j = 0; j < columnCount; j++) {
              var cell = row.insertCell(-1);
              cell.innerHTML = customers[i][j];
          }
      }

      var dvTable = document.getElementById("datatable");
      dvTable.innerHTML = "";
      dvTable.appendChild(table);
      
      Highcharts.chart('3', {
        colors: ['#483D8B','#FF7F50'],
        data: {
          table: 'datatable'
        },
        chart: {
          type: 'column'
        },
        title: {
          text: ''
        },
        credits: {
            enabled: false
        },
        exporting: {
          buttons: {
            contextButton: {
              menuItems: ["viewFullscreen", "printChart", "separator", "downloadPNG", "downloadJPEG", "downloadPDF", "downloadSVG", "separator", "downloadCSV", "downloadXLS", "viewData"]
            }
          }
        },
        xAxis: {
          title: {
            text: 'Date'
          }
        },
        yAxis: {
          allowDecimals: false,
          title: {
            text: 'Creative Count'
          }
        }
      });
    }
  });
}

function generateMonthlyBarChartTable(start, end) {
  var start_date = start.format('YYYY-MM-DD');
  var end_date = end.format('YYYY-MM-DD');
  var post_data = {'start':start_date, 'end':end_date};
  
  $.ajax({
    type: 'POST',
    url: 'monthlyReport.php',
    data: post_data,
    success: function(data){
      var res = $.parseJSON(data);
      var customers = new Array();
      customers.push(["", "Opened", "Closed"]);
      res.forEach(myFunction)
      function myFunction(item, index, arr) {
        customers.push([item.response_date, item.output, item.rejection]);
      }

      //Create a HTML Table element.
      var table = document.createElement("TABLE");
      table.border = "1";

      //Get the count of columns.
      var columnCount = customers[0].length;

      //Add the header row.
      var row = table.insertRow(-1);
      for (var i = 0; i < columnCount; i++) {
          var headerCell = document.createElement("TH");
          headerCell.innerHTML = customers[0][i];
          row.appendChild(headerCell);
      }

      //Add the data rows.
      for (var i = 1; i < customers.length; i++) {
          row = table.insertRow(-1);
          for (var j = 0; j < columnCount; j++) {
              var cell = row.insertCell(-1);
              cell.innerHTML = customers[i][j];
          }
      }

      var dvTable = document.getElementById("datatable");
      dvTable.innerHTML = "";
      dvTable.appendChild(table);
      
      Highcharts.chart('33', {
        colors: ['#483D8B','#FF7F50'],
        data: {
          table: 'datatable'
        },
        chart: {
          type: 'column'
        },
        title: {
          text: ''
        },
      /* tooltip: {
            formatter: function() {
              return Highcharts.dateFormat('<span style="font-size: 10px">' +'%b - %Y', new Date(this.x)) + '</span><br/><span dx="0">' + this.series.name +'</span>:<span style="font-weight:bold" dx="0">'+this.y+'</span>';
            }
        },*/
        credits: {
            enabled: false
        },
        exporting: {
          buttons: {
            contextButton: {
              menuItems: ["viewFullscreen", "printChart", "separator", "downloadPNG", "downloadJPEG", "downloadPDF", "downloadSVG", "separator", "downloadCSV", "downloadXLS", "viewData"]
            }
          }
        },
       /* xAxis: {
          type: 'datetime',
     labels: {
    format: '{value:%b}'
  }
        },*/
        yAxis: {
          allowDecimals: false,
          title: {
            text: 'Count'
          }
        }
      });
    }
  });
}

// Generate table body for Creative status table
function generateCreativeStatusTable(status) {
  $.ajax({
    type: 'POST',
    url: 'creativeStatus.php',
    data: {'status':status},
    success: function(data){
      var res = $.parseJSON(data);
      var content = '';
      res.forEach(myFunction)
      function myFunction(item, index, arr) {
        content += '<tr>';
        //content += '<td ">' + item.creative_id + '</td>';
        content +=  '<td><a href="tickets.php?id='+ item.ticket_id +'">' + item.creative_id + '</a></td>';
        content += '<td class="label" style="text-align:right;">' + item.status + '</td>';
        content += '</tr>';
      }
      $('#searchInput').val(null);
      $('#creative_status tbody').html(content);

    }
  });
}

// Generate table body for Monthly breakup table
function generateMonthlyBreakupTable() {
  $.ajax({
    type: 'POST',
    url: 'monthlyBreakup.php',
    data: {},
    success: function(data){
      var res = $.parseJSON(data);
      var content = '';
      res.forEach(myFunction)
      function myFunction(item, index, arr) {
        content += '<tr>';
        //content += '<td ">' + item.creative_id + '</td>';
        content +=  '<td >' + item.months + '</td>';
        content += '<td class="label" style="text-align:center;">' + item.creative_count + '</td>';
        content += '</tr>';
      }
      //$('#searchInput').val(null);
      $('#monthly_breakup tbody').html(content);

    }
  });
}

// Generate table body for Daily report
function generateDailyReport(start, end) {
  var start_date = start.format('YYYY-MM-DD');
  var end_date = end.format('YYYY-MM-DD');
  var post_data = {'start':start_date, 'end':end_date};

  $.ajax({
    type: 'POST',
    url: 'dailyReport.php',
    data: post_data,
    success: function(data){
      var res = $.parseJSON(data);
      var content = '';
      res.forEach(myFunction)
      function myFunction(item, index, arr) {
        content += '<tr>';
        content += '<td>' + item.created_date + '</td>';
        content += '<td>' + item.input + '</td>';
        content += '<td>' + item.output + '</td>';
        content += '<td>' + item.rejection + '</td>';
        content += '<td>' + item.duplicate_creative_count + '</td>';
        content += '<td>' + item.failed_ticket_count + '</td>';
        content += '<td>' + item.not_appear + '</td>';
        content += '</tr>';
      }
      if (content=='') {
        content += '<tr>';
        content += '<td colspan="7">No record found for given date range </td';
        content += '</tr>';
      }
      $('#dashboard_report tbody').html(content);
      
    }
  });
}

function search() {
  // Declare variables 
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("searchInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("creative_status");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}

// Download Daily report
function downloadDailyReport() {
  var input_date = $("input[name='dashboard_report_date']").val();
  var split_date = input_date.split(" - ");
  var start = moment(split_date[0], "MMM D, YYYY");
  var end = moment(split_date[1], "MMM D, YYYY");
  
  var start_date = start.format('YYYY-MM-DD');
  var end_date = end.format('YYYY-MM-DD');
  var post_data = {'start':start_date, 'end':end_date};

  $.ajax({
    type: 'POST',
    url: 'dailyReport.php',
    data: post_data,
    success: function(data){
      var res = $.parseJSON(data);
      var customers = new Array();
      res.forEach(myFunction)
      function myFunction(item, index, arr) {
        customers.push([item.created_date, 
          item.input, 
          item.output,
          item.rejection,
          item.duplicate_creative_count,
          item.failed_ticket_count,
          item.not_appear
          ]);
        
      }

      var csv = 'Date,Total no. of creatives received,Total no. of creatives processed,No. of No Go,No. of Duplicates,No. of failed Tickets,No. of Creatives didnt appear in Ticketing system\n';
      customers.forEach(function(row) {
              csv += row.join(',');
              csv += "\n";
      });
   
      var hiddenElement = document.createElement('a');
      hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
      hiddenElement.target = '_blank';
      hiddenElement.download = 'dailyReport.csv';
      hiddenElement.click();      
    }
  });
}

