jw_insersion_id = 'jwvideoadslot'; // used for inserting theoVideoPlayer on this div ID
jw_div_id = "jwvideoad"; // used ad id of theoVideoPlayer
/*****************************************************************************
 * Set below properties to true which is require to track, otherwise false
 *****************************************************************************/
jw_trackers_object = {
  'adImpression' : true,
  'adPlay': true,
  'adPause' : true,
  'adSkipped' : true,
  'adTime' : true,
  'adClick' : true,
  'adComplete' : true,
  'adBlock' : true,
  'adError' : true,
  'playerMinimize' : true,
  'playerMute' : true,
  'playerVolume' : true,
  // VPAID Events
  'adStarted' : true,
  'adExpanded' : true,
  'adResumed' : true,
  'adVideoFirstQuartile' : true,
  'adVideoMidPoint' : true,
  'adVideoThirdQuartile' : true
}

/***************************************************************************************
 * Call theo_jwPlayerSetupWithrequiredTracker function with passing require parameters
 **************************************************************************************/
setTimeout(theo_jwPlayerSetupWithrequiredTracker, 200, jw_insersion_id, jw_div_id, jw_trackers_object);

function theo_jwPlayerSetupWithrequiredTracker(theo_jwInsersionId, theo_jwDivId, theo_jwTrackerObject){
  // just for reference of inview
  var theo_jwInview = true;
    
  /** Write container **/
  if (!document.getElementById(theo_jwDivId)) {
    var theo_jwVideoPlayer = document.getElementById(theo_jwInsersionId),
        theo_jwVideoPlayerOuter = document.createElement('div'),
        theo_jwVideoPlayerInner = document.createElement('div'),
        theo_jwVideoPlayerMain = document.createElement('div');
      
    theo_jwVideoPlayerOuter.className = "jw-video-outer";
    theo_jwVideoPlayerInner.className = "jw-video-inner";
    theo_jwVideoPlayerMain.id = theo_jwDivId;


    theo_jwVideoPlayerInner.appendChild(theo_jwVideoPlayerMain);
    theo_jwVideoPlayerOuter.appendChild(theo_jwVideoPlayerInner);
    theo_jwVideoPlayer.appendChild(theo_jwVideoPlayerOuter);
  }

  /** Initialize player **/
  theo_jwPlayerIns = jwplayer('jwvideoad');

  // configure player
  theo_jwPlayerIns.setup({
    'file': 'assets/contentvideo.mp4',
    'controls': true,
    'height': 270,
    /********************************************************
     *This Key Daily need to replace (till we got license)as from below URL  
     * https://support.jwplayer.com/customer/portal/articles/1482127-static-vast-xml-tag
     * Search for loaded file name look like "bFyELn2w.js" from location https://content.jwplatform.com/libraries/
     *******************************************************/
    "key": "qBnwvXG7nNFtSlUZLDvzg7dJg6Dp9Wh1vztKsZrTPNXVGx3gVpUS1A==",
    'mute': false,
    'width': 480,
    advertising: {
      'skipoffset': 5,
      client: 'vast',
      tag: 'assets/vast.xml'
    }
  })


  function theo_jswVideoAdInviewFunc () {
    var divObj = document.getElementById(theo_jwDivId),
      divVisiblePerc = theoEventTracker.theo_cpoCalculateVisibilityForDiv(divObj),
      theo_jwInview = (divVisiblePerc >= cpo_constants.cpo_ADVISIBILITY) ? true : false

    // console.log("JWPLAYER>>> " ,theo_jwPlayerIns.getState(), theo_jwInview);
    if ((theo_jwPlayerIns.getState() != 'idle') && (theo_jwPlayerIns.getState() != 'complete') && !theo_jwInview) {
      theo_jwPlayerIns.resize(300, 180);
      document.getElementsByClassName('jw-video-inner')[0].classList.add('jw-player-minimize');
    }else {
      theo_jwPlayerIns.resize(480, 270);
      document.getElementsByClassName('jw-video-inner')[0].classList.remove('jw-player-minimize');
    }
  }


  var theo_jwIsScrollTimeout = false;
  window.theo_jwplayerMinimisedOnScrollWhenPlaying = function() {
    // skip if no need to track minimize and maximize event
    if(!theo_jwTrackerObject.playerMinimize) return;
    // skip if we're waiting on a scroll update timeout to finish
    if (theo_jwIsScrollTimeout) return;
    // flag that a new timeout will begin
    theo_jwIsScrollTimeout = true;
    // otherwise, call scroll event view handler
    theo_jswVideoAdInviewFunc();
    // set new timeout
    setTimeout(function() {
      // reset timeout flag to false (no longer waiting)
      theo_jwIsScrollTimeout = false;
    }, 80);
  };

  // sample variable for tracking percentile 
  var theo_jwAdPercTracking = {
      '25': false,
      '50': false,
      '75': false
    },
    theo_jwAdIsComplete = false
  /*************************************
   * All jwplayer ad event listeners
   ************************************/
  // adImpression event
  theo_jwPlayerIns.on('adImpression', function (event) {
    if(!theo_jwTrackerObject[event.type]) return;
    // console.log("theo_JwPlayerIns Event ", event.adposition, "ad impression fired")
    theo_jwLogFunction(event.type);
  })

  // adSkipped event
  theo_jwPlayerIns.on('adSkipped', function (event) {
    if(!theo_jwTrackerObject[event.type]) return;
    //  console.log("theo_JwPlayerIns Event skip fired")
    theo_jwLogFunction(event.type);
  })

  // adPause event
  theo_jwPlayerIns.on('adPause', function (event) {
    if(!theo_jwTrackerObject[event.type]) return;
    //  console.log("theo_JwPlayerIns Event pause fired")
    theo_jwLogFunction(event.type);
  })

  // adPlay event
  theo_jwPlayerIns.on('adPlay', function (event) {
    if(!theo_jwTrackerObject[event.type]) return;
    //  console.log("theo_JwPlayerIns Event play fired")
    theo_jwLogFunction(event.type);
  })

  // adTime event
  theo_jwPlayerIns.on('adTime', function (event) {
    if(!theo_jwTrackerObject[event.type]) return;
    var perc = Math.round(event.position / event.duration * 100);
    if (perc >= 25 && !theo_jwAdPercTracking['25']) {
      theo_jwAdPercTracking['25'] = true;
      // console.log("theo_JwPlayerIns Event firstQuartile fired")
      theo_jwLogFunction('firstQuartile');
    }
    if (perc >= 50 && !theo_jwAdPercTracking['50']) {
      theo_jwAdPercTracking['50'] = true;
      // console.log("theo_JwPlayerIns Event midpoint fired")
      theo_jwLogFunction('midpoint');
    }
    if (perc >= 75 && !theo_jwAdPercTracking['75']) {
      theo_jwAdPercTracking['75'] = true;
      // console.log("theo_JwPlayerIns Event thirdQuartile fired")
      theo_jwLogFunction('thirdQuartile');
    }
  })

  // adClick event
  theo_jwPlayerIns.on('adClick', function (event) {
    if(!theo_jwTrackerObject[event.type]) return;
    //  console.log("theo_JwPlayerIns Event adClick fired")
    theo_jwLogFunction(event.type);
  })

  // adComplete event
  theo_jwPlayerIns.on('adComplete', function (event) {
    if(!theo_jwTrackerObject[event.type]) return;
    //  console.log("theo_JwPlayerIns Event adComplete fired")
    theo_jwLogFunction(event.type);
    theo_jwAdIsComplete = true
    theo_jwAdPercTracking = {
      '25': false,
      '50': false,
      '75': false
    }
  })

  // adBlock event
  theo_jwPlayerIns.on('adBlock', function (event) {
    if(!theo_jwTrackerObject[event.type]) return;
    //  console.log("theo_JwPlayerIns Event adBlock fired")
    theo_jwLogFunction(event.type);
  })

  // adError event
  theo_jwPlayerIns.on('adError', function (event) {
    if(!theo_jwTrackerObject[event.type]) return;
    //  console.log("theo_JwPlayerIns Event adError fired")
    theo_jwLogFunction(event.type);
  })

  /***********************************************
   *  VPAID events
   * ********************************************/
  // adStarted event
  theo_jwPlayerIns.on('adStarted', function (event) {
    if(!theo_jwTrackerObject[event.type]) return;
    //  console.log("theo_JwPlayerIns Event adStarted fired")
    theo_jwLogFunction(event.type);
  })
  // AdExpanded event
  // AdResumed event
  // AdVideoFirstQuartile event
  // AdVideoMidPoint event
  // AdVideoThirdQuartile event

  /**************************************************
   *  content and ad video player common events
   *************************************************/

  // mute event
  theo_jwPlayerIns.on('mute', function (event) {
    if(!theo_jwTrackerObject.playerMute) return;
    //  console.log("theo_JwPlayerIns Event mute fired")
    theo_jwLogFunction(event.type);
  })

  // volume event
  theo_jwPlayerIns.on('volume', function (event) {
    if(!theo_jwTrackerObject.playerVolume) return;
    //  console.log("theo_JwPlayerIns Event volume change fired")
    theo_jwLogFunction(event.type);
  })

  theo_jwLogFunction = function (e) {
    var divObj = document.getElementById(theo_jwDivId)
    if (divObj != null && divObj != undefined) {
      var data = theoEventTracker.logInfo(),
        logdata = '',
        random = Math.random() * 10000000,
        isVisible = 'NO',
        divVisiblePerc = theoEventTracker.theo_cpoCalculateVisibilityForDiv(divObj)
      if (divVisiblePerc >= cpo_constants.cpo_ADVISIBILITY) {isVisible = 'YES';}else {
        // get the non viewable reason code
      }

      logdata = data + '||et=' + e + '||ID=' + theo_jwDivId + '||w=' + divObj.offsetWidth + '||h=' + divObj.offsetHeight + '||vp=' + divVisiblePerc + '||iv=' + isVisible + '||rc=' + cpo_constants.CPO_NONVIEWREASON + '||ck=' + theoEventTracker.theo_cpoGenerateCookie() + '||sn=' + cpo_constants.cpo_SESSIONCOOKIE + '||rn=' + random + '||cid=||li='+cpo_constants.cpo_HEATMAPSTATS;

      console.log('theo_jwLogFunction logdata >>> ', logdata) // Remove this line later
      theo_cpoGenerateTrackpix(logdata, 'videoplayer');   //-----> Function to log data in apachache access logs
    }
  }
}
