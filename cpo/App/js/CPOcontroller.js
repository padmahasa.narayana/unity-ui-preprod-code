
  window.theo_xhrArr = window.theo_xhrArr || [];
  var origOpen = XMLHttpRequest.prototype.open;
  XMLHttpRequest.prototype.open = function(e,url) {
      this.addEventListener('error', function() {
        window.theo_xhrArr.push(url);
      });    
    origOpen.apply(this, arguments);
  };
 var theo_extensionListToCheckArr = [];
  if(!window.theo_installedExtensionListArr){
    window.theo_installedExtensionListArr =  [];

    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;

    if (is_chrome===true)
    {
      for ( var i = 0; i < theo_extensionListToCheckArr.length; i++ ){
          var img = new Image();
          img.onload = (function(value){
              return function(){                
                  theo_installedExtensionListArr.push(value);                
              };
          })(theo_extensionListToCheckArr[i].name);
          img.src = theo_extensionListToCheckArr[i].asseturl;
      }    
    }    
  }

//var cpo_LOGSERVERIP = 'https://cpotracker.theoreminc.net';
var cpo_LOGSERVERIP = 'http://52.45.109.26/cpo/App/images/';
var cpo_constants = {
  cpo_UNIQUEIDS:[],
  cpo_PANELIDS:[], 
  cpo_SLOTIDS: [],
  cpo_PUBLISHERURL: [],
  cpo_CLASSNAME: '',
  cpo_RANDOMSALT: '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
  cpo_ADVISIBILITY: 50,
  CPO_NONVIEWREASON: '',
  cpo_ACCEPTEDVIEWABLEPERCENTAGE: 100,
  cpo_SCROLLTIMETOTRACK: 1000, // in mili Sec.
  cpo_MOUSEINTERVAL: 1000, // In mili Sec.
  cpo_SESSIONCOOKIE: '', // Session Cookie.
  cpo_SESSIONSEQ: 0, // Session Sequence.
  cpo_PVTIME: 0,
  cpo_CREATIVEIDARR: [],
  cpo_LINEITEMARR: [],
  cpo_VIARR: [],

  cpo_VIEWLOGURL: this.cpo_LOGSERVERIP +'viewimage.png',
  cpo_CLICKLOGURL: this.cpo_LOGSERVERIP +'clickimage.png',
  cpo_SCROLLLOGURL: this.cpo_LOGSERVERIP +'scrollimage.png',
  cpo_PAGEVIEWLOGURL :this.cpo_LOGSERVERIP +'pageimage.png',
  cpo_IMPRESSIONLOGURL : this.cpo_LOGSERVERIP +'impimage.png',
  cpo_MOUSEMOVELOGURL : this.cpo_LOGSERVERIP +'mouseimage.png',
  cpo_ADBLOCKLOGURL : this.cpo_LOGSERVERIP +'blockimage.png',
  cpo_VIDEOLOGURL : this.cpo_LOGSERVERIP +'videoimage.png',
  cpo_ADFRAUDLOGURL : this.cpo_LOGSERVERIP +'fraudimage.png',

  cpo_LINEITEM: 'placementId',
  cpo_CREATIVEID: 'pubCreative',  
  CPO_COOKENAB : navigator.cookieEnabled,
  cpo_ISADBLOCK : false,
  cpo_TRACKMOUSEHOVER : false,
  cpo_TRACKADCLICK : false,
  cpo_TRACKSCROLL : false,
  cpo_TRACKMOUSECLICK : false,
  cpo_TRACKMOUSEMOVE : false,
  cpo_TRACKPAGEVIEW : false,
  cpo_SECTIONSUBDOMAIN : false,	// true = SUB-DOMAIN, false = DOMAIN
  cpo_ADLATENCY : true,	// true = Enabled, false = Disabled
  cpo_UN : '',
  cpo_VIDEOPLAYEREVENTMAPPING : {
    'adImpression' : 'adImpression',
    'adPlay': 'adPlay',
    'adPause' : 'adPause',
    'adSkipped' : 'adSkipped',
    'adTime' : 'adTime',
    'adClick' : 'adClick',
    'adComplete' : 'adComplete',
    'adBlock' : 'adBlock',
    'adError' : 'adError',
    'firstQuartile' : 'adFirstQuartile',
    'midpoint' : 'adMidPoint',
    'thirdQuartile' : 'adThirdQuartile',
    'skipshown' : 'adSkipShown',
    // VPAID ad events
    'adStarted' : 'adStarted',
    'adExpanded' : 'adExpanded',
    'adResumed' : 'adResumed',
    'adVideoFirstQuartile' : 'adFirstQuartile',
    'adVideoMidPoint' : 'adMidPoint',
    'adVideoThirdQuartile' : 'adThirdQuartile',
    // content video events
    'firstFrame': 'started',
    'rewind' : 'rewind',
    'play' : 'play',
    'resume' : 'play',
    'pause' : 'pause',
    'finish' : 'complete',
    'complete' : 'complete',
    'fullscreen' : 'fullscreen',
    'fullscreen-exit' : 'fullscreen-exit',
    'seek' : 'seek',
    'mute' : 'mute',
    'unmute' : 'unmute',
    'stop' : 'stop',
    'contentVid25' : 'firstQuartile',
    'contentVid50' : 'midPoint',
    'contentVid75' : 'thirdQuartile',
    'player-minimize' : 'player-minimize',
    'player-maximize' : 'player-maximize',
    'setupError' : 'error',
    'error' : 'error'
  },
  cpo_PLAYERTRACKER : {
    'adImpression' : true,
    'adPlay': true,
    'adPause' : true,
    'adSkipped' : true,
    'adTime' : true,
    'adClick' : true,
    'adComplete' : true,
    'adBlock' : true,
    'adError' : true,
    // VPAID Events
    'adStarted' : true,
    'adExpanded' : true,
    'adResumed' : true,
    'adVideoFirstQuartile' : true,
    'adVideoMidPoint' : true,
    'adVideoThirdQuartile' : true,
    //content Video Tracking
    'playerMinimize' : true,
    'playerMute' : true,
    'playerVideo': true
  }
};

/****************************
 * INIT VARS AND ETC FROM HTML SCRIPT
 ***************************/
var CPO_LIB = CPO_LIB || (function(){
    var _args = {}; // private

    return {
        init : function(Args) {
            _args = Args;			
			if(_args.cpo_PANELIDS){cpo_constants.cpo_PANELIDS = _args.cpo_PANELIDS;}
			if(_args.cpo_SLOTIDS){cpo_constants.cpo_SLOTIDS = _args.cpo_SLOTIDS;}
			if(_args.cpo_PUBLISHERURL){cpo_constants.cpo_PUBLISHERURL = _args.cpo_PUBLISHERURL;}
			if(_args.cpo_CLASSNAME){cpo_constants.cpo_CLASSNAME = _args.cpo_CLASSNAME;}
			if(_args.cpo_TRACKMOUSEHOVER){cpo_constants.cpo_TRACKMOUSEHOVER = _args.cpo_TRACKMOUSEHOVER;}
			if(_args.cpo_TRACKADCLICK){cpo_constants.cpo_TRACKADCLICK = _args.cpo_TRACKADCLICK;}
			if(_args.cpo_TRACKSCROLL){cpo_constants.cpo_TRACKSCROLL = _args.cpo_TRACKSCROLL;}
			if(_args.cpo_TRACKMOUSECLICK){cpo_constants.cpo_TRACKMOUSECLICK = _args.cpo_TRACKMOUSECLICK;}
			if(_args.cpo_TRACKMOUSEMOVE){cpo_constants.cpo_TRACKMOUSEMOVE = _args.cpo_TRACKMOUSEMOVE;}
			if(_args.cpo_TRACKPAGEVIEW){cpo_constants.cpo_TRACKPAGEVIEW = _args.cpo_TRACKPAGEVIEW;}
			if(_args.cpo_ADVISIBILITY){cpo_constants.cpo_ADVISIBILITY = _args.cpo_ADVISIBILITY;}
			if(_args.cpo_SCROLLTIMETOTRACK){cpo_constants.cpo_SCROLLTIMETOTRACK = _args.cpo_SCROLLTIMETOTRACK;}
			if(_args.cpo_MOUSEINTERVAL){cpo_constants.cpo_MOUSEINTERVAL = _args.cpo_MOUSEINTERVAL;}
			if(_args.cpo_SECTIONSUBDOMAIN){cpo_constants.cpo_SECTIONSUBDOMAIN = _args.cpo_SECTIONSUBDOMAIN;}
			if(_args.cpo_ADLATENCY){cpo_constants.cpo_ADLATENCY = _args.cpo_ADLATENCY;}
			if(_args.cpo_UN){cpo_constants.cpo_UN = _args.cpo_UN;}				
        },        
    };
}());

function cleanArray(actual) {
  var newArray = new Array();
  for (var i = 0; i < actual.length; i++) {
	    if (actual[i] != '[object HTMLCollection]') {
      newArray.push(actual[i]);
    }
  }
  return newArray;
}

function getHEATMAPSTATS(divObj){
	var result = '||tn=||cn=||es=||cx=||cy=||px=||py=||mit=||mot=||tot=';
	if(divObj){
		position= getPos(divObj);
		result = '||tn=||cn=||es=||cx='+position.x+'||cy='+position.y+'||px=||py=||mit=||mot=||tot=';
	}
	
	return result;
}

function randomString (length) {
  var result = '';
  chars = cpo_constants.cpo_RANDOMSALT;
  for (var i = length; i > 0; --i) {
    result += chars[Math.round(Math.random() * (chars.length - 1))];
  }
    return result;  
}

function findScriptTagsFromIframe (iframe,searchTag) {
  var newScriptArray = [];
  try {
    var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
    var scriptTagsInsideIframe = innerDoc.querySelectorAll(searchTag);
    for (var j in scriptTagsInsideIframe) {
      if (scriptTagsInsideIframe[j].nodeName === searchTag.toUpperCase()) {
        newScriptArray.push({id: iframe.id,tag: scriptTagsInsideIframe[j]}); 
      }
    }
  } catch (error) {}
  return newScriptArray;
}

function getPos(el) {
     for (var lx=0, ly=0;
         el != null;
         lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
    return {x: lx,y: ly};
}
function processDOM_elements (searchTag) {
  var scriptTags = document.querySelectorAll(searchTag + ',iframe'),
    allScriptTagsIncludingInsideIframe = [];
  for (var i in scriptTags) {
    switch (scriptTags[i].nodeName) {
      case searchTag.toUpperCase():
        allScriptTagsIncludingInsideIframe.push({id: scriptTags[i].parentNode.id,tag: scriptTags[i]});
        break;
      case 'IFRAME':
        var tmpArr = findScriptTagsFromIframe(scriptTags[i], searchTag);
        for (var l in tmpArr) {
          allScriptTagsIncludingInsideIframe.push(tmpArr[l]);
        }
        break;
    }
  }
   if(searchTag == 'script' || searchTag == 'IFRAME') {
    for (var k in allScriptTagsIncludingInsideIframe) {
      for (i = 0; i < cpo_constants.cpo_PUBLISHERURL.length; i++) {
        if (allScriptTagsIncludingInsideIframe[k].tag.src && (allScriptTagsIncludingInsideIframe[k].tag.src.indexOf(cpo_constants.cpo_PUBLISHERURL[i]) > -1)) {
          src = allScriptTagsIncludingInsideIframe[k].tag.src;
		  if(allScriptTagsIncludingInsideIframe[k].tag.tagName == "IFRAME"){
			if(!cpo_constants.cpo_UNIQUEIDS.includes(allScriptTagsIncludingInsideIframe[k].tag.id)){
				cpo_constants.cpo_UNIQUEIDS.push(allScriptTagsIncludingInsideIframe[k].tag.id);
				cpo_constants.cpo_PANELIDS.push(allScriptTagsIncludingInsideIframe[k].tag.id);
			}
		
			//check element id is already mapped in cpo_constants.cpo_UNIQUEIDS
		}
		else{
        		  if (src.indexOf(cpo_constants.cpo_CREATIVEID) > 0) {
		            creativeId = unescape(src).split(cpo_constants.cpo_CREATIVEID + '=')[1].split('&')[0];
		            lineitemId = unescape(src).split(cpo_constants.cpo_LINEITEM + '=')[1].split('&')[0];
		            if (allScriptTagsIncludingInsideIframe[k].id !== '') {
			              cpo_constants.cpo_CREATIVEIDARR[allScriptTagsIncludingInsideIframe[k].id] = creativeId;
			              cpo_constants.cpo_LINEITEMARR[allScriptTagsIncludingInsideIframe[k].id] = lineitemId;
			              if(!cpo_constants.cpo_UNIQUEIDS.includes(allScriptTagsIncludingInsideIframe[k].id)){
						cpo_constants.cpo_UNIQUEIDS.push(allScriptTagsIncludingInsideIframe[k].id);
						cpo_constants.cpo_PANELIDS.push(allScriptTagsIncludingInsideIframe[k].id);
					}
		              }
			  }
          	}
        }
      }
    }
  } 
  return {
    'searchTagArr': allScriptTagsIncludingInsideIframe
  };
}

function getCreativeId (adid) {
  return (cpo_constants.cpo_CREATIVEIDARR[adid]) ? cpo_constants.cpo_CREATIVEIDARR[adid] : adid;
}

function getLineItemId (adid) {
  return (cpo_constants.cpo_LINEITEMARR[adid]) ? cpo_constants.cpo_LINEITEMARR[adid] : adid;
}

function InitializeUserNetwork(){
	return theoEventTracker.theo_cpoSetPublisherCookie();  
}

function get_userNetwork() {
  var adId = '';
  var scriptTags = document.getElementsByTagName('script');
  
  for (var k in scriptTags) {
    if (scriptTags[k].src && ((scriptTags[k].src.indexOf('CPOcontroller') > -1) || (scriptTags[k].src.indexOf('cpoCntrl') > -1) )) {
      src = scriptTags[k].src;
      adId = (src.indexOf('un') > 0) ? unescape(src).split('un=')[1].split('&')[0] : InitializeUserNetwork();    
    }
  }
  return adId;
}

var theoEventTracker = {
  enginName: navigator.appName,
  osName: navigator.userAgent.match(/Windows/),
  osVersion: navigator.platform,
  site: document.location.hostname,
  page: window.location.href,

  get_browser: function () {
    var ua = navigator.userAgent,
      tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
      tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
      return {
        name: 'IE',
        version: (tem[1] || '')
      };
    }
    if (M[1] === 'Chrome') {
      tem = ua.match(/\bOPR\/(\d+)/);
      if (tem !== null) {
        return {
          name: 'Opera',
          version: tem[1]
        };
      }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) !== null) {
      M.splice(1, 1, tem[1]);
    }
    return {
      name: M[0],
      version: M[1]
    };
  },
    
  get_section: function () {
    if(cpo_constants.cpo_SECTIONSUBDOMAIN){
		category = this.site.split(".");
		if(category.length >= 2){
			return category[0];
		}else{
			return "";
		}
	}
    else{
		category = this.page.split("/");
		if(category.length > 4){
			return category[3];
		}else{
			return "";
		}
	}
  },
  
  get_device: function () {
    var nav = window.navigator,
      ua = window.navigator.userAgent.toLowerCase();
    if (ua.match(/ipad/i) !== null)
      return 'iPod';
    if (ua.match(/iphone/i) !== null)
      return 'iPhone';
    if (ua.match(/android/i) !== null)
      return 'Android';
    if ( (nav.appName.toLowerCase().indexOf('microsoft') != -1 || nav.appName.toLowerCase().match(/trident/gi) !== null))
      return 'IE';
    return null;
  },

  getSequenceId: function () {
	if(cpo_constants.CPO_COOKENAB === false){cpo_constants.cpo_SESSIONSEQ = parseInt(cpo_constants.cpo_SESSIONSEQ) + 1;}
	else{		
		cpo_constants.cpo_SESSIONSEQ = this.theo_cpoGetCookieByName('Thrm_CPOseq');
		cpo_constants.cpo_SESSIONSEQ = parseInt(cpo_constants.cpo_SESSIONSEQ) + 1;
		document.cookie = 'Thrm_CPOseq = ' + cpo_constants.cpo_SESSIONSEQ;
		cpo_constants.cpo_SESSIONSEQ = this.theo_cpoGetCookieByName('Thrm_CPOseq');
	}
    return cpo_constants.cpo_SESSIONSEQ;
  },
 windowActiveInactive: (function () {
    var hidden = 'hidden';

    // Standards:
    if (hidden in document)
      document.addEventListener('visibilitychange', onchange);
    else if ((hidden = 'mozHidden') in document)
      document.addEventListener('mozvisibilitychange', onchange);
    else if ((hidden = 'webkitHidden') in document)
      document.addEventListener('webkitvisibilitychange', onchange);
    else if ((hidden = 'msHidden') in document)
      document.addEventListener('msvisibilitychange', onchange);
    // IE 9 and lower:
    else if ('onfocusin' in document)
      document.onfocusin = document.onfocusout = onchange;
    // All others:
    else
      window.onpageshow = window.onpagehide = window.onfocus = window.onblur = onchange;

    function onchange (evt) {
      var v = 'visible',
        h = 'hidden',
        evtMap = {
          focus: v,
          focusin: v,
          pageshow: v,
          blur: h,
          focusout: h,
          pagehide: h
      };

      evt = evt || window.event;
      if (evt.type in evtMap)
        document.body.className = evtMap[evt.type];
      else
        document.body.className = this[hidden] ? 'hidden' : 'visible';
      if (document.body.className == 'hidden') {
        cpo_constants.CPO_NONVIEWREASON = 'R5';
        var usernetworkid = get_userNetwork();
        var deviceTracker = theoEventTracker.get_device();
        var deviceName = '';
        if (deviceTracker) {
          deviceName = deviceTracker;
        } else {
          deviceName = 'Not Detected';
        }

        var windowWidth = document.documentElement.clientWidth,
          windowHeight = document.documentElement.clientHeight,
          logdata = 'n=' + usernetworkid + '||s=' + theoEventTracker.site + '||p=' + theoEventTracker.page + '||bn=' + theoEventTracker.get_browser().name + '||bv=' + theoEventTracker.get_browser().version + '||dn=' + deviceName + '||en=' + theoEventTracker.enginName + '||on=' + theoEventTracker.osName + '||ov=' + theoEventTracker.osVersion + '||wa=False' + '||sq=' + theoEventTracker.getSequenceId() + '||l=' + theoEventTracker.theo_cpogetLocale() + '||et=Inactivewindow||ID=||w=' + windowWidth + '||h=' + windowHeight + '||vp=0||iv=false||rc=' + cpo_constants.CPO_NONVIEWREASON;

        theoEventTracker.theo_cpoLogInactiveWindowData(logdata, 'Inactivewindow');
        return false;
      } else {
        // theoEventTracker.CPO_NONVIEWREASON = ''
        return true;
      }
    }
    /*if (document[hidden] !== undefined)
      onchange({
        type: document[hidden] ? 'blur' : 'focus'
      });*/
  })(), 

  logInfo: function () {
    var deviceTracker = this.get_device();

    var deviceName = '';
    if (deviceTracker) {
      deviceName = deviceTracker;
    } else {
      deviceName = 'Not Detected';
    }

    var usernetworkid = get_userNetwork();
    return 'n=' + usernetworkid + '||s=' + this.site + '||p=' + this.page + '||bn=' + this.get_browser().name + '||bv=' + this.get_browser().version + '||dn=' + deviceName + '||en=' + this.enginName + '||on=' + this.osName + '||ov=' + this.osVersion + '||wa=TRUE' + '||sq=' + this.getSequenceId() + '||l=' + this.theo_cpogetLocale();
  },

  theo_adOverlappingForDiv: function (divObj, Overlapdiv) {
    var x1 = divObj.offset().left;
    var y1 = divObj.offset().top;
    var h1 = divObj.outerHeight(true);
    var w1 = divObj.outerWidth(true);
    var b1 = y1 + h1;
    var r1 = x1 + w1;
    var x2 = Overlapdiv.offset().left;
    var y2 = Overlapdiv.offset().top;
    var h2 = Overlapdiv.outerHeight(true);
    var w2 = Overlapdiv.outerWidth(true);
    var b2 = y2 + h2;
    var r2 = x2 + w2;

    if (b1 < y2 || y1 > b2 || r1 < x2 || x1 > r2) return false;
    return true;
  },
   
  theo_cpoCalculateVisibilityForDiv: function (divObj) {
	var divBoundingRect = divObj.getBoundingClientRect(),
		windowHeight = document.documentElement.clientHeight,
		visiblePerc;
	
	if (divBoundingRect.top < 0) {
		visiblePerc = (divBoundingRect.height + divBoundingRect.top) / divBoundingRect.height * 100;
		if(visiblePerc < 0){
			this.CPO_NONVIEWREASON = 'R0';
			visiblePerc = 0;
		}else{
			this.CPO_NONVIEWREASON = '';
		}
	}else if ((divBoundingRect.top + divBoundingRect.height) > windowHeight) {
		visiblePerc = (windowHeight -  divBoundingRect.top) / divBoundingRect.height * 100;
		if(visiblePerc < 0){
			this.CPO_NONVIEWREASON = 'R1';
			visiblePerc = 0;
		}else{
			this.CPO_NONVIEWREASON = '';
		}
	}else{
		visiblePerc = 100;
	}

	return  parseFloat(visiblePerc).toFixed(2);
  },
  
  theo_cpoTrackedIDcalc: function (obj, type, e) {
	obj = cleanArray(obj);  
    var data = theoEventTracker.logInfo(),
      results = {};
    var cookieValue = this.theo_cpoGenerateCookie();
    for (i = 0; i < obj.length; i++) {
      var divId = '',
        isVisible = 'NO',
        random = Math.random() * 10000000,
        count = 0;
      if (type == 'id' || type == 'ad_slot') {
        divId = obj[i];
      }
      if (type == 'class') {
        divId = obj[i].id;
		if(divId == "" || divId == null){
			var sinCls = obj[i].className.split(" ");
			obj[i].id = sinCls[0]+'_CPO_'+i;
			divId = obj[i].id;
		}
      }
      var divObj = document.getElementById(divId);
      if (divObj !== null && divObj !== undefined) {
        var logdata = '';
        results[divId] = this.theo_cpoCalculateVisibilityForDiv(divObj);
        if (results[divId] >= cpo_constants.cpo_ADVISIBILITY) {
          isVisible = 'YES';
        } else {
          // get the non viewable reason code
        }
		 if (results[divId] == 100) {
          var isVisible_imp = theo_cpoVisibleImpression(divObj);
        }
		
		if(e == "window_load"){ // Call this only once during window load.
			// IMAGE LATENCY -- START
			var ImgLatency = "";
			if(cpo_constants.cpo_ADLATENCY){
				ImgLatency = this.theo_cpoGetChildIMG(divObj);
			}
			// LOG DATA
			position= getPos(divObj);			
			logdata = data + '||et=' + e + '||ID=' + divObj.id + '||w=' + divObj.offsetWidth + '||h=' + divObj.offsetHeight + '||vp=' + results[divId] + '||iv=' + isVisible + '||rc=' + this.CPO_NONVIEWREASON + '||ck=' + cookieValue + '||sn=' + cpo_constants.cpo_SESSIONCOOKIE + '||rn=' + random + '||cid=' + getCreativeId(divId) + '||li=' + getLineItemId(divId) + '||tn=||cn=||es=||cx='+position.x+'||cy='+position.y+'||px=||py=||mit=||mot=||tot='+ImgLatency;
			// IMAGE LATENCY -- END
		}
		//Call otherthan window load
		else{
			logdata = data + '||et=' + e + '||ID=' + divObj.id + '||w=' + divObj.offsetWidth + '||h=' + divObj.offsetHeight + '||vp=' + results[divId] + '||iv=' + isVisible + '||rc=' + this.CPO_NONVIEWREASON + '||ck=' + cookieValue + '||sn=' + cpo_constants.cpo_SESSIONCOOKIE + '||rn=' + random + '||cid=' + getCreativeId(divId) + '||li=' + getLineItemId(divId) + getHEATMAPSTATS(divObj);
		}
		 theo_cpoGenerateTrackpix(logdata, e); // -----> Function to log data in apachache access logs
	     }
    }

    function theo_cpoVisibleImpression (divObj) {
      var check = {
        percentObscured: 0,
        percentViewable: 0,
        acceptedViewablePercentage: cpo_constants.cpo_ACCEPTEDVIEWABLEPERCENTAGE,
        viewabiltyStatus: false,
        duration: 0
      };
      var timer = setInterval(function () {
        if (theoEventTracker.theo_cpoCalculateVisibilityForDiv(divObj) >= check.acceptedViewablePercentage) {
          count++;
        } else {
          count = 0;
          clearInterval(timer);
        }
        check.duration = count * 100;
        if (count >= 9) {
          check.viewabiltyStatus = true;
         var logdataVI = '';
          clearInterval(timer);
         if(cpo_constants.cpo_VIARR.indexOf(divObj.id) > -1){
				// Do-NOTHING				
			}
			else
			{				
				// Log Data and push object id to array
				if(divObj.offsetWidth === 0 || divObj.offsetHeight === 0){
					logdataVI = data + '||et=visibleimpression' + '||ID=' + divObj.id + '||w=' + divObj.offsetWidth + '||h=' + divObj.offsetHeight + '||vp=' + '0' + '||iv=visibleimpression||rc=' + 'R7' + '||ck=' + cookieValue + '||sn=' + cpo_constants.cpo_SESSIONCOOKIE + '||rn=' + random + '||cid=' + getCreativeId(divObj.id) + '||li=' + getLineItemId(divObj.id) + getHEATMAPSTATS(divObj);
				}
				else{
					logdataVI = data + '||et=visibleimpression' + '||ID=' + divObj.id + '||w=' + divObj.offsetWidth + '||h=' + divObj.offsetHeight + '||vp=' + theoEventTracker.theo_cpoCalculateVisibilityForDiv(divObj) + '||iv=visibleimpression||rc=' + '' + '||ck=' + cookieValue + '||sn=' + cpo_constants.cpo_SESSIONCOOKIE + '||rn=' + random + '||cid=' + getCreativeId(divObj.id) + '||li=' + getLineItemId(divObj.id) + getHEATMAPSTATS(divObj);
				}
				theo_cpoGenerateTrackpix(logdataVI, 'visibleimpression');
				
				cpo_constants.cpo_VIARR.push(divObj.id); // Push ID Values to the array
			}
        }
       }, 1000);
    }
  },

  theo_cpoGenerateCookie: function () {
    var getCookieValue = '';
    if (this.theo_cpoGetCookie('Thrm_CPOtracker')) {
      getCookieValue = this.theo_cpoGetCookie('Thrm_CPOtracker');
    } else {
      var CPOcookExpdata = new Date();
      CPOcookExpdata.setDate(CPOcookExpdata.getDate() + 365);      
      document.cookie = 'Thrm_CPOtracker = Thrmtrack_' + Math.floor(Math.random() * 1000000) + '; expires=' + CPOcookExpdata;
      getCookieValue = this.theo_cpoGetCookie('Thrm_CPOtracker');
    }
	
	if(cpo_constants.CPO_COOKENAB === false){getCookieValue = "disabled";}
    return getCookieValue;
  },

  // get cookie value based on the key
  theo_cpoGetCookie: function (cname) {
    var name = cname + '=';
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') c = c.substring(1);
      if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
    }
    return '';
  },

  // Track and LOG Click_Details
  theo_cpoLogClickData: function (data_trackedClick, e, id) {
    var cookieValue = this.theo_cpoGenerateCookie();
    var dataGeneric = this.logInfo(),
      windowWidth = document.documentElement.clientWidth,
      windowHeight = document.documentElement.clientHeight,
      random = Math.random() * 10000000,
      logdata = dataGeneric + '||et=' + e + '||ID=' + id + '||w=' + windowWidth + '||h=' + windowHeight + '||vp=' + '||iv=TRUE' + '||rc=' + this.CPO_NONVIEWREASON + '||ck=' + cookieValue + '||sn=' + cpo_constants.cpo_SESSIONCOOKIE + '||rn=' + random + '||cid=' + getCreativeId(id) + '||li=' + getLineItemId(id) + '||' + data_trackedClick;
   theo_cpoGenerateTrackpix(logdata, e); // -----> Function to log data in apachache access logs
  },

   theo_cpoLogInactiveWindowData: function (inactivedata_log, e) {
	 var divObj = (window.event) ? window.event.srcElement : e.target; 
    var cookieValue = this.theo_cpoGenerateCookie();
    var random = Math.random() * 10000000,
      logdata = inactivedata_log + '||ck=' + cookieValue + '||sn=' + cpo_constants.cpo_SESSIONCOOKIE + '||rn=' + random + '||cid=||li=' + getHEATMAPSTATS(divObj);
    theo_cpoGenerateTrackpix(logdata, e); // -----> Function to log data in apachache access logs
  },

  theo_cpofindDomElements: function () {
    var dom_Element = document.getElementsByTagName('*');
    var domArry = [];
    for (var i = 0; i < dom_Element.length; i++) {
      var domIDs = dom_Element[i].getAttribute('id');
      if (domIDs !== null) {
        domArry.push(domIDs);
      }
    }
    return domArry;
  },

  theo_cpoFindmatchingDoms: function (domArry) {
    var theo_cpofound = [];
    var theo_cpofound_adIDS = [];
    var theo_cpofoundFull_adIDS = [];
    for (var i = 0; i < cpo_constants.cpo_SLOTIDS.length; i++) {
      for (var j = 0; j < domArry.length; j++) {
        if (domArry[j].indexOf(cpo_constants.cpo_SLOTIDS[i]) != -1) { // includes -> (domArry[j].includes(cpo_constants.cpo_SLOTIDS[i]))
          theo_cpofound.push(cpo_constants.cpo_SLOTIDS[i]);
          theo_cpofoundFull_adIDS.push(domArry[j]);
          var idsfullLength = domArry[j].length;
          var ad_slot_startPos = domArry[j].search(cpo_constants.cpo_SLOTIDS[i]);
          var res_id = domArry[j].substring(ad_slot_startPos, idsfullLength);
           if(!cpo_constants.cpo_UNIQUEIDS.includes(res_id)){
				cpo_constants.cpo_UNIQUEIDS.push(res_id);
				theo_cpofound_adIDS.push(res_id);
		  }
          break;
        }
      }
    }
    return theo_cpofoundFull_adIDS;
  },

  theo_cpogetLocale: function () {
    if (navigator.languages !== undefined)
      return navigator.languages[0];
    else
      return navigator.language;
  },

  // Attach Click event to objects
  theo_cpoAttachClickEvent: function (obj, type) {
    var divObj,i;
	
    if (obj) {
      if (type == 'id') {
        for (i = 0; i < obj.length; i++) {
          divObj = document.getElementById(obj[i]);
          if(divObj) divObj.addEventListener('click', this.theo_cpoTrackerClickEvent, false);
        }
      }
      if (type == 'classname') {	  
		if(cpo_constants.cpo_CLASSNAME.length == 1){
		    divObj = document.getElementsByClassName(obj);
			for (i = 0; i < divObj.length; i++) {
			  if(divObj[i]) divObj[i].addEventListener('click', this.theo_cpoTrackerClickEvent, false);	
			}
		}
		else{
			for(var clCount=0;clCount < cpo_constants.cpo_CLASSNAME.length; clCount++) {
				divObj = document.getElementsByClassName(obj[clCount]);
				for (i = 0; i < divObj.length; i++) {
				  if(divObj[i]) divObj[i].addEventListener('click', this.theo_cpoTrackerClickEvent, false);
				}
			}
		}
      }
    }
  },

  // Attach mouseover event to objects
  theo_cpoAttachOverOutEvent: function (obj, type) {
    var divObj,i;
    if (obj) {
      if (type == 'id') {
        for (i = 0; i < obj.length; i++) {
          divObj = document.getElementById(obj[i]);
          if (divObj) {
            divObj.addEventListener('mouseover',theo_cpoMouseOver);
            divObj.addEventListener('mouseout',theo_cpoMouseOut);
          }
        }
      }
      if (type == 'classname') {
		if(cpo_constants.cpo_CLASSNAME.length == 1){
			divObj = document.getElementsByClassName(obj);
			for (i = 0; i < divObj.length; i++) {
			  if (divObj[i]) {
				divObj[i].addEventListener('mouseover',theo_cpoMouseOver);
				divObj[i].addEventListener('mouseout',theo_cpoMouseOut);
			  }
			}
		}
		else{
			for(var clCount=0;clCount < cpo_constants.cpo_CLASSNAME.length; clCount++) {
				divObj = document.getElementsByClassName(obj[clCount]);
				for (i = 0; i < divObj.length; i++) {
				  if (divObj[i]) {
					divObj[i].addEventListener('mouseover',theo_cpoMouseOver);
					divObj[i].addEventListener('mouseout',theo_cpoMouseOut);
				  }
				}
			}
		}
      }
    }
  },
  
   // Attach Click event to objects -- logger
  theo_cpoTrackerClickEvent: function () {
    var theoclsID = [this];
   theoEventTracker.theo_cpoTrackedIDcalc(theoclsID, 'class', 'click');    
  },
  
  //set Publisher cookie
  theo_cpoSetPublisherCookie: function () {
	  
	  var getPublisherCookieValue = '';
    if (this.theo_cpoGetCookieByName('Thrm_CPOpublisher')) {
      getPublisherCookieValue = this.theo_cpoGetCookieByName('Thrm_CPOpublisher');
    } else {
		if(cpo_constants.cpo_UN === ""){
			document.cookie = 'Thrm_CPOpublisher = UN-' + Math.floor(Math.random() * 90000) + '-' + randomString(3);
			getPublisherCookieValue = this.theo_cpoGetCookieByName('Thrm_CPOpublisher');
		}else{
			document.cookie = 'Thrm_CPOpublisher ='+cpo_constants.cpo_UN;
			getPublisherCookieValue = this.theo_cpoGetCookieByName('Thrm_CPOpublisher');
		}
    }
    return getPublisherCookieValue;
  },
   // Set SessionCookie
  theo_cpoSetSessionCookie: function () {
    var getSessionCookieValue = '';
    if (this.theo_cpoGetCookieByName('Thrm_CPOsession')) {
      getSessionCookieValue = this.theo_cpoGetCookieByName('Thrm_CPOsession');
    } else {
      document.cookie = 'Thrm_CPOsession = theoSession_' + Math.floor(Math.random() * 1000000);
      document.cookie = 'Thrm_CPOseq = 0';
      getSessionCookieValue = this.theo_cpoGetCookieByName('Thrm_CPOsession');
    }
    return getSessionCookieValue;
  },

  theo_cpoGetCookieByName: function (cookName) {
    var name = cookName + '=';
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') c = c.substring(1);
      if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
    }
    return '';
  },

	/*************************************
	* Code for TRACKING PAGE VIEWS Function Definition || MOUSEMOVE EVENT TRACKING
	*************************************/ 
	theo_cpoPageViews: function () {
		var cookieValue = this.theo_cpoGenerateCookie();
		var dataGeneric = this.logInfo(),
		  windowWidth = document.documentElement.clientWidth,
		  windowHeight = document.documentElement.clientHeight,
		  random = Math.random() * 10000000,
		  siteCategory = this.get_section();
		  logdata = dataGeneric + '||et=pageview||ID=||w=' + windowWidth + '||h=' + windowHeight + '||vp=' + '||iv=' + '||rc=' + '||ck=' + cookieValue + '||sn=' + cpo_constants.cpo_SESSIONCOOKIE + '||rn=' + random + '||cid=' + '||li=||cat='+siteCategory+'||plt='+cpo_constants.cpo_PVTIME;
		 theo_cpoGenerateTrackpix(logdata, 'pageview'); // -----> Function to log data in apachache access logs
	},
		
	/*************************************
	* Code for Getting IMG Latency
	*************************************/ 
	theo_cpoGetLatencyIMG: function (imgURL) {
		var timer = window.performance.getEntriesByName(imgURL);
		var imageLoadTime = timer[0]['duration'].toFixed(0);
		return parseInt(imageLoadTime);
	},
	
	/*************************************
	* Code for Getting Child Img ULR and pass it to get IMG Latency
	*************************************/ 
	theo_cpoGetChildIMG: function (divObj) {
		var ImgLatency=0;		
		var imgElements = divObj.getElementsByTagName('img');
		if(imgElements.length>0){
			for(var i=0;i<imgElements.length;i++){					
				ImgLatency = ImgLatency + this.theo_cpoGetLatencyIMG(imgElements[i].src); // IMG Source URL to find latency
			}
		}
		return ImgLatency;
		},
};

// Scroll function which count only one scroll in give seconds 
window.theo_scrollTm = new Date();
window.theo_checklastscroll = function(newTm){
  var diff = newTm - theo_scrollTm;
  if(diff > cpo_constants.cpo_SCROLLTIMETOTRACK) {
    theo_scrollTm = new Date();
    theo_cpoLogEvents('scroll');
  }
};


// ------------------------- SCROLL Logic ------------------------//
/*************************************
	 * Code for SCROLL Events || called when the window is scrolled.
	 *************************************/
// window.onscroll = function (e) { Commented by NAVEEN ON MAR 9 2017.. IS NOT WORKING ON THRM SITE
document.onscroll = function (e) {
	if(cpo_constants.cpo_TRACKSCROLL){
		theo_checklastscroll(new Date());
	}
	try {
		theo_videoPlayerMinimisedOnScrollWhenPlaying();
	} catch (error) {}
};
// ------------------------- SCROLL Logic End ------------------------//
function create(htmlStr) {
    var frag = document.createDocumentFragment(),
        temp = document.createElement('div');
    temp.innerHTML = htmlStr;
    while (temp.firstChild) {
        frag.appendChild(temp.firstChild);
    }
    return frag;
}

// ------------------------- DOCUMENT READY / WINDOW LOAD Track the viewebility Logic ------------------------//
window.onload = function (e) {
	// Forensiq Pixel Tag start
	var scr= "//c.fqtag.com/tag/implement-r.js?org=2rZ9BbL7BcrCBGGaykE7&s=theo_534343&rt=display&p=theotracker1234&fmt=banner&a=cpo_SubSource&cmp=cpo_campaign&fq=1&rd=URL&dmn="+theoEventTracker.site;
	var scrp = document.createElement('script');
	scrp.src = scr;
	scrp.async = true;
	document.getElementsByTagName('body')[0].appendChild(scrp);
	var fragment = create('<noscript> <img src="https://www.fqtag.com/pixel.cgi?org=2rZ9BbL7BcrCBGGaykE7&s=theo_534343&rt=displayImg&p=theotracker1234&fmt=banner&a=cpo_SubSource&cmp=cpo_campaign&fq=1&rd=URL&dmn="'+theoEventTracker.site+' width="1" height="1" border="0" /> </noscript>');
	document.body.insertBefore(fragment, document.body.firstChild);
	// Forensiq Pixel Tag end
	
   if(cpo_constants.CPO_COOKENAB === false){cpo_constants.cpo_SESSIONCOOKIE = "disabled";}else{cpo_constants.cpo_SESSIONCOOKIE = theoEventTracker.theo_cpoSetSessionCookie();}

   cpo_constants.cpo_PANELIDS = cpo_constants.cpo_PANELIDS.filter( function( item, index, inputArray ) {
           return inputArray.indexOf(item) == index;
	});	 	   
  cpo_constants.cpo_PANELIDS = cleanArray( cpo_constants.cpo_PANELIDS);	
  cpo_constants.cpo_UNIQUEIDS = cpo_constants.cpo_PANELIDS;	
  cpo_constants.cpo_UNIQUEIDS = cleanArray( cpo_constants.cpo_UNIQUEIDS);
  
  
  processDOM_elements('script');
  processDOM_elements('IFRAME');
  
   theo_cpoLogEvents('window_load');
  
	/* setInterval(function(){
		//theo_cpoLogEvents('viewtime');
	}
	, 1000); */
 
  setTimeout(function(){
    if(cpo_constants.cpo_ISADBLOCK) theo_detectWhichAdBlocked();
  },2000);
   
  // PAGE LOAD TIME
  setTimeout(function(){
    var t = performance.timing;
	cpo_constants.cpo_PVTIME = t.loadEventEnd - t.navigationStart;
	/*************************************
	 * Code for Page PAGE VIEW Function init on load
	*************************************/
	if(cpo_constants.cpo_TRACKPAGEVIEW){
		theoEventTracker.theo_cpoPageViews();
	}
	
  }, 0);
  
  
	// ------------------------- Click Logic ------------------------//	
	/*************************************
	 * Code for Ad Click Events
	 *************************************/
	if(cpo_constants.cpo_TRACKADCLICK){
		theoEventTracker.theo_cpoAttachClickEvent(cpo_constants.cpo_PANELIDS, 'id');
		theoEventTracker.theo_cpoAttachClickEvent(cpo_constants.cpo_CLASSNAME, 'classname');
	}
	// ------------------------- Click Logic End ------------------------//

	/*************************************
	 * Code for tracking mouse hovers( mousein and out time ) / mousehover_time
	 *************************************/
	if(cpo_constants.cpo_TRACKMOUSEHOVER){
		theoEventTracker.theo_cpoAttachOverOutEvent(cpo_constants.cpo_PANELIDS, 'id');
		theoEventTracker.theo_cpoAttachOverOutEvent(cpo_constants.cpo_CLASSNAME, 'classname');
	}
	
	/*************************************
	 * Code for Page TRACKED_CLICK Function init on load
	 *************************************/
	if(cpo_constants.cpo_TRACKMOUSECLICK){
		theo_cpoTrackedClick();
	}
	
	/*************************************
	 * Code for Page TRACK MOUSEMOVE Function init on load
	 *************************************/
	if(cpo_constants.cpo_TRACKMOUSEMOVE){
		theo_cpoTrackMouseMove();
	}

};
// ------------------------- DOCUMENT READY / WINDOW LOAD ------------------------//

/*************************************
 * Code for Page TRACKED_CLICK Function Definition || MOUSECLICK EVENT TRACKING
 *************************************/
function theo_cpoTrackedClick(){
    document.onclick = theo_cpoMouseClickHandler;

    function theo_cpoMouseClickHandler (e) {
	  e = e || window.event;
      var clickedElement = (window.event) ?
          window.event.srcElement :
          e.target,
          tags = document.getElementsByTagName(clickedElement.tagName);

      var element_id = clickedElement.id,
        element_className = +clickedElement.className,
        element_tagName = clickedElement.tagName,
        element_src = clickedElement.src;
      if (element_id === '' || element_id === null) {
        element_id = 'null';
      }
      if (element_className === '' || element_className === null) {
        element_className = 'null';
      }
      if (element_tagName === '' || element_tagName === null) {
        element_tagName = 'null';
      }
      if (element_src === '' || element_src === null) {
        element_src = 'null';
      }

      var xPos = 0,
          yPos = 0;
      xPos = (e.pageX) ? e.pageX : e.clientX + document.body.scrollLeft;
      yPos = (e.pageY) ? e.pageY : e.clientY + document.body.scrollTop;

      var data = 'tn=' + element_tagName + '||cn=' + element_className + '||es=' + element_src + '||cx=' + e.clientX + '||cy=' + e.clientY + '||px=' + xPos + '||py=' + yPos + '||mit=||mot=||tot=';

      theoEventTracker.theo_cpoLogClickData(data, 'tracked_click', element_id);
    }
}

/*************************************
 * Code for Page Mouse Moves Function Definition || MOUSEMOVE EVENT TRACKING
 *************************************/
function theo_cpoTrackMouseMove(){
    var theo_cpoMousePos, theo_cpoMousePosTemp, theo_cpoElementsData;

    document.onmousemove = theo_cpoMouseMoveHandler;
    var theo_cpoIntervalID = setInterval(theo_cpoGetMousePosition, cpo_constants.cpo_MOUSEINTERVAL); // setInterval repeats every X ms

    function theo_cpoMouseMoveHandler (event) {
      var dot, eventDoc, doc, body, pageX, pageY;
	  event = event || window.event;
      var mouseMoveElement = (window.event) ?
          window.event.srcElement :
          event.target,
          tags = document.getElementsByTagName(mouseMoveElement.tagName);

      var element_id = mouseMoveElement.id,
        element_className = +mouseMoveElement.className,
        element_tagName = mouseMoveElement.tagName,
        element_src = mouseMoveElement.src;

      if (element_id === '' || element_id === null) {
        element_id = 'null';
      }
      if (element_className === '' || element_className === null) {
        element_className = 'null';
      }
      if (element_tagName === '' || element_tagName === null) {
        element_tagName = 'null';
      }
      if (element_src === '' || element_src === null) {
        element_src = 'null';
      }      
      event = event || window.event; // IE-ism

      if (event.pageX === null && event.clientX !== null) {
        eventDoc = (event.target && event.target.ownerDocument) || document;
        doc = eventDoc.documentElement;
        body = eventDoc.body;

        event.pageX = event.clientX +
          (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
          (doc && doc.clientLeft || body && body.clientLeft || 0);
        event.pageY = event.clientY +
          (doc && doc.scrollTop || body && body.scrollTop || 0) -
          (doc && doc.clientTop || body && body.clientTop || 0);
      }

      theo_cpoElementsData = {
        id: element_id,
        className: element_className,
        tagName: element_tagName,
        src: element_src
      };
      theo_cpoMousePos = {
        pagx: event.pageX,
        pagy: event.pageY,
        clix: event.clientX,
        cliy: event.clientY
      };
    }

    function theo_cpoGetMousePosition () {
      var theo_cpoPos = theo_cpoMousePos;

      if (!theo_cpoPos) {
        // We haven't seen any movement yet       
      } else {
        // Use pos.x and pos.y        					
      }
      if (!theo_cpoMousePosTemp) {
        // do nothing        
      } else {
        if (theo_cpoMousePosTemp.pagx == theo_cpoPos.pagx && theo_cpoMousePosTemp.pagy == theo_cpoPos.pagy && theo_cpoMousePosTemp.clix == theo_cpoPos.clix && theo_cpoMousePosTemp.cliy == theo_cpoPos.cliy) {
          // console.log("Move not moved from long time -- Don't log mouse move data here");							
        } else {
          var data = 'tn=' + theo_cpoElementsData.tagName + '||cn=' + theo_cpoElementsData.className + '||es=' + theo_cpoElementsData.src + '||cx=' + theo_cpoPos.clix + '||cy=' + theo_cpoPos.cliy + '||px=' + theo_cpoPos.pagx + '||py=' + theo_cpoPos.pagy + '||mit=||mot=||tot=';
          theoEventTracker.theo_cpoLogClickData(data, 'pos_mousemove', theo_cpoElementsData.id);
        }
      }
      theo_cpoMousePosTemp = theo_cpoPos;
    }
}

function theo_cpoLogEvents (e) {
  // For IDS
  theoEventTracker.theo_cpoTrackedIDcalc(cpo_constants.cpo_PANELIDS, 'id', e);

  // For class
  if(cpo_constants.cpo_CLASSNAME.length == 1){
	  var theo_cpoTrackClass = document.getElementsByClassName(cpo_constants.cpo_CLASSNAME);
	  theoEventTracker.theo_cpoTrackedIDcalc(theo_cpoTrackClass, 'class', e);	  
  }  
  //for multiple class
  else{
	  for(var clCount=0;clCount < cpo_constants.cpo_CLASSNAME.length; clCount++) {
		var theo_cpoTrackClass = document.getElementsByClassName(cpo_constants.cpo_CLASSNAME[clCount]);		
		theoEventTracker.theo_cpoTrackedIDcalc(theo_cpoTrackClass, 'class', e);
	  }
  }
   // For Ad_Slots
  var domArry = theoEventTracker.theo_cpofindDomElements();
  var theo_cpofound_adIDS = theoEventTracker.theo_cpoFindmatchingDoms(domArry);
  theo_cpofound_adIDS = theo_cpofound_adIDS.filter( function( item, index, inputArray ) {
           return inputArray.indexOf(item) == index;
    });
  theoEventTracker.theo_cpoTrackedIDcalc(theo_cpofound_adIDS, 'ad_slot', e);
}

/* 	Generate tracking pixel | function theo_cpoGenerateTrackpix
		param: data
*/
function theo_cpoGenerateTrackpix (data, e) {
  var url = cpo_constants.cpo_SCROLLLOGURL; 
   var url = cpo_constants.cpo_SCROLLLOGURL; 
   var body = document.body,
    html = document.documentElement;
  var docHeight = Math.max( body.scrollHeight, body.offsetHeight, 
                       html.clientHeight, html.scrollHeight, html.offsetHeight );
	var docWidth = Math.max( body.scrollWidth, body.offsetWidth, 
                       html.clientWidth, html.scrollWidth, html.offsetWidth );
   data = data +'||dh='+docHeight+'||dw='+docWidth;

  if (e === 'scroll' || e === 'tracked_click' || e === 'Inactivewindow') {
    url = cpo_constants.cpo_SCROLLLOGURL;
  } else if (e === 'click' ) {
    url = cpo_constants.cpo_CLICKLOGURL;
  } else if (e === 'pos_mousemove' || e === 'mousehover_time' || e === 'mousein') {
    url = cpo_constants.cpo_MOUSEMOVELOGURL;
  } else if (e === 'window_load' ) {
    url = cpo_constants.cpo_IMPRESSIONLOGURL;
  } else if (e === 'visibleimpression') {
    url = cpo_constants.cpo_VIEWLOGURL;
  } else if (e === 'pageview') {
    url = cpo_constants.cpo_PAGEVIEWLOGURL;
  }else if (e === 'adblock') {
    url = cpo_constants.cpo_ADBLOCKLOGURL;
  }else if (e === 'videoplayer') {
    url = cpo_constants.cpo_VIDEOLOGURL;
  }else if (e === 'viewtime') {
    url = cpo_constants.cpo_VIEWTIME;
  }

  
  var image = new Image(1, 1);
  image.onLoad = function () {};
  image.src = url + '?' + data;
}

var mouse_inTime, mouse_outTime;
var timeoutId = null;

function theo_cpoMouseOver (e) {
  mouse_inTime = new Date();
  var mouseMoveElement = (window.event) ? window.event.srcElement : e.target;
  var element_id = mouseMoveElement.id,
    element_className = +mouseMoveElement.className,
    element_tagName = mouseMoveElement.tagName,
    element_src = mouseMoveElement.src;

  if (element_id === '' || element_id === null) {
    element_id = 'null';
  }
  if (element_className === '' || element_className === null) {
    element_className = 'null';
  }
  if (element_tagName === '' || element_tagName === null) {
    element_tagName = 'null';
  }
  if (element_src === '' || element_src === null) {
    element_src = 'null';
  }
  
  position= getPos(mouseMoveElement);		
	timeoutId = window.setTimeout(function(){
		var data = 'tn=' + element_tagName + '||cn=' + element_className + '||es=' + element_src + '||cx='+position.x+'||cy='+position.y+'px=||py=' + '||mit=' + mouse_inTime + '||mot=' + mouse_outTime + '||tot=';
		theoEventTracker.theo_cpoLogClickData(data, 'mousein', element_id);
   }, 2000);
}

function theo_cpoMouseOut (e, obj) {
  window.clearTimeout(timeoutId);
  mouse_outTime = new Date();
  var mouseMoveElement = (window.event) ? window.event.srcElement : e.target;
  var element_id = mouseMoveElement.id,
    element_className = mouseMoveElement.className,
    element_tagName = mouseMoveElement.tagName,
    element_src = mouseMoveElement.src;

  if (element_id === '' || element_id === null) {
    element_id = 'null';
  }
  if (element_className === '' || element_className === null) {
    element_className = 'null';
  }
  if (element_tagName === '' || element_tagName === null) {
    element_tagName = 'null';
  }
  if (element_src === '' || element_src === null) {
    element_src = 'null';
  }

  
	position= getPos(mouseMoveElement);
		
  var time_diff = (mouse_outTime - mouse_inTime) / 1000;
  var data = 'tn=' + element_tagName + '||cn=' + element_className + '||es=' + element_src + '||cx='+position.x+'||cy='+position.y+'||px=||py=' + '||mit=' + mouse_inTime + '||mot=' + mouse_outTime + '||tot=' + time_diff;
  theoEventTracker.theo_cpoLogClickData(data, 'mousehover_time', element_id);
}

/*************************************
 * Code for getting client ip
 *************************************/
function getIPs(callback){
    var ip_dups = {};
   var RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
    var useWebKit = !!window.webkitRTCPeerConnection;

    //bypass naive webrtc block using an iframe
    if(!RTCPeerConnection){       
        var win = iframe.contentWindow;
        RTCPeerConnection = win.RTCPeerConnection || win.mozRTCPeerConnection || win.webkitRTCPeerConnection;
        useWebKit = !!win.webkitRTCPeerConnection;
    }

    //minimal requirements for data connection
    var mediaConstraints = {
        optional: [{RtpDataChannels: true}]
    };

    var servers = {iceServers: [{urls: "stun:stun.services.mozilla.com"}]};

    //construct a new RTCPeerConnection
    var pc = new RTCPeerConnection(servers, mediaConstraints);

    function handleCandidate(candidate){
        //match just the IP address
        var ip_regex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/;
        var ip_addr = ip_regex.exec(candidate)[1];

        //remove duplicates
        if(ip_dups[ip_addr] === undefined)
            callback(ip_addr);

        ip_dups[ip_addr] = true;
    }

    //listen for candidate events
    pc.onicecandidate = function(ice){

        //skip non-candidate events
        if(ice.candidate)
            handleCandidate(ice.candidate.candidate);
    };

    //create a bogus data channel
    pc.createDataChannel("");

    //create an offer sdp
    pc.createOffer(function(result){

        //trigger the stun server request
        pc.setLocalDescription(result, function(){}, function(){});

    }, function(){});

    //wait for a while to let everything done
    setTimeout(function(){
        //read candidate info from local description
        var lines = pc.localDescription.sdp.split('\n');

        lines.forEach(function(line){
            if(line.indexOf('a=candidate:') === 0)
                handleCandidate(line);
        });
    }, 1000); 
}
// array for clientip
var clientIpArr = [];

//Forensic API call
function theo_forensicApiCall(){
  var ua = navigator.userAgent;
  for(var ipCnt=0;ipCnt < clientIpArr.length; ipCnt++) {
    var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
    //URL for sending request
    var forensicUrl = 'http://2pth.com/check?ua='+ ua +'&ck=XQzjvZPRul6sR4f0MtJl&output=json&rt=click&ip='+clientIpArr[ipCnt]+'&s='+cpo_constants.cpo_SESSIONCOOKIE+'&p=309&a=%20CD3547&cmp=2296';
    xobj.open('GET', forensicUrl, true); 
    xobj.onreadystatechange = getForensicData;
    xobj.send(null);
  }
}
function getForensicData(e) {
    if (xobj.readyState == 4 && xobj.status == "200") {      
    }
}

// Added function to get given node's index of its parentElement
var theo_getChildIndex = function(child){
    var parent = child.parentNode || child.parentElement;
    var children = parent.children;
    var i = children.length - 1;
    for (; i >= 0; i--){
        if (child == children[i]){
            break;
        }
    }
    return i;
};

/********************************
 * JW player tracking code 
 ********************************/
function theo_jwPlayerSetupWithrequiredTracker(){
  var theo_jwVideoPanel = document.querySelector('.jwplayer'),
      theo_vastadPostfix = '';
  if(theo_jwVideoPanel === null) {
    return;
  }

  theo_jwPlayerIns = jwplayer(theo_jwVideoPanel.id);
  
  var theo_jwVideoPlayerMain = theo_playerGenerateForMinNMaxView(theo_jwVideoPanel);
  theo_InitializeMinNMaxView(theo_jwVideoPlayerMain);

  // sample variable for tracking percentile 
  var theo_jwAdPercTracking = {
      '25': false,
      '50': false,
      '75': false
    },
    theo_jwAdCurrentTime=0,
    theo_jwAdTotalTime=0,
    theo_jwAdSkipShown = false,
    theo_jwVideoComplete = false;
  /*************************************
   * All jwplayer adevents listeners
   ************************************/
  theo_jwPlayerIns.on('adRequest adPlay adPause adTime adSkipped adImpression adClick adBlock adError adComplete adStarted adExpanded adResumed adVideoFirstQuartile adVideoMidPoint adVideoThirdQuartile mute', function (event) {
    if(event.type == 'adRequest'){
      theo_vastadPostfix = event.adposition +(( event.offset == "pre" ||  event.offset == "post" )? '' :  "_" + event.offset); 
      return;
    }
    
    if(event.type == 'adPlay') {
      var skipshownTimer = function(){
        if(theo_jwVideoPanel.querySelector('.jw-skippable') !== null && !theo_jwAdSkipShown) {
          theo_videoEventsTracker('skipshown', theo_jwVideoPlayerMain,theo_jwAdCurrentTime,theo_jwAdTotalTime,theo_vastadPostfix);
          theo_jwAdSkipShown = true;
        }else{
          setTimeout(skipshownTimer,200);
        }
      };
      setTimeout(skipshownTimer,200);
    }
    if(!cpo_constants.cpo_PLAYERTRACKER[event.type] && event.type !== 'mute') return;
    switch(event.type){      
      case 'adPlay' : 
          if (theo_jwVideoComplete) {
            theo_videoEventsTracker('rewind', theo_jwVideoPlayerMain,0,theo_jwAdTotalTime,theo_vastadPostfix);
            theo_jwVideoComplete = false;
          }
          theo_videoEventsTracker(event.type, theo_jwVideoPlayerMain,0,theo_jwAdTotalTime,theo_vastadPostfix);
          break;
      case 'adTime' :
          var perc = Math.round(event.position / event.duration * 100);
          theo_jwAdCurrentTime = event.position;
          theo_jwAdTotalTime = event.duration;
          if (perc >= 25 && !theo_jwAdPercTracking['25']) {
            theo_jwAdPercTracking['25'] = true;
            theo_videoEventsTracker('firstQuartile', theo_jwVideoPlayerMain,theo_jwAdCurrentTime,theo_jwAdTotalTime,theo_vastadPostfix);
          }
          if (perc >= 50 && !theo_jwAdPercTracking['50']) {
            theo_jwAdPercTracking['50'] = true;
            theo_videoEventsTracker('midpoint', theo_jwVideoPlayerMain,theo_jwAdCurrentTime,theo_jwAdTotalTime,theo_vastadPostfix);
            
          }
          if (perc >= 75 && !theo_jwAdPercTracking['75']) {
            theo_jwAdPercTracking['75'] = true;
            theo_videoEventsTracker('thirdQuartile', theo_jwVideoPlayerMain,theo_jwAdCurrentTime,theo_jwAdTotalTime,theo_vastadPostfix);
          } 
          break;
      case 'adComplete' :         
        theo_videoEventsTracker(event.type, theo_jwVideoPlayerMain,theo_jwAdCurrentTime,theo_jwAdTotalTime,theo_vastadPostfix);
        theo_jwAdPercTracking = {
          '25': false,
          '50': false,
          '75': false
        };
        theo_jwAdCurrentTime=0;
        theo_jwAdSkipShown = false;
        break;
      case 'mute' :
        if(cpo_constants.cpo_PLAYERTRACKER.playerMute) theo_videoEventsTracker((theo_jwPlayerIns.getMute() ? event.type : 'unmute'), theo_jwVideoPlayerMain,theo_jwPlayerIns.getPosition(),theo_jwPlayerIns.getDuration());
        break;
      default:
        theo_videoEventsTracker(event.type, theo_jwVideoPlayerMain,theo_jwAdCurrentTime,theo_jwAdTotalTime,theo_vastadPostfix);
    }
  });
  /************************************
   * Jwplayer content video events
   ***********************************/
  if(cpo_constants.cpo_PLAYERTRACKER.playerVideo) {
    var theo_jwContentVideoPercTracking = {
      '25': false,
      '50': false,
      '75': false
    };
    theo_jwPlayerIns.on('ready play pause seek fullscreen firstFrame complete time setupError error',function(event){
      switch (event.type) {
        case 'play' :
          if (theo_jwVideoComplete) {
            theo_videoEventsTracker('rewind', theo_jwVideoPlayerMain,theo_jwPlayerIns.getPosition(),theo_jwPlayerIns.getDuration());
            theo_jwVideoComplete = false;
          }
          theo_videoEventsTracker(event.type, theo_jwVideoPlayerMain,theo_jwPlayerIns.getPosition(),theo_jwPlayerIns.getDuration());
          break;
        case 'fullscreen':
          theo_videoEventsTracker((theo_jwPlayerIns.getFullscreen()? event.type : 'fullscreen-exit'), theo_jwVideoPlayerMain,theo_jwPlayerIns.getPosition(),theo_jwPlayerIns.getDuration());
          break;
        case 'time':
          var perc = Math.round(event.position / event.duration * 100);
          if (perc >= 25 && !theo_jwContentVideoPercTracking['25']) {
            theo_jwContentVideoPercTracking['25'] = true;
            theo_videoEventsTracker('contentVid25', theo_jwVideoPlayerMain,theo_jwPlayerIns.getPosition(),theo_jwPlayerIns.getDuration());
          }
          if (perc >= 50 && !theo_jwContentVideoPercTracking['50']) {
            theo_jwContentVideoPercTracking['50'] = true;
            theo_videoEventsTracker('contentVid50', theo_jwVideoPlayerMain,theo_jwPlayerIns.getPosition(),theo_jwPlayerIns.getDuration());
          }
          if (perc >= 75 && !theo_jwContentVideoPercTracking['75']) {
            theo_jwContentVideoPercTracking['75'] = true;
            theo_videoEventsTracker('contentVid75', theo_jwVideoPlayerMain,theo_jwPlayerIns.getPosition(),theo_jwPlayerIns.getDuration());
          }   
          break; 
        case 'complete':
          theo_jwContentVideoPercTracking = {
            '25': false,
            '50': false,
            '75': false
          };
          theo_jwVideoComplete = true;
          theo_videoEventsTracker(event.type, theo_jwVideoPlayerMain,theo_jwPlayerIns.getPosition(),theo_jwPlayerIns.getDuration());
          break;
        default:
          theo_videoEventsTracker(event.type, theo_jwVideoPlayerMain,theo_jwPlayerIns.getPosition(),theo_jwPlayerIns.getDuration());
          break;
      }
    });
  }
}


/********************************
 * Flow player tracking code 
 *******************************/
function theo_flowPlayerSetupWithrequiredTracker(theo_flowTrackerObject){
  var theo_flowVideoPanel = document.querySelector('.flowplayer');
  if(theo_flowVideoPanel === null) {
    return;
  }
  var theo_flowVideoId = theo_flowVideoPanel.id,
      theo_flowvideoduration;

  theo_flowPlayerIns = flowplayer(theo_flowVideoPanel);
  
  var theo_flowVideoPlayerMain = theo_playerGenerateForMinNMaxView(theo_flowVideoPanel);
  theo_InitializeMinNMaxView(theo_flowVideoPlayerMain);

  theo_flowVideoComplete = false;
  /*************************************
   * All flowplayer adevents listeners
   ************************************/
  theo_flowPlayerIns.on("mute ima_ad_loaded ima_ad_error", function(event, obj, ad)
  {
      switch (event.type) {
        case 'mute' : 
          if(cpo_constants.cpo_PLAYERTRACKER.playerMute) theo_videoEventsTracker((theo_flowPlayerIns.muted ? event.type : 'unmute'), theo_flowVideoPlayerMain,theo_flowPlayerIns.video.time,theo_flowPlayerIns.video.duration );
          break;
        case 'ima_ad_error' : 
          console.info("Ad error message: " + err.message);
          console.info("Ad error code: " + err.code);
          break;
        default:
          
      }
  });
  /************************************
   * flowplayer content video events
   ***********************************/
  if(cpo_constants.cpo_PLAYERTRACKER.playerVideo) {
    var theo_flowContentVideoPercTracking = {
        '25': false,
        '50': false,
        '75': false
      };
    theo_flowPlayerIns.on("pause finish fullscreen fullscreen-exit progress resume seek stop ", function(event, obj, video) {
      switch (event.type) {
        case 'resume' : 
          if (theo_flowVideoComplete) {
            theo_videoEventsTracker('rewind', theo_flowVideoPlayerMain,theo_flowPlayerIns.video.time,theo_flowPlayerIns.video.duration );
            theo_flowVideoComplete = false;
          }
          theo_videoEventsTracker(event.type, theo_flowVideoPlayerMain,theo_flowPlayerIns.video.time,theo_flowPlayerIns.video.duration );
          break;
        case 'progress' : 
          var perc = Math.round(video/obj.video.duration * 100);
          if (perc >= 25 && !theo_flowContentVideoPercTracking['25']) {
            theo_flowContentVideoPercTracking['25'] = true;
            theo_videoEventsTracker('contentVid25', theo_flowVideoPlayerMain,theo_flowPlayerIns.video.time,theo_flowPlayerIns.video.duration );
          }
          if (perc >= 50 && !theo_flowContentVideoPercTracking['50']) {
            theo_flowContentVideoPercTracking['50'] = true;
            theo_videoEventsTracker('contentVid50', theo_flowVideoPlayerMain,theo_flowPlayerIns.video.time,theo_flowPlayerIns.video.duration );
          }
          if (perc >= 75 && !theo_flowContentVideoPercTracking['75']) {
            theo_flowContentVideoPercTracking['75'] = true;
            theo_videoEventsTracker('contentVid75', theo_flowVideoPlayerMain,theo_flowPlayerIns.video.time,theo_flowPlayerIns.video.duration );
          }
          break;
        case 'finish' : 
          theo_flowContentVideoPercTracking = {
            '25': false,
            '50': false,
            '75': false
          };
          theo_flowVideoComplete = true;
          theo_videoEventsTracker(event.type, theo_flowVideoPlayerMain,theo_flowPlayerIns.video.time,theo_flowPlayerIns.video.duration );
          break;
        default: 
          theo_videoEventsTracker(event.type, theo_flowVideoPlayerMain,theo_flowPlayerIns.video.time,theo_flowPlayerIns.video.duration );
      }
    });
  }
}


/***********************************************************************
 * Generate structure for minimize and maximize videoplayer by given div
 ***********************************************************************/
 theo_playerGenerateForMinNMaxView = function(videoNode){
  var videoNodeIndex = theo_getChildIndex(videoNode);
  videoNodeParent = videoNode.parentElement || videoNode.parentNode;
  mainVideoNode = document.createElement('div');
  videoNodeOuter = document.createElement('div');
  videoNodeInner = document.createElement('div');
  mainVideoNode.id = 'theo_'+videoNode.id;
  videoNodeOuter.className = "theo-video-outer";
  videoNodeInner.className = "theo-video-inner";  
  videoNodeInner.appendChild(videoNode);
  videoNodeOuter.appendChild(videoNodeInner);
  mainVideoNode.appendChild(videoNodeOuter);
  videoNodeParent.insertBefore(mainVideoNode, videoNodeParent.children[videoNodeIndex]);
  cpo_constants.cpo_PANELIDS.push(mainVideoNode.id);
  return mainVideoNode;
};

var theo_videoPlayerInview = true;
theo_InitializeMinNMaxView = function(divObj) {  
  function theo_videoPlayerInviewFunc () {
    var isFlowPlayer = divObj.querySelectorAll('.flowplayer').length > 0,
        isJwPlayer = divObj.querySelectorAll('.jwplayer').length > 0,
        divVisiblePerc = theoEventTracker.theo_cpoCalculateVisibilityForDiv(divObj),
        theo_videoPlayerInview = (divVisiblePerc >= cpo_constants.cpo_ADVISIBILITY) ? true : false;
     theo_videoPlayerIsScrollTimeout = false;
    if(isFlowPlayer) {
      if ((!theo_flowPlayerIns.finished) && !theo_videoPlayerInview) {
        if(divObj.offsetWidth> 300) {
          divObj.getElementsByClassName('theo-video-inner')[0].classList.add('theo-player-minimize');
          theo_videoEventsTracker('player-minimize', divObj);
        }
      }else {
        if(divObj.getElementsByClassName('theo-video-inner')[0].classList.contains('theo-player-minimize')) {
          divObj.getElementsByClassName('theo-video-inner')[0].classList.remove('theo-player-minimize');
          theo_videoEventsTracker('player-maximize', divObj);
        }
      }
    }else if(isJwPlayer){
      if ((theo_jwPlayerIns.getState() != 'idle') && (theo_jwPlayerIns.getState() != 'complete') && !theo_videoPlayerInview) {
        if(theo_jwPlayerIns.getWidth() > 300) {
          theo_jwPlayerIns.resize(300, 180);
          divObj.getElementsByClassName('theo-video-inner')[0].classList.add('theo-player-minimize');
          theo_videoEventsTracker('player-minimize', divObj);
        }
      }else {
        if(theo_jwPlayerIns.getWidth() < 480) {
          theo_jwPlayerIns.resize(480, 270);
          divObj.getElementsByClassName('theo-video-inner')[0].classList.remove('theo-player-minimize');
          theo_videoEventsTracker('player-maximize', divObj);
        }
      }
    }    
  }


  var theo_videoPlayerIsScrollTimeout = false;
  window.theo_videoPlayerMinimisedOnScrollWhenPlaying = function() {
    if(!cpo_constants.cpo_PLAYERTRACKER.playerMinimize) return;
    if (theo_videoPlayerIsScrollTimeout) return;
    theo_videoPlayerIsScrollTimeout = true;
    theo_videoPlayerInviewFunc();
     setTimeout(function() {
      theo_videoPlayerIsScrollTimeout = false;
    }, 80);
  };
};
/****************************
 * Video Events tracker
 ***************************/
theo_videoEventsTracker = function (e, divObj,CurrTime, totTime, vastAdPosition) {
  if (divObj !== null && divObj !== undefined) {
    var data = theoEventTracker.logInfo(),
      logdata = '',
      random = Math.random() * 10000000,
      isVisible = 'NO',
      divVisiblePerc = theoEventTracker.theo_cpoCalculateVisibilityForDiv(divObj);
	  var position = getPos( divObj );
    if (divVisiblePerc >= cpo_constants.cpo_ADVISIBILITY) {isVisible = 'YES';}else {
      // get the non viewable reason code
    }
    var cpo_hms = '';
    cpo_hms = '||tn=||cn=||es=||cx='+position.x+'||cy='+position.y+'||px=||py=||mit=0||mot='+ (CurrTime !== undefined ? Math.round(CurrTime*100)/100 : 0 ) +'||tot='+( totTime !== undefined ? Math.round(totTime*100)/100 : 0);

    logdata = data + '||et=' + cpo_constants.cpo_VIDEOPLAYEREVENTMAPPING[e] + '||ID=' + divObj.id + (vastAdPosition !== undefined ? (vastAdPosition.length > 0  ? '_'+vastAdPosition : ' ') : '')  + '||w=' + divObj.offsetWidth + '||h=' + divObj.offsetHeight + '||vp=' + divVisiblePerc + '||iv=' + isVisible + '||rc=' + cpo_constants.CPO_NONVIEWREASON + '||ck=' + theoEventTracker.theo_cpoGenerateCookie() + '||sn=' + cpo_constants.cpo_SESSIONCOOKIE + '||rn=' + random + '||cid=||li='+cpo_hms;
	theo_cpoGenerateTrackpix(logdata, 'videoplayer');   //-----> Function to log data in apachache access logs
  }
};

/*************
 * Ad Blocking
 *************/
var theo_detectWhichAdBlocked = function () {
  var theo_adBlockLog = function (blockEl, blockElDim, blockIdClsScr, blockScrUrl) {
    var nonViewReason = 'R7';
    var random = Math.random() * 10000000,
        data = theoEventTracker.logInfo(),
        logdata;
      if(blockIdClsScr != 'ext' && blockIdClsScr != 'script' && blockIdClsScr != 'xhr'  && blockIdClsScr != 'img' && blockIdClsScr != 'css') {
        logdata = data + '||et=adblock||ID=' + blockIdClsScr + '||w=' + blockElDim.width + '||h=' + blockElDim.height + '||vp=0||iv=NO||rc=' + nonViewReason + '||ck=' + theoEventTracker.theo_cpoGenerateCookie() + '||sn=' + cpo_constants.cpo_SESSIONCOOKIE + '||rn=' + random + '||cid=' + getCreativeId(blockEl.id) + '||li=' + getLineItemId(blockEl.id) + getHEATMAPSTATS(divObj);
      }else{
        logdata = data + '||et=adblock'+ blockIdClsScr +'||ID=||w=0||h=0||vp=0||iv=NO||rc=' + nonViewReason + '||ck=' + theoEventTracker.theo_cpoGenerateCookie() + '||sn=' + cpo_constants.cpo_SESSIONCOOKIE + '||rn=' + random + '||cid='+ blockIdClsScr +'||li='+ blockIdClsScr + getHEATMAPSTATS(divObj);
        if (blockScrUrl !== undefined){
          logdata = logdata + '||bsu=' + window.btoa(blockScrUrl);
        }
      }         
   theo_cpoGenerateTrackpix(logdata, 'adblock');
  };

   setTimeout(function(){
    for(var theo_installedExtensionListCnt = 0; theo_installedExtensionListCnt < theo_installedExtensionListArr.length; theo_installedExtensionListCnt++ ){
      theo_adBlockLog('','', 'ext', theo_installedExtensionListArr[theo_installedExtensionListCnt]);
    }
  },10);

  //easylist array
  var blockElArr = [];
  //load easylist.txt
  var theo_xhr = new XMLHttpRequest();
  theo_xhr.open('GET', './js/easylist.txt', true);
  theo_xhr.onload = function () {
    if(theo_xhr.readyState === XMLHttpRequest.DONE && theo_xhr.status === 200) {
      var easyListArr = theo_xhr.responseText.split("\n"),
          easyListCnt = 0,
          easyListLen = easyListArr.length,
          pArr = processDOM_elements('div').searchTagArr,
          sArr = processDOM_elements('script').searchTagArr,
          imgArr = processDOM_elements('img').searchTagArr,
          cssArr = processDOM_elements('link').searchTagArr;

      for(easyListCnt;easyListCnt<easyListLen;easyListCnt++){
        //Search by IDs
        if(easyListArr[easyListCnt].indexOf("###")  !== -1){
          var pIdCnt = 0,
              eslId = easyListArr[easyListCnt].split("###")[1].toString();
          for(pIdCnt;pIdCnt<pArr.length;pIdCnt++){
            if(pArr[pIdCnt].tag !== undefined){
              if (pArr[pIdCnt].tag.id !== undefined && pArr[pIdCnt].tag.id.length > 0) {
                if(pArr[pIdCnt].tag.id.indexOf(eslId) !== -1){
                  var pElId = pArr[pIdCnt].tag,
                      pDimId = pElId.getBoundingClientRect();
                  theo_adBlockLog(pElId,pDimId, pElId.id);
                  pArr.splice(pIdCnt,1);
                  pIdCnt--;
                }
              }
            }
          }
        // Search by class
        } else if(easyListArr[easyListCnt].indexOf("##.")  !== -1){
          var pCnt = 0,
              esl = easyListArr[easyListCnt].split("##.")[1].toString();
          for(pCnt;pCnt<pArr.length;pCnt++){
            if(pArr[pCnt].tag !== undefined){
              if (pArr[pCnt].tag.className.length > 0) {
                if(pArr[pCnt].tag.className.indexOf(esl) !== -1){
                  var pEl = pArr[pCnt].tag,
                      pDim = pEl.getBoundingClientRect(),
                      pI = 0,
                      pExistFlg = false;
                  for(pI; pI < blockElArr.length; pI++) {
                    if(blockElArr[pI] == '.'+esl) {
                      pExistFlg = true;
                    }
                  }
                  if(!pExistFlg) {
                    blockElArr.push('.'+esl);
                    theo_adBlockLog(pEl,pDim, esl);
                    pArr.splice(pCnt,1);
                    pCnt--;
                  }
                }
              }
            }
          }
        // Search for image & css(stylesheet)
        } else if(easyListArr[easyListCnt].indexOf("||")  !== -1){
          var imgCnt = 0,
              cssCnt = 0,
              eslthirdpartyUrl = new RegExp(easyListArr[easyListCnt].split("||")[1].toString().split("^")[0],"g");
          for(imgCnt;imgCnt<imgArr.length;imgCnt++){
            if(imgArr[imgCnt].tag !== undefined){
              if(imgArr[imgCnt].tag.src !== undefined) {
                if(imgArr[imgCnt].tag.src.match(eslthirdpartyUrl) !== null) {
                  theo_adBlockLog('','', 'img', imgArr[imgCnt].tag.src);
                }
              }
            } 
          }
          for(cssCnt;cssCnt<cssArr.length;cssCnt++){
            if(cssArr[cssCnt].tag !== undefined){
              if(cssArr[cssCnt].tag.href !== undefined) {
                if(cssArr[cssCnt].tag.href.match(eslthirdpartyUrl) !== null) {
                  theo_adBlockLog('','', 'css', cssArr[cssCnt].tag.href);
                }
              }
            } 
          }
        // Search for script
        } else{
          var sCnt = 0,
              eslScrUrl = easyListArr[easyListCnt];
          for(sCnt;sCnt<sArr.length;sCnt++){
            if(sArr[sCnt].tag !== undefined){
              if(sArr[sCnt].tag.src !== undefined && sArr[sCnt].tag.src.length > 0) {
                if(sArr[sCnt].tag.src.toLowerCase().indexOf(eslScrUrl) != -1) {
                  theo_adBlockLog('','', 'script', sArr[sCnt].tag.src);
                }
              }
            } 
          }
        }
      }
    }
  };
  theo_xhr.send();
  // track XMLHttpRequest
  var theo_xhrCnt = 0,
      theo_xhrLen = theo_xhrArr.length;
  for(theo_xhrCnt;theo_xhrCnt < theo_xhrLen;theo_xhrCnt++) {
    theo_adBlockLog('','', 'xhr', theo_xhrArr[theo_xhrCnt]);
  }    
};
