<?php 
// include DB connection
//include('generalconfig.php');
include('unity_dashboard_reporting.php');
// check start and end date in post request
if($_POST && isset($_POST['start']) && isset($_POST['end'])){
	$start = $_POST['start'];
	$end = $_POST['end'];
	// sql statement to retrive approval and rejection data
	$sql = "SELECT approval.approval_count approval, rejection.rejection_count rejection, approval.response_date FROM
				(SELECT 
					CASE 
						WHEN TIME(response_date) BETWEEN '00:00:00' AND '13:59:59' 
							THEN DATE_ADD(DATE(response_date),INTERVAL -1 DAY) 
						ELSE DATE(response_date) 
					END AS response_date, COUNT(creative_id) AS approval_count 
				FROM creative_scan_decision
				WHERE STATUS='sent' AND scan_decision='Go' AND DATE(response_date) BETWEEN '$start' AND DATE_ADD('$end',INTERVAL 1 DAY)
				GROUP BY 
					CASE 
						WHEN TIME(response_date) BETWEEN '00:00:00' AND '13:59:59' 
							THEN DATE_ADD(DATE(response_date),INTERVAL -1 DAY) 
						ELSE DATE(response_date) 
					END) AS approval
			LEFT JOIN
				(SELECT 
					CASE 
						WHEN TIME(response_date) BETWEEN '00:00:00' AND '13:59:59' 
							THEN DATE_ADD(DATE(response_date),INTERVAL -1 DAY) 
						ELSE DATE(response_date) 
					END AS response_date, COUNT(creative_id) AS rejection_count 
				FROM creative_scan_decision
				WHERE STATUS='sent' AND scan_decision='No-Go' AND DATE(response_date) BETWEEN '$start' AND DATE_ADD('$end',INTERVAL 1 DAY)
				GROUP BY 
					CASE 
						WHEN TIME(response_date) BETWEEN '00:00:00' AND '13:59:59' 
							THEN DATE_ADD(DATE(response_date),INTERVAL -1 DAY) 
						ELSE DATE(response_date) 
					END) AS rejection
			ON CAST(approval.response_date AS DATE)=CAST(rejection.response_date AS DATE)";

	// execute query
	$result=$conn->query($sql);
	
	// get data from result query and return response
	if ($result->num_rows > 0) {
		$response_result = array();
	    // get data from each row
	    $count = 0;
	    while($input_row = $result->fetch_assoc()) {
	    	if ($count==0) {
	    		$count++;
	    		continue;
	    	}
	    	$count++;
	    	if ($count==$result->num_rows) {
	    		if (date_create($end)==date('Y-m-d',strtotime("-1 days"))) {
	    			//continue;
	    		}else{
	    			continue;
	    		}	
	    	}
	    	$data['approval'] = $input_row["approval"];
	    	$data['rejection'] = $input_row["rejection"];
	    	$data['response_date'] = $input_row["response_date"];
	    	$response_result[] = $data;  
	    }
	    echo json_encode($response_result);
	} else {
		// return empty data if data is not available in DB
		$data['approval'] = 0;
		$data['rejection'] = 0;
		$data['response_date'] = "No data";
		$response_result[] = $data;
		echo json_encode($response_result);
	}
	mysqli_close($conn);
}

?>