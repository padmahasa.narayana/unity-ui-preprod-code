<?php
//Note that ticket obj is initiated in tickets.php.
if(!defined('OSTSCPINC') || !$thisstaff || !is_object($ticket) || !$ticket->getId()) die('Invalid path');

//Make sure the staff is allowed to access the page.
if(!@$thisstaff->isStaff() || !$ticket->checkStaffAccess($thisstaff)) die('Access Denied');

//Re-use the post info on error...savekeyboards.org (Why keyboard? -> some people care about objects than users!!)
$info=($_POST && $errors)?Format::input($_POST):array();

//Auto-lock the ticket if locking is enabled.. If already locked by the user then it simply renews.
if($cfg->getLockTime() && !$ticket->acquireLock($thisstaff->getId(),$cfg->getLockTime()))
    $warn.=__('Unable to obtain a lock on the ticket');

//Get the goodies.
$dept  = $ticket->getDept();  //Dept
$staff = $ticket->getStaff(); //Assigned or closed by..
$user  = $ticket->getOwner(); //Ticket User (EndUser)
$team  = $ticket->getTeam();  //Assigned team.
$sla   = $ticket->getSLA();
$lock  = $ticket->getLock();  //Ticket lock obj
$id    = $ticket->getId();    //Ticket ID.

//Useful warnings and errors the user might want to know!
if ($ticket->isClosed() && !$ticket->isReopenable())
    $warn = sprintf(
            __('Current ticket status (%s) does not allow the end user to reply.'),
            $ticket->getStatus());
elseif ($ticket->isAssigned()
        && (($staff && $staff->getId()!=$thisstaff->getId())
            || ($team && !$team->hasMember($thisstaff))
        ))
    $warn.= sprintf('&nbsp;&nbsp;<span class="Icon assignedTicket">%s</span>',
            sprintf(__('Ticket is assigned to %s'),
                implode('/', $ticket->getAssignees())
                ));

if (!$errors['err']) {

    if ($lock && $lock->getStaffId()!=$thisstaff->getId())
        $errors['err'] = sprintf(__('This ticket is currently locked by %s'),
                $lock->getStaffName());
    elseif (($emailBanned=TicketFilter::isBanned($ticket->getEmail())))
        $errors['err'] = __('Email is in banlist! Must be removed before any reply/response');
    elseif (!Validator::is_valid_email($ticket->getEmail()))
        $errors['err'] = __('EndUser email address is not valid! Consider updating it before responding');
}

$unbannable=($emailBanned) ? BanList::includes($ticket->getEmail()) : false;

if($ticket->isOverdue())
    $warn.='&nbsp;&nbsp;<span class="Icon overdueTicket">'.__('Marked overdue!').'</span>';

?>
<table width="940" cellpadding="2" cellspacing="0" border="0">
    <tr>
        <td width="20%" class="has_bottom_border">
             <h2><a href="tickets.php?id=<?php echo $ticket->getId(); ?>"
             title="<?php echo __('Reload'); ?>"><i class="icon-refresh"></i>
             <?php echo sprintf(__('Ticket #%s'), $ticket->getNumber()); ?></a></h2>
        </td>
<!--        <td width="auto" class="flush-right has_bottom_border">
            <?php
            if ($thisstaff->canBanEmails()
                    || $thisstaff->canEditTickets()
                    || ($dept && $dept->isManager($thisstaff))) { ?>
            <span class="action-button pull-right" data-dropdown="#action-dropdown-more">
                <i class="icon-caret-down pull-right"></i>
                <span ><i class="icon-cog"></i> <?php echo __('More');?></span>
            </span>
            <?php
            }
            // Status change options
            echo TicketStatus::status_options();

            if ($thisstaff->canEditTickets()) { ?>
                <a class="action-button pull-right" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=edit"><i class="icon-edit"></i> <?php
                    echo __('Edit'); ?></a>
            <?php
            }
            if ($ticket->isOpen()
                    && !$ticket->isAssigned()
                    && $thisstaff->canAssignTickets()
                    && $ticket->getDept()->isMember($thisstaff)) {?>
                <a id="ticket-claim" class="action-button pull-right confirm-action" href="#claim"><i class="icon-user"></i> <?php
                    echo __('Claim'); ?></a>

            <?php
            }?>
            <span class="action-button pull-right" data-dropdown="#action-dropdown-print">
                <i class="icon-caret-down pull-right"></i>
                <a id="ticket-print" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=print"><i class="icon-print"></i> <?php
                    echo __('Print'); ?></a>
            </span>
            <div id="action-dropdown-print" class="action-dropdown anchor-right">
              <ul>
                 <li><a class="no-pjax" target="_blank" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=print&notes=0"><i
                 class="icon-file-alt"></i> <?php echo __('Ticket Thread'); ?></a>
                 <li><a class="no-pjax" target="_blank" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=print&notes=1"><i
                 class="icon-file-text-alt"></i> <?php echo __('Thread + Internal Notes'); ?></a>
              </ul>
            </div>
            <div id="action-dropdown-more" class="action-dropdown anchor-right">
              <ul>
                <?php
                 if($thisstaff->canEditTickets()) { ?>
                    <li><a class="change-user" href="#tickets/<?php
                    echo $ticket->getId(); ?>/change-user"><i class="icon-user"></i> <?php
                    echo __('Change Owner'); ?></a></li>
                <?php
                 }
                 if($thisstaff->canDeleteTickets()) {
                     ?>
                    <li><a class="ticket-action" href="#tickets/<?php
                    echo $ticket->getId(); ?>/status/delete"
                    data-href="tickets.php"><i class="icon-trash"></i> <?php
                    echo __('Delete Ticket'); ?></a></li>
                <?php
                 }
                if($ticket->isOpen() && ($dept && $dept->isManager($thisstaff))) {

                    if($ticket->isAssigned()) { ?>
                        <li><a  class="confirm-action" id="ticket-release" href="#release"><i class="icon-user"></i> <?php
                            echo __('Release (unassign) Ticket'); ?></a></li>
                    <?php
                    }

                    if(!$ticket->isOverdue()) { ?>
                        <li><a class="confirm-action" id="ticket-overdue" href="#overdue"><i class="icon-bell"></i> <?php
                            echo __('Mark as Overdue'); ?></a></li>
                    <?php
                    }

                    if($ticket->isAnswered()) { ?>
                    <li><a class="confirm-action" id="ticket-unanswered" href="#unanswered"><i class="icon-circle-arrow-left"></i> <?php
                            echo __('Mark as Unanswered'); ?></a></li>
                    <?php
                    } else { ?>
                    <li><a class="confirm-action" id="ticket-answered" href="#answered"><i class="icon-circle-arrow-right"></i> <?php
                            echo __('Mark as Answered'); ?></a></li>
                    <?php
                    }
                } ?>
                <li><a href="#ajax.php/tickets/<?php echo $ticket->getId();
                    ?>/forms/manage" onclick="javascript:
                    $.dialog($(this).attr('href').substr(1), 201);
                    return false"
                    ><i class="icon-paste"></i> <?php echo __('Manage Forms'); ?></a></li>

<?php           if($thisstaff->canBanEmails()) {
                     if(!$emailBanned) {?>
                        <li><a class="confirm-action" id="ticket-banemail"
                            href="#banemail"><i class="icon-ban-circle"></i> <?php echo sprintf(
                                Format::htmlchars(__('Ban Email <%s>')),
                                $ticket->getEmail()); ?></a></li>
                <?php
                     } elseif($unbannable) { ?>
                        <li><a  class="confirm-action" id="ticket-banemail"
                            href="#unbanemail"><i class="icon-undo"></i> <?php echo sprintf(
                                Format::htmlchars(__('Unban Email <%s>')),
                                $ticket->getEmail()); ?></a></li>
                    <?php
                     }
                }?>
              </ul>
            </div>
        </td> -->
    </tr>
</table> 
<table class="ticket_info" cellspacing="0" cellpadding="0" width="940" border="0">
    <tr>
        <td width="50%">
            <table border="0" cellspacing="" cellpadding="4" width="100%">
                <tr>
                    <th width="100"><?php echo __('Status');?>:</th>
                    <td><?php echo $ticket->getStatus(); ?></td>
                </tr>
                 <tr>
                    <th><?php echo __('Priority');?>:</th>
                    <td><?php echo $ticket->getPriority(); ?></td>
                </tr> 
                <tr>
                    <th><?php echo __('Submit Date');?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getSubmitDate()); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Resubmitted');?>:</th>
                    <td><?php echo ($ticket->getResubmitted())?'Yes':'No'; ?></td>
                </tr>
            </table>
        </td>
        <td width="50%" style="vertical-align:top">
            <table border="0" cellspacing="" cellpadding="4" width="100%">
                <tr>
                    <th width="100"><?php echo __('User'); ?>:</th>
                    <td><!--<a href="#tickets/<?php echo $ticket->getId(); ?>/user"
                        onclick="javascript:
                            $.userLookup('ajax.php/tickets/<?php echo $ticket->getId(); ?>/user',
                                    function (user) {
                                        $('#user-'+user.id+'-name').text(user.name);
                                        $('#user-'+user.id+'-email').text(user.email);
                                        $('#user-'+user.id+'-phone').text(user.phone);
                                        $('select#emailreply option[value=1]').text(user.name+' <'+user.email+'>');
                                    });
                            return false;
                            "><i class="icon-user"></i> <span id="user-<?php echo $ticket->getOwnerId(); ?>-name"
                            ><?php echo Format::htmlchars($ticket->getName());
                        ?></span></a> -->
                        <?php
			echo Format::htmlchars($ticket->getName());
                        if($user) {
                           /* echo sprintf('&nbsp;&nbsp;<a href="tickets.php?a=search&uid=%d" title="%s" data-dropdown="#action-dropdown-stats">(<b>%d</b>)</a>',
                                    urlencode($user->getId()), __('Related Tickets'), $user->getNumTickets());*/
			echo " (".$user->getNumTickets().")";
                        ?>
                            <div id="action-dropdown-stats" class="action-dropdown anchor-right">
                                <ul>
                                    <?php
                                    if(($open=$user->getNumOpenTickets()))
                                        echo sprintf('<li><a href="tickets.php?a=search&status=open&uid=%s"><i class="icon-folder-open-alt icon-fixed-width"></i> %s</a></li>',
                                                $user->getId(), sprintf(_N('%d Open Ticket', '%d Open Tickets', $open), $open));

                                    if(($closed=$user->getNumClosedTickets()))
                                        echo sprintf('<li><a href="tickets.php?a=search&status=closed&uid=%d"><i
                                                class="icon-folder-close-alt icon-fixed-width"></i> %s</a></li>',
                                                $user->getId(), sprintf(_N('%d Closed Ticket', '%d Closed Tickets', $closed), $closed));
                                    ?>
                                    <li><a href="tickets.php?a=search&uid=<?php echo $ticket->getOwnerId(); ?>"><i class="icon-double-angle-right icon-fixed-width"></i> <?php echo __('All Tickets'); ?></a></li>
                                    <li><a href="users.php?id=<?php echo
                                    $user->getId(); ?>"><i class="icon-user
                                    icon-fixed-width"></i> <?php echo __('Manage User'); ?></a></li>
<?php if ($user->getOrgId()) { ?>
                                    <li><a href="orgs.php?id=<?php echo $user->getOrgId(); ?>"><i
                                        class="icon-building icon-fixed-width"></i> <?php
                                        echo __('Manage Organization'); ?></a></li>
<?php } ?>
                                </ul>
                            </div>
                    <?php
                        }
                    ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Email'); ?>:</th>
                    <td>
                        <span id="user-<?php echo $ticket->getOwnerId(); ?>-email"><?php echo $ticket->getEmail(); ?></span>
                    </td>
                </tr>
                <!--<tr>
                    <th><?php echo __('Phone'); ?>:</th>
                    <td>
                        <span id="user-<?php echo $ticket->getOwnerId(); ?>-phone"><?php echo $ticket->getPhoneNumber(); ?></span>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Source'); ?>:</th>
                    <td><?php
                        echo Format::htmlchars($ticket->getSource());

                        if (!strcasecmp($ticket->getSource(), 'Web') && $ticket->getIP())
                            echo '&nbsp;&nbsp; <span class="faded">('.Format::htmlchars($ticket->getIP()).')</span>';
                        ?>
                    </td>
                </tr> -->
		<tr>
                    <th><?php echo __('Re-Submitted Date');?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getResubmitted()); ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br>
<table class="ticket_info" cellspacing="0" cellpadding="0" width="940" border="0">
    <tr>
        <td width="50%">
            <table cellspacing="0" cellpadding="4" width="100%" border="0">
                <?php
                if($ticket->isOpen()) { ?>
                <tr>
                    <th width="100"><?php echo __('Assigned To');?>:</th>
                    <td>
                        <?php
                        if($ticket->isAssigned())
                            echo Format::htmlchars(implode('/', $ticket->getAssignees()));
                        else
                            echo '<span class="faded">&mdash; '.__('Unassigned').' &mdash;</span>';
                        ?>
                    </td>
                </tr>
                <?php
                } else { ?>
                <tr>
                    <th width="100"><?php echo __('Closed By');?>:</th>
                    <td>
                        <?php
                        if(($staff = $ticket->getStaff()))
                            echo Format::htmlchars($staff->getName());
                        else
                            echo '<span class="faded">&mdash; '.__('Unknown').' &mdash;</span>';
                        ?>
                    </td>
                </tr>
                <?php
                } ?>
                <!-- <tr>
                    <th><?php echo __('SLA Plan');?>:</th>
                    <td><?php echo $sla?Format::htmlchars($sla->getName()):'<span class="faded">&mdash; '.__('None').' &mdash;</span>'; ?></td>
                </tr> -->
                <?php
                if($ticket->isOpen()){ ?>
                <tr>
                    <th><?php echo __('TAT');?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getEstDueDate()); ?></td>
                </tr>
                <?php
                }else { ?>
                <tr>
                    <th><?php echo __('Close Date');?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getCloseDate()); ?></td>
                </tr>
                <?php
                }
                ?>
            </table>
        </td>
        <td width="50%">
            <table cellspacing="0" cellpadding="4" width="100%" border="0">
                <!-- <tr>
                    <th width="100"><?php echo __('Help Topic');?>:</th>
                    <td><?php echo Format::htmlchars($ticket->getHelpTopic()); ?></td>
                </tr> -->
                <tr>
                    <th nowrap><?php echo __('Client Last Message');?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getLastMsgDate()); ?></td>
                </tr>
                <tr>
                    <th nowrap><?php echo __('Agent Last Response');?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getLastRespDate()); ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br>
<table class="ticket_info" cellspacing="0" cellpadding="0" width="940" border="0">
<?php
$idx = 0;
foreach (DynamicFormEntry::forTicket($ticket->getId()) as $form) {
    // Skip core fields shown earlier in the ticket view
    // TODO: Rewrite getAnswers() so that one could write
    //       ->getAnswers()->filter(not(array('field__name__in'=>
    //           array('email', ...))));
    $answers = array_filter($form->getAnswers(), function ($a) {
        return !in_array($a->getField()->get('name'),
                array('email','subject','name','priority'));
        });
    if (count($answers) == 0)
        continue;
    ?>
        <tr>
        <td colspan="2">
            <table cellspacing="0" cellpadding="4" width="100%" border="0">
            <?php foreach($answers as $a) {
                if (!($v = $a->display())) continue; ?>
                <tr>
                    <th width="100"><?php
    echo $a->getField()->get('label');
                    ?>:</th>
                    <td><?php
    echo $v;
                    ?></td>
                </tr>
                <?php } ?>
            </table>
        </td>
        </tr>
    <?php
    $idx++;
    } ?>
</table>
<div class="clear"></div>
<h2 style="padding:10px 0 5px 0; font-size:11pt;"><?php echo Format::htmlchars($ticket->getSubject()); ?></h2>
<?php
$tcount = $ticket->getThreadCount();
$tcount+= $ticket->getNumNotes();
?>
<ul id="threads">
    <li><a class="active" id="toggle_ticket_thread" href="#" style="cursor:default;text-decoration:none;"><?php echo sprintf(__('Ticket Thread (%d)'), $tcount); ?></a></li>
</ul>
<div id="ticket_thread">
    <?php
    $threadTypes=array('M'=>'message','R'=>'response', 'N'=>'note');
    /* -------- Messages & Responses & Notes (if inline)-------------*/
    $types = array('M', 'R', 'N');
    if(($thread=$ticket->getThreadEntries($types))) {
	$threadCount = 0;
       foreach($thread as $entry) { ?>
        <table class="thread-entry <?php echo $threadTypes[$entry['thread_type']]; ?>" cellspacing="0" cellpadding="1" width="940" border="0">
            <tr>
                <th colspan="4" width="100%">
                <div>
                    <span class="pull-left">
                    <span style="display:inline-block"><?php
                        echo Format::db_datetime($entry['created']);?></span>
                    <span style="display:inline-block;padding:0 1em" class="faded title"><?php
                        echo Format::truncate($entry['title'], 100); ?></span>
                    </span>
                    <span class="pull-right" style="white-space:no-wrap;display:inline-block">
                        <span style="vertical-align:middle;" class="textra"></span>
                        <span style="vertical-align:middle;"
                            class="tmeta faded title"><?php
                            echo Format::htmlchars($entry['name'] ?: $entry['poster']); ?></span>
                    </span>
                </div>
                </th>
            </tr>
            <?php $Dimension = $ticket->getDimensionType(); ?>
            <tr><td colspan="4" class="thread-body" id="thread-id-<?php
                echo $entry['id']; ?>">
                <?php if (($threadCount==0) && ($threadTypes[$entry['thread_type']])=='message') { 
                    echo "<b>Creative Dimension - Type : </b>".$Dimension."<br>"; 
                    echo "<b>Creative Tag Details:</b></br>";
		 echo "<input type=button value='Hide/Show' onclick='toggleFunction();'>";

                } $threadCount = $threadCount+1;?>
                
                <div id="toggleThread" <?php if($threadCount==1){ ?>style="display:none;" <?php } ?>><?php
		if((strpos($entry['body'], '<script') !== false) || (($threadTypes[$entry['thread_type']])=='message')){
                    echo htmlspecialchars($entry['body']); 
                }
                else{
                    echo $entry['body']->toHtml();
                }
               // echo htmlspecialchars($entry['body']); ?></div></td></tr>
            <?php
            if($entry['attachments']
                    && ($tentry = $ticket->getThreadEntry($entry['id']))
                    && ($urls = $tentry->getAttachmentUrls())
                    && ($links = $tentry->getAttachmentsLinks())) {?>
            <tr>
                <td class="info" colspan="4"><?php echo $tentry->getAttachmentsLinks(); ?></td>
            </tr> <?php
            }
            if ($urls) { ?>
                <script type="text/javascript">
                    $('#thread-id-<?php echo $entry['id']; ?>')
                        .data('urls', <?php
                            echo JsonDataEncoder::encode($urls); ?>)
                        .data('id', <?php echo $entry['id']; ?>);
                </script>
<?php
            } ?>
        </table>
        <?php
        if($entry['thread_type']=='M')
            $msgId=$entry['id'];
       }
    } else {
        echo '<p>'.__('Error fetching ticket thread - get technical help.').'</p>';
    }?>
</div>
<div class="clear" style="padding-bottom:10px;"></div>
<?php if($errors['err']) { ?>
    <div id="msg_error"><?php echo $errors['err']; ?></div>
<?php }elseif($msg) { ?>
    <div id="msg_notice"><?php echo $msg; ?></div>
<?php }elseif($warn) { ?>
    <div id="msg_warning"><?php echo $warn; ?></div>
<?php } ?>
 <?php $scanStatus = $ticket->getScanStatus();
        if($scanStatus!=null && $scanStatus!="add"){ ?>
<div id="response_options">
    <ul class="tabs">
        <?php
        if($thisstaff->canPostReply()) { ?>
        <li><a id="reply_tab" href="#reply"><?php echo __('Post Reply');?></a></li>
        <?php
        } ?>
       <!--  <li><a id="note_tab" href="#note"><?php echo __('Post Internal Note');?></a></li> -->
        <?php
        if($thisstaff->canTransferTickets()) { ?>
        <!-- <li><a id="transfer_tab" href="#transfer"><?php echo __('Department Transfer');?></a></li> -->
        <?php
        }

        if($thisstaff->canAssignTickets()) { ?>
        <li><a id="assign_tab" href="#assign"><?php echo $ticket->isAssigned()?__('Reassign Ticket'):__('Assign Ticket'); ?></a></li>
        <?php
        } 
        ?>
            <li><a href="#autocheck" id="auto_tab">Automated Check</a></li>
            <?php if(($ticket->getStaffId()) && (empty(array_intersect($ticket->getAutocheckValues(), unserialize(USER_SECURITY_ID))))){ ?>
                <li><a href="#manualcheck" id="manual_tab">Manual Check</a></li>
            <?php } ?>
        
    </ul>
    <?php
    if($thisstaff->canPostReply()) { ?>
    <form id="reply" action="tickets.php?id=<?php echo $ticket->getId(); ?>#reply" name="reply" method="post" enctype="multipart/form-data">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="msgId" value="<?php echo $msgId; ?>">
        <input type="hidden" name="a" value="reply">
        <span class="error"></span>
        <table style="width:100%" border="0" cellspacing="0" cellpadding="3">
           <tbody id="to_sec">
            <tr>
                <td width="120">
                    <label><strong><?php echo __('To'); ?>:</strong></label>
                </td>
                <td>
                    <?php
                    # XXX: Add user-to-name and user-to-email HTML ID#s
                    $to =sprintf('%s &lt;%s&gt;',
                            Format::htmlchars($ticket->getName()),
                            $ticket->getReplyToEmail());
                    $emailReply = (!isset($info['emailreply']) || $info['emailreply']);
                    ?>
                    <select id="emailreply" name="emailreply">
                        <option value="1" <?php echo $emailReply ?  'selected="selected"' : ''; ?>><?php echo $to; ?></option>
                        <option value="0" <?php echo !$emailReply ? 'selected="selected"' : ''; ?>
                        >&mdash; <?php echo __('Do Not Email Reply'); ?> &mdash;</option>
                    </select>
                </td>
            </tr>
            </tbody>
            <?php
            if(1) { //Make CC optional feature? NO, for now.
                ?>
            <!-- <tbody id="cc_sec"
                style="display:<?php echo $emailReply?  'table-row-group':'none'; ?>;">
             <tr>
                <td width="120">
                    <label><strong><?php echo __('Collaborators'); ?>:</strong></label>
                </td>
                <td>
                    <input type='checkbox' value='1' name="emailcollab" id="emailcollab"
                        <?php echo ((!$info['emailcollab'] && !$errors) || isset($info['emailcollab']))?'checked="checked"':''; ?>
                        style="display:<?php echo $ticket->getNumCollaborators() ? 'inline-block': 'none'; ?>;"
                        >
                    <?php
                    $recipients = __('Add Recipients');
                    if ($ticket->getNumCollaborators())
                        $recipients = sprintf(__('Recipients (%d of %d)'),
                                $ticket->getNumActiveCollaborators(),
                                $ticket->getNumCollaborators());

                    echo sprintf('<span><a class="collaborators preview"
                            href="#tickets/%d/collaborators"><span id="recipients">%s</span></a></span>',
                            $ticket->getId(),
                            $recipients);
                   ?>
                </td>
             </tr>
            </tbody> -->
            <?php
            } ?>
            <tbody id="resp_sec">
            <?php
            if($errors['response']) {?>
            <tr><td width="120">&nbsp;</td><td class="error"><?php echo $errors['response']; ?>&nbsp;</td></tr>
            <?php
            }?>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label><strong><?php echo __('Response');?>:</strong></label>
                </td>
                <td>
<?php if ($cfg->isCannedResponseEnabled()) { ?>
                    <select id="cannedResp" name="cannedResp">
                        <option value="0" selected="selected"><?php echo __('Select a canned response');?></option>
                       <!-- <option value='original'><?php echo __('Original Message'); ?></option>
                        <option value='lastmessage'><?php echo __('Last Message'); ?></option> -->
                        <?php
                        if(($cannedResponses=Canned::responsesByDeptId($ticket->getDeptId()))) {
                            echo '<option value="0" disabled="disabled">
                                ------------- '.__('Premade Replies').' ------------- </option>';
                            foreach($cannedResponses as $id =>$title)
                                echo sprintf('<option value="%d">%s</option>',$id,$title);
                        }
                        ?>
                    </select>
                    <br>
<?php } # endif (canned-resonse-enabled)
                    $signature = '';
                    switch ($thisstaff->getDefaultSignatureType()) {
                    case 'dept':
                        if ($dept && $dept->canAppendSignature())
                           $signature = $dept->getSignature();
                       break;
                    case 'mine':
                        $signature = $thisstaff->getSignature();
                        break;
                    } ?>
                    <input type="hidden" name="draft_id" value=""/>
                    <textarea name="response" id="response" cols="50"
                        data-draft-namespace="ticket.response"
                        data-signature-field="signature" data-dept-id="<?php echo $dept->getId(); ?>"
                        data-signature="<?php
                            echo Format::htmlchars(Format::viewableImages($signature)); ?>"
                        placeholder="<?php echo __(
                        'Start writing your response here. Use canned responses from the drop-down above'
                        ); ?>"
                        data-draft-object-id="<?php echo $ticket->getId(); ?>"
                        rows="9" wrap="soft"
                        class="richtext ifhtml draft draft-delete"><?php
                        echo $info['response']; ?></textarea>
                <div id="reply_form_attachments" class="attachments">
<?php
print $response_form->getField('attachments')->render();
?>
                </div>
                </td>
            </tr>
            <tr>
                <td width="120">
                    <label for="signature" class="left"><?php echo __('Signature');?>:</label>
                </td>
                <td>
                    <?php
                    $info['signature']=$info['signature']?$info['signature']:$thisstaff->getDefaultSignatureType();
                    ?>
                    <label><input type="radio" name="signature" value="none" checked="checked"> <?php echo __('None');?></label>
                    <?php
                    if($thisstaff->getSignature()) {?>
                    <label><input type="radio" name="signature" value="mine"
                        <?php echo ($info['signature']=='mine')?'checked="checked"':''; ?>> <?php echo __('My Signature');?></label>
                    <?php
                    } ?>
                    <?php
                    if($dept && $dept->canAppendSignature()) { ?>
                    <label><input type="radio" name="signature" value="dept"
                        <?php echo ($info['signature']=='dept')?'checked="checked"':''; ?>>
                        <?php echo sprintf(__('Department Signature (%s)'), Format::htmlchars($dept->getName())); ?></label>
                    <?php
                    } ?>
                </td>
            </tr>
            <tr>
                <td width="120">
                    <label><strong><?php echo __('Ticket Status');?>:</strong></label>
                </td>
                <td>
                    <select name="reply_status_id">
                    <?php
                    $responseValue = $ticket->getResponse();
                    $statusId = $info['reply_status_id'] ?: $ticket->getStatusId();
                    $states = array('open');
                    if($thisstaff->getQaLevel()=="level1" || $responseValue['submit_type']!="Final")
                        $states = array_merge($states, array('resolved'));
                    if ($thisstaff->canCloseTickets() && ($thisstaff->getQaLevel()=="level2" || $responseValue['submit_type']=="Final"))
                        $states = array_merge($states, array('closed'));

                    foreach (TicketStatusList::getStatuses(
                                array('states' => $states)) as $s) {
                        if (!$s->isEnabled()) continue;
                        $selected = ($statusId == $s->getId());
                        echo sprintf('<option value="%d" %s>%s%s</option>',
                                $s->getId(),
                                $selected
                                 ? 'selected="selected"' : '',
                                __($s->getName()),
                                $selected
                                ? (' ('.__('current').')') : ''
                                );
                    }
                    ?>
                    </select>
                </td>
            </tr>
         </tbody>
        </table>
        <p  style="padding:0 165px;">
            <input class="btn_sm" type="submit" value="<?php echo __('Post Reply');?>">
            <input class="btn_sm" type="reset" value="<?php echo __('Reset');?>">
        </p>
    </form>
    <?php
    } ?>
    <form id="note" action="tickets.php?id=<?php echo $ticket->getId(); ?>#note" name="note" method="post" enctype="multipart/form-data">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="locktime" value="<?php echo $cfg->getLockTime(); ?>">
        <input type="hidden" name="a" value="postnote">
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <?php
            if($errors['postnote']) {?>
            <tr>
                <td width="120">&nbsp;</td>
                <td class="error"><?php echo $errors['postnote']; ?></td>
            </tr>
            <?php
            } ?>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label><strong><?php echo __('Internal Note'); ?>:</strong><span class='error'>&nbsp;*</span></label>
                </td>
                <td>
                    <div>
                        <div class="faded" style="padding-left:0.15em"><?php
                        echo __('Note title - summary of the note (optional)'); ?></div>
                        <input type="text" name="title" id="title" size="60" value="<?php echo $info['title']; ?>" >
                        <br/>
                        <span class="error">&nbsp;<?php echo $errors['title']; ?></span>
                    </div>
                    <br/>
                    <div class="error"><?php echo $errors['note']; ?></div>
                    <textarea name="note" id="internal_note" cols="80"
                        placeholder="<?php echo __('Note details'); ?>"
                        rows="9" wrap="soft" data-draft-namespace="ticket.note"
                        data-draft-object-id="<?php echo $ticket->getId(); ?>"
                        class="richtext ifhtml draft draft-delete"><?php echo $info['note'];
                        ?></textarea>
                <div class="attachments">
<?php
print $note_form->getField('attachments')->render();
?>
                </div>
                </td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td width="120">
                    <label><?php echo __('Ticket Status');?>:</label>
                </td>
                <td>
                    <div class="faded"></div>
                    <select name="note_status_id">
                        <?php
                        $statusId = $info['note_status_id'] ?: $ticket->getStatusId();
                        $states = array('open');
                        if ($thisstaff->canCloseTickets())
                            $states = array_merge($states, array('closed'));
                        foreach (TicketStatusList::getStatuses(
                                    array('states' => $states)) as $s) {
                            if (!$s->isEnabled()) continue;
                            $selected = $statusId == $s->getId();
                            echo sprintf('<option value="%d" %s>%s%s</option>',
                                    $s->getId(),
                                    $selected ? 'selected="selected"' : '',
                                    __($s->getName()),
                                    $selected ? (' ('.__('current').')') : ''
                                    );
                        }
                        ?>
                    </select>
                    &nbsp;<span class='error'>*&nbsp;<?php echo $errors['note_status_id']; ?></span>
                </td>
            </tr>
        </table>

       <p  style="padding-left:165px;">
           <input class="btn_sm" type="submit" value="<?php echo __('Post Note');?>">
           <input class="btn_sm" type="reset" value="<?php echo __('Reset');?>">
       </p>
   </form>
    <?php
    if($thisstaff->canTransferTickets()) { ?>
    <form id="transfer" action="tickets.php?id=<?php echo $ticket->getId(); ?>#transfer" name="transfer" method="post" enctype="multipart/form-data">
        <?php csrf_token(); ?>
        <input type="hidden" name="ticket_id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="a" value="transfer">
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <?php
            if($errors['transfer']) {
                ?>
            <tr>
                <td width="120">&nbsp;</td>
                <td class="error"><?php echo $errors['transfer']; ?></td>
            </tr>
            <?php
            } ?>
            <tr>
                <td width="120">
                    <label for="deptId"><strong><?php echo __('Department');?>:</strong></label>
                </td>
                <td>
                    <?php
                        echo sprintf('<span class="faded">'.__('Ticket is currently in <b>%s</b> department.').'</span>', $ticket->getDeptName());
                    ?>
                    <br>
                    <select id="deptId" name="deptId">
                        <option value="0" selected="selected">&mdash; <?php echo __('Select Target Department');?> &mdash;</option>
                        <?php
                        if($depts=Dept::getDepartments()) {
                            foreach($depts as $id =>$name) {
                                if($id==$ticket->getDeptId()) continue;
                                echo sprintf('<option value="%d" %s>%s</option>',
                                        $id, ($info['deptId']==$id)?'selected="selected"':'',$name);
                            }
                        }
                        ?>
                    </select>&nbsp;<span class='error'>*&nbsp;<?php echo $errors['deptId']; ?></span>
                </td>
            </tr>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label><strong><?php echo __('Comments'); ?>:</strong><span class='error'>&nbsp;*</span></label>
                </td>
                <td>
                    <textarea name="transfer_comments" id="transfer_comments"
                        placeholder="<?php echo __('Enter reasons for the transfer'); ?>"
                        class="richtext ifhtml no-bar" cols="80" rows="7" wrap="soft"><?php
                        echo $info['transfer_comments']; ?></textarea>
                    <span class="error"><?php echo $errors['transfer_comments']; ?></span>
                </td>
            </tr>
        </table>
        <p style="padding-left:165px;">
           <input class="btn_sm" type="submit" value="<?php echo __('Transfer');?>">
           <input class="btn_sm" type="reset" value="<?php echo __('Reset');?>">
        </p>
    </form>
    <?php
    } ?>
    <?php
    if($thisstaff->canAssignTickets()) { ?>
    <form id="assign" action="tickets.php?id=<?php echo $ticket->getId(); ?>#assign" name="assign" method="post" enctype="multipart/form-data">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="a" value="assign">
        <table style="width:100%" border="0" cellspacing="0" cellpadding="3">

            <?php
            if($errors['assign']) {
                ?>
            <tr>
                <td width="120">&nbsp;</td>
                <td class="error"><?php echo $errors['assign']; ?></td>
            </tr>
            <?php
            } ?>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label for="assignId"><strong><?php echo __('Assignee');?>:</strong></label>
                </td>
                <td>
                    <select id="assignId" name="assignId">
                        <option value="0" selected="selected">&mdash; <?php echo __('Select an Agent OR a Team');?> &mdash;</option>
                        <?php
                        if ($ticket->isOpen()
                                && !$ticket->isAssigned()
                                && $ticket->getDept()->isMember($thisstaff))
                            echo sprintf('<option value="%d">'.__('Claim Ticket (comments optional)').'</option>', $thisstaff->getId());

                        $sid=$tid=0;

                        if ($dept->assignMembersOnly())
                            $users = $dept->getAvailableMembers();
                        else
                            $users = Staff::getAvailableStaffMembers();

                        if ($users) {
                            echo '<OPTGROUP label="'.sprintf(__('Agents (%d)'), count($users)).'">';
                            $staffId=$ticket->isAssigned()?$ticket->getStaffId():0;
                            foreach($users as $id => $name) {
                                if($staffId && $staffId==$id)
                                    continue;

                                if (!is_object($name))
                                    $name = new PersonsName($name);

                                $k="s$id";
                                echo sprintf('<option value="%s" %s>%s</option>',
                                        $k,(($info['assignId']==$k)?'selected="selected"':''), $name);
                            }
                            echo '</OPTGROUP>';
                        }

                        if(($teams=Team::getActiveTeams())) {
                            echo '<OPTGROUP label="'.sprintf(__('Teams (%d)'), count($teams)).'">';
                            $teamId=(!$sid && $ticket->isAssigned())?$ticket->getTeamId():0;
                            foreach($teams as $id => $name) {
                                if($teamId && $teamId==$id)
                                    continue;

                                $k="t$id";
                                echo sprintf('<option value="%s" %s>%s</option>',
                                        $k,(($info['assignId']==$k)?'selected="selected"':''),$name);
                            }
                            echo '</OPTGROUP>';
                        }
                        ?>
                    </select>&nbsp;<span class='error'>*&nbsp;<?php echo $errors['assignId']; ?></span>
                    <?php
                    if ($ticket->isAssigned() && $ticket->isOpen()) { ?>
                        <div class="faded"><?php echo sprintf(__('Ticket is currently assigned to %s'),
                            sprintf('<b>%s</b>', $ticket->getAssignee())); ?></div> <?php
                    } elseif ($ticket->isClosed()) { ?>
                        <div class="faded"><?php echo __('Assigning a closed ticket will <b>reopen</b> it!'); ?></div>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label><strong><?php echo __('Comments');?>:</strong><span class='error'>&nbsp;</span></label>
                </td>
                <td>
                    <textarea name="assign_comments" id="assign_comments"
                        cols="80" rows="7" wrap="soft"
                        placeholder="<?php echo __('Enter reasons for the assignment or instructions for assignee'); ?>"
                        class="richtext ifhtml no-bar"><?php echo $info['assign_comments']; ?></textarea>
                    <span class="error"><?php echo $errors['assign_comments']; ?></span><br>
                </td>
            </tr>
        </table>
        <p  style="padding-left:165px;">
            <input class="btn_sm" type="submit" value="<?php echo $ticket->isAssigned()?__('Reassign'):__('Assign'); ?>">
            <input class="btn_sm" type="reset" value="<?php echo __('Reset');?>">
        </p>
    </form>
    <?php
    } ?>
    <form id="autocheck" action="#" method="post" name="autocheck">
        <!-- <h4>User Security:</h4>
        <span>There is no issues found in User security</span>
        <h4>Creative and Advertiser Quality</h4>
        <table class="list" style="width:100%" border="0" cellspacing="1" cellpadding="3">
            <tr>
                <td>Offensive Content</td>
                <td>This may include: partial or full nudity and/or offensive keywords</td>
                <td>CAQ001</td>
                <td>No-Go</td>
            </tr>
            <tr>
                <td>Pop-up</td>
                <td>Pop-up banner/landing page over the user’s current window</td>
                <td>CAQ005</td>
                <td>No-Go</td>
            </tr>
        </table>
        <h4>Operational Verifications</h4>
        <table class="list" style="width:100%" border="0" cellspacing="1" cellpadding="3">
            <tr>
                <td>Creative File Size</td>
                <td>Max creative size: (Creative assets only) > 5 mb</td>
                <td>OV001</td>
                <td>No-Go</td>
            </tr>
            <tr>
                <td>1x1 Image</td>
                <td>creative only containing a blank image or a 1x1 pixel image is served (blanks)</td>
                <td>OV007</td>
                <td>No-Go</td>
            </tr>
        </table> -->
        <table class="list" style="width:100%" border="0" cellspacing="1" cellpadding="3">
            <tr>
                <th width="25%">Category</th>
                <th width="20%">Sub Category</th>
                <th width="30%">Scan Type</th>
                <th width="10%">Code</th>
                <th width="15%">Decision</th>
            </tr>
        <?php /* $autocheck=$ticket->getAutocheckValues(); 
         if($autocheck){ 
        foreach ($autocheck as $key => $value) {
            $scan_check = $ticket->getAutoScanCheck($key,$ticket->getSubject());*/
	$autocheck=$ticket->getAutocheckValues(); 
         if($autocheck){ 
        foreach ($autocheck as $key => $value) {
		if($value==null) continue;
	if(strpos($value, 'M') !== false) continue;
            $scan_check = $ticket->getAutoScanCheck($value); ?>
            <tr>
                <td><?php echo $scan_check[0]; ?></td>
                <td><?php echo $scan_check[1]; ?></td>
                <td><?php echo $scan_check[2]; ?></td>
                <td><?php echo $scan_check[3]; ?></td>
                <td><?php echo 'No-Go'; ?></td>
            </tr>

        <?php }}else { ?>
            <tr><td colspan="5" align="center"><?php echo "There is no Error in Creative to Display";?></td></tr>
            <?php } ?>
        </table>
    </form>
    <?php $manualValue = $ticket->getScanValues();
          $responseValue = $ticket->getResponse();
     ?>
    <form id="manualcheck" name="manualcheck" action="tickets.php?id=<?php echo $ticket->getId(); ?>#manualcheck" method="post" onsubmit="return formcheck();">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="a" value="manualcheck">
        <input type="hidden" name="submit_type" value="<?php echo ($manualValue || $responseValue['submit_type']!='')?'Final':'Initial';?>">
        <table class="list" style="width:100%" border="0" cellspacing="1" cellpadding="3">
            <tr>
                <th hidden width="5%"></th>
                <th width="30%" style="align:left;">Category</th>
                <th width="40%">Scan Type</th>
                <th width="25%">Decision</th>
            </tr>
            <?php $decision=$ticket->getScanDecision('M',DISPLAY_IFRAME);?>
            
            <?php $decision=$ticket->getScanDecision('M',AD_INTERACTIVE);?>
            <tr>
                <td hidden><input hidden disabled type="checkbox" name="interactiveCheck" checked value="interactiveCheck"></td>
                <td>Display</td>
                <td>Is Creative Responsive</td>
                <td><input type="radio" name="isInteractive" <?php if($responseValue['is_ad_interactive']==1){ ?>checked <?php } ?> value="1">Yes
                    &nbsp;&nbsp;&nbsp;<input type="radio" name="isInteractive" <?php if($responseValue['is_ad_interactive']==0){ ?>checked <?php } ?> value="0">No</td>
            </tr>
            <!-- <tr>
                <td><input disabled type="checkbox" name="urlSecureCheck" checked value="urlSecureCheck"></td>
                <td>Display</td>
                <td>Is URL Secure</td>
                <td><input type="radio" name="urlSecure" <?php if($responseValue['is_url_secure']==1){ ?>checked <?php } ?> value="1">Yes
                    &nbsp;&nbsp;&nbsp;<input type="radio" name="urlSecure" <?php if($responseValue['is_url_secure']==0){ ?>checked <?php } ?> value="0">No</td>
            </tr> -->
            <?php
                $scan = $ticket->getScanChecks();
                foreach ($scan as $key => $value) { 
                    $decision=$ticket->getScanDecision('M',$value[0]);?>
                <tr>
                    <td hidden disabled><input class="checkbox" type="checkbox" name="scan_ids[]" <?php if((in_array($value[0],$manualValue))){ ?> checked <?php } ?> value="<?php echo $value[0];?>"></td>
                    <?php
                    foreach ($value as $key => $val) { 
                            if($key == 0)
                                continue;?>
                          <td><?php echo $val; ?></td>                     
                    <?php } ?>
                    <td><input type="radio" class="radio" name="decision<?php echo $value[0];?>" onclick="toggleCheckbox(this.value);" <?php if(!in_array($value[0],$manualValue)|| ($responseValue['scan_language']=="")){ ?>checked <?php } ?> value="Go">Go
                    &nbsp;&nbsp;&nbsp;<input type="radio" class="radio" name="decision<?php echo $value[0];?>" onclick="toggleCheckbox(this.value);" <?php if((in_array($value[0],$manualValue))){ ?>checked <?php } ?>  value="No-Go">No-Go</td>
                </tr>
              <?php  }
            ?>
            <tr>
                <td hidden><input hidden disabled type="checkbox" name="iframeCheck" checked value="iframeCheck"></td>
                <td>Creative Asset</td>
                <td>Creative has iFrame</td>
                <td><input type="radio" name="iframe" <?php if($responseValue['is_iframe']==1){ ?>checked <?php } ?> value="1">Yes
                    &nbsp;&nbsp;&nbsp;<input type="radio" name="iframe" <?php if($responseValue['is_iframe']==0){ ?>checked <?php } ?> value="0">No</td>
            </tr>
        </table>
        <?php
            //$manual_values = $ticket->getManualScanValues();
        ?>
        <table>
	<span>Creative Information :</span>
            <tr>
                <td>Language <font class="error">*</font> :</td>
                <td width="250px;"><select name="language">
                    <option value="English" <?php if($responseValue['scan_language']=="English"){?> selected <?php } ?>>English</option>
                    <option value="Non-English" <?php if($responseValue['scan_language']=="Non-English"){?> selected <?php } ?>>Non-English</option>
                </select>
                <!-- <input type="text" <?php if($responseValue){ ?> value="<?php echo $responseValue['scan_language']; ?>" <?php } ?> name="language" id="language"> --></td>
            <td>Object Errors : </td>
            <td><textarea name="obj_error" rows="3" cols="20" style="width:300px !important;"><?php echo $responseValue['object_error']; ?></textarea></td>
        </tr>
            <tr>
                <td>Category 1 - Tier 1 :</td>
                <td><select name="specific_vertical" id="specific_vertical" onchange="loadType('type');">
                <option value="">-- Select Tier1 --</option>
                <?php $tier = $ticket->getTier();
                while ($vertical = db_fetch_array($tier)) { ?>
                    <option value="<?php echo $vertical['id'];?>" <?php if($ticket->getSpecificTierValue('specific_tier',$responseValue['category1_scan_vertical'])==$vertical['id']) { ?> selected <?php } ?> ><?php echo $vertical['name'];?></option>
                <?php }
                ?>
                    </select>
                </td>
                <td>Category 1 - Tier 2 :</td>
                <td><select name="type" id="type">
                        <option value="">-- Select Tier2 --</option>
                            <?php $tier2 = $ticket->getTier2Values($ticket->getSpecificTierValue('specific_tier',$responseValue['category1_scan_vertical'])); 
                                foreach ($tier2 as $key => $value) {?>
                                    <option value="<?php echo $key; ?>" <?php if($ticket->getSpecificTierValue('specific_vertical',$responseValue['category1_scan_sub_vertical'])==$key) { ?> selected <?php } ?>><?php echo $value;?></option>
                               <?php }?>
                    </select>
                </td>
            </tr> 
            <tr>
                <td>Category 2 - Tier 1:</td>
                <td><select name="specific_vertical2" id="specific_vertical2" onchange="loadType('type2');">
                <option value="">-- Select Tier1 --</option>
                <?php $tier = $ticket->getTier();
                while ($vertical = db_fetch_array($tier)) { ?>
                    <option value="<?php echo $vertical['id'];?>" <?php if($ticket->getSpecificTierValue('specific_tier',$responseValue['category2_scan_vertical'])==$vertical['id']) { ?> selected <?php } ?> ><?php echo $vertical['name'];?></option>
                <?php }
                ?>
                    </select>
                </td>
                <td>Category 2 - Tier 2:</td>
                <td><select name="type2" id="type2">
                        <option value="">-- Select Tier2 --</option>
                            <?php $tier2 = $ticket->getTier2Values($ticket->getSpecificTierValue('specific_tier',$responseValue['category2_scan_vertical'])); 
                                foreach ($tier2 as $key => $value) {?>
                                    <option value="<?php echo $key; ?>" <?php if($ticket->getSpecificTierValue('specific_vertical',$responseValue['category2_scan_sub_vertical'])==$key) { ?> selected <?php } ?>><?php echo $value;?></option>
                               <?php }?>
                    </select>
                </td>
                
            </tr>
	</table>
                <hr>
            <table> 
            <tr>
                <td>Operational Verification : </td>
               <td>Creative Initial File Size : <?php echo ($responseValue['creative_initial_file_size'])?$responseValue['creative_initial_file_size']." Kb":'NA'; ?></td>
               <td>Click Here to Open MRAID Tester :</td>
                <td><a href="http://webtester.mraid.org/" target="_blank">http://webtester.mraid.org/</a></td>
            </tr>
            <tr>
                <td></td>
                <td>Creative File Size : <?php echo ($responseValue['creative_file_size'])?$responseValue['creative_file_size']." Kb":"NA"; ?></td>
                <td>Click Here to Open VAST Inspector :</td>
                <td><a href="http://zutils.zedo.com/vastvalidator/#/vastInspector" target="_blank">http://zutils.zedo.com/vastvalidator/#/vastInspector</a></td>
            </tr>
            <tr>
                <td></td>
                <td>Creative Ad Load Time : <?php echo ($responseValue['ad_load_time'])?$responseValue['ad_load_time']." Secs":"NA"; ?></td>
            </tr>
            <tr>
                <td></td>
                <td>Creative Video Max Length : <?php echo ($responseValue['video_max_length'])?$responseValue['video_max_length']." Secs":"NA"; ?></td>
            </tr>
            <tr>
                <td></td>
                <td>Creative Video Frame Rate : <?php echo ($responseValue['video_frame_rate'])?$responseValue['video_frame_rate']." fps":"NA"; ?></td>
            </tr>
            <tr>
                <td></td>
                <td>Creative Video Bit Rate : <?php echo ($responseValue['video_bit_rate'])?$responseValue['video_bit_rate']." kbps":"NA"; ?></td>
            </tr>
	   <!-- <tr>
                <td></td>
                <td>Creative Vast Video Bit Rate : <?php echo ($responseValue['vast_video_bitrate'])?$responseValue['vast_video_bitrate']." kbps":"NA"; ?></td>
            </tr> -->
           <!-- <tr>
                <td></td>
                <td>Creative Vast File Size : <?php echo ($responseValue['vast_file_size'])?$responseValue['vast_file_size']." Kb":"NA"; ?></td>
            </tr>
        
            <tr>
                <td></td>
                <td>Creative Vast Load From Start : <?php echo ($responseValue['vast_load_time'])?$responseValue['vast_load_time']." Secs":"NA"; ?></td>
            </tr> -->
            <tr><td>Dimension - Type : </td>
                <td><?php echo $Dimension;?></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><input type="submit" name="submit" value="Submit"></td>
            </tr>
        </table>
    </form>
</div>
<?php
	 if(!$ticket->getStaffId()){
        echo "<br><p align='center'><strong>Ticket needs to be assigned to see the Manual Check Tab</strong></p>";
    }
 }else{
    echo "<br><br><p align='center'><strong>Auto Check is not yet done for this Creative. Please check after some time</strong></p>";
} ?>
<div style="display:none;" class="dialog" id="print-options">
    <h3><?php echo __('Ticket Print Options');?></h3>
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <form action="tickets.php?id=<?php echo $ticket->getId(); ?>" method="post" id="print-form" name="print-form">
        <?php csrf_token(); ?>
        <input type="hidden" name="a" value="print">
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <fieldset class="notes">
            <label class="fixed-size" for="notes"><?php echo __('Print Notes');?>:</label>
            <input type="checkbox" id="notes" name="notes" value="1"> <?php echo __('Print <b>Internal</b> Notes/Comments');?>
        </fieldset>
        <fieldset>
            <label class="fixed-size" for="psize"><?php echo __('Paper Size');?>:</label>
            <select id="psize" name="psize">
                <option value="">&mdash; <?php echo __('Select Print Paper Size');?> &mdash;</option>
                <?php
                  $psize =$_SESSION['PAPER_SIZE']?$_SESSION['PAPER_SIZE']:$thisstaff->getDefaultPaperSize();
                  foreach(Export::$paper_sizes as $v) {
                      echo sprintf('<option value="%s" %s>%s</option>',
                                $v,($psize==$v)?'selected="selected"':'', __($v));
                  }
                ?>
            </select>
        </fieldset>
        <hr style="margin-top:3em"/>
        <p class="full-width">
            <span class="buttons pull-left">
                <input type="reset" value="<?php echo __('Reset');?>">
                <input type="button" value="<?php echo __('Cancel');?>" class="close">
            </span>
            <span class="buttons pull-right">
                <input type="submit" value="<?php echo __('Print');?>">
            </span>
         </p>
    </form>
    <div class="clear"></div>
</div>
<div style="display:none;" class="dialog" id="confirm-action">
    <h3><?php echo __('Please Confirm');?></h3>
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <p class="confirm-action" style="display:none;" id="claim-confirm">
        <?php echo __('Are you sure you want to <b>claim</b> (self assign) this ticket?');?>
    </p>
    <p class="confirm-action" style="display:none;" id="answered-confirm">
        <?php echo __('Are you sure you want to flag the ticket as <b>answered</b>?');?>
    </p>
    <p class="confirm-action" style="display:none;" id="unanswered-confirm">
        <?php echo __('Are you sure you want to flag the ticket as <b>unanswered</b>?');?>
    </p>
    <p class="confirm-action" style="display:none;" id="overdue-confirm">
        <?php echo __('Are you sure you want to flag the ticket as <font color="red"><b>overdue</b></font>?');?>
    </p>
    <p class="confirm-action" style="display:none;" id="banemail-confirm">
        <?php echo sprintf(__('Are you sure you want to <b>ban</b> %s?'), $ticket->getEmail());?> <br><br>
        <?php echo __('New tickets from the email address will be automatically rejected.');?>
    </p>
    <p class="confirm-action" style="display:none;" id="unbanemail-confirm">
        <?php echo sprintf(__('Are you sure you want to <b>remove</b> %s from ban list?'), $ticket->getEmail()); ?>
    </p>
    <p class="confirm-action" style="display:none;" id="release-confirm">
        <?php echo sprintf(__('Are you sure you want to <b>unassign</b> ticket from <b>%s</b>?'), $ticket->getAssigned()); ?>
    </p>
    <p class="confirm-action" style="display:none;" id="changeuser-confirm">
        <span id="msg_warning" style="display:block;vertical-align:top">
        <?php echo sprintf(Format::htmlchars(__('%s <%s> will longer have access to the ticket')),
            '<b>'.Format::htmlchars($ticket->getName()).'</b>', Format::htmlchars($ticket->getEmail())); ?>
        </span>
        <?php echo sprintf(__('Are you sure you want to <b>change</b> ticket owner to %s?'),
            '<b><span id="newuser">this guy</span></b>'); ?>
    </p>
    <p class="confirm-action" style="display:none;" id="delete-confirm">
        <font color="red"><strong><?php echo __('Are you sure you want to DELETE this ticket?');?></strong></font>
        <br><br><?php echo __('Deleted data CANNOT be recovered, including any associated attachments.');?>
    </p>
    <div><?php echo __('Please confirm to continue.');?></div>
    <form action="tickets.php?id=<?php echo $ticket->getId(); ?>" method="post" id="confirm-form" name="confirm-form">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="a" value="process">
        <input type="hidden" name="do" id="action" value="">
        <hr style="margin-top:1em"/>
        <p class="full-width">
            <span class="buttons pull-left">
                <input type="button" value="<?php echo __('Cancel');?>" class="close">
            </span>
            <span class="buttons pull-right">
                <input type="submit" value="<?php echo __('OK');?>">
            </span>
         </p>
    </form>
    <div class="clear"></div>
</div>
<script type="text/javascript">
$(function() {
    $(document).on('click', 'a.change-user', function(e) {
        e.preventDefault();
        var tid = <?php echo $ticket->getOwnerId(); ?>;
        var cid = <?php echo $ticket->getOwnerId(); ?>;
        var url = 'ajax.php/'+$(this).attr('href').substr(1);
        $.userLookup(url, function(user) {
            if(cid!=user.id
                    && $('.dialog#confirm-action #changeuser-confirm').length) {
                $('#newuser').html(user.name +' &lt;'+user.email+'&gt;');
                $('.dialog#confirm-action #action').val('changeuser');
                $('#confirm-form').append('<input type=hidden name=user_id value='+user.id+' />');
                $('#overlay').show();
                $('.dialog#confirm-action .confirm-action').hide();
                $('.dialog#confirm-action p#changeuser-confirm')
                .show()
                .parent('div').show().trigger('click');
            }
        });
    });
<?php
    // Set the lock if one exists
    if ($lock) { ?>
!function() {
  var setLock = setInterval(function() {
    if (typeof(window.autoLock) === 'undefined')
      return;
    clearInterval(setLock);
    autoLock.setLock({
      id:<?php echo $lock->getId(); ?>,
      time: <?php echo $cfg->getLockTime(); ?>}, 'acquire');
  }, 50);
}();
<?php } ?>
});
</script>

<script>
$("input.radio").click(function() {
    var $this = $(this);
    if($this.val()=="Go"){
        $this.closest('tr').find('td').first().find('input').first().prop('checked',false);
    }
    else{
        $this.closest('tr').find('td').first().find('input').first().prop('checked',true);
    }
    
});

function toggleCheckbox(value){

    if (value == "Go") {
        $(this).closest('tr').find('td').first().attr('disabled');
    }
    else{
        $(this).prev(".checkbox").prop('checked',true);
    }
}

function loadType(id)
{   
    var f = document.forms['manualcheck'];
    if(id=="type")
        var tier_id = f.specific_vertical.value;
    else
        var tier_id = f.specific_vertical2.value;
    var xmlhttp;
    
    if (window.XMLHttpRequest)
    {
        xmlhttp=new XMLHttpRequest(); // code for IE7+, Firefox, Chrome, Opera, Safari
    }
    else
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
    }
    xmlhttp.onreadystatechange=function()
    {    
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById(id).innerHTML=xmlhttp.responseText;
            //document.getElementById('subjectDiv').innerHTML=default_subject;
            /*if($("#deptId :selected").text().toLowerCase() == 'it')
            {               
                document.getElementById("dept_hidden").value = $("#deptId :selected").text().toLowerCase();
                $("#category_id").hide();               
            }     
            else                
                $("#category_id").show();  */         
        }
    }

        var u = "../fetchprocess.php?type=tier&id="+tier_id;
    
    //var u = "../fetchprocess.php?dept_id="+dept_id+"&cat_id="+id+"&act=category";
    xmlhttp.open("GET",u,true);
    xmlhttp.send(); 
}

function formcheck(){
    var f = document.forms[5];

        if (f.iframe.value == "") {
            alert("Please Select Creative Has iFrame - Yes or No");
            return false;
        }
        if (f.isInteractive.value == "") {
            alert("Please Select Is Creative Responsive - Yes or No");
            return false;
        }
       /* if (f.urlSecure.value == "") {
            alert("Please Select Is URL Secure - Yes or No");
            return false;
        }*/
        if (f.language.value == "") {
            alert("Please Enter Language");
            return false;
        }
       /* if (f.specific_vertical.value == "") {
            alert("Please Select Tier 1");
            return false;
        }
	if (f.type.value == "") {
            alert("Please Select Category1 Tier 2");
            return false;
        }
        if(f.specific_vertical2.value !=""){
            if (f.type2.value=="") {
                alert("Please select Category2 - Tier2");
                return false;
            }
        }*/
        return true;
}
$( document ).ready(function() {
    var type = window.location.hash.substr(1);
    if(type){
        $("#manual_tab").addClass("active");
    }
});
function toggleFunction(){
    var x = document.getElementById("toggleThread");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>
