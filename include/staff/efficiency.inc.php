<?php
	echo "<h2>Efficiency Reports</h2>";
?>
<script>
$(document).ready(function () {
	$("#from").datepicker();
	$("#to").datepicker();
});
</script>
<form action="efficiency.php" method="post">
	<?php csrf_token(); ?>
From : <input type="text" name="from" id="from" class="dp" value="<?php echo ($_POST['from'])?date('Y-m-d',strtotime($_POST['from'])):date('Y-m-01',strtotime('this month')); ?>">
To : <input type="text" name="to" id="to" class="dp" value="<?php echo ($_POST['to'])?date('Y-m-d',strtotime($_POST['to'])):date('Y-m-t',strtotime('this month')); ?>">
<input type="submit" name="submit" value="Submit">
</form>
<?php
$from=($_POST['from'])?date('Y-m-d',strtotime($_POST['from'])):date('Y-m-01',strtotime('this month'));
$to=($_POST['to'])?date('Y-m-d',strtotime($_POST['to'])):date('Y-m-t',strtotime('this month'));
$url='http://34.233.162.0/CreativeScan/theorem/staffEfficency?fromDate='.$from."&toDate=".$to;

$test = file_get_contents($url);

$response = json_decode($test,true);
//print_r($response);//exit;
echo "<table class=list><tr><th>Staff Name</th><th>Time Spent</th><th>Total Creatives Worked</th><th>Avg. Creative Per hour</th><th>Total Go/No-Go</th></tr>";
foreach ($response as $key => $value) {
			echo "<tr><td>$value[staffName]</td><td>$value[timeSpent]</td><td>$value[creativesWorked]</td><td>$value[avgCreative]</td><td>$value[go] / $value[noGo]</td></tr>";	
}
echo "</table>";
?>
