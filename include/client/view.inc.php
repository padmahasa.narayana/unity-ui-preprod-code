<?php
if(!defined('OSTCLIENTINC') || !$thisclient || !$ticket || !$ticket->checkUserAccess($thisclient)) die('Access Denied!');

$info=($_POST && $errors)?Format::htmlchars($_POST):array();

$dept = $ticket->getDept();

$manualTicket = $ticket->checkManualTicket();

if ($ticket->isClosed() && !$ticket->isReopenable())
    $warn = __('This ticket is marked as closed and cannot be reopened.');

//Making sure we don't leak out internal dept names
if(!$dept || !$dept->isPublic())
    $dept = $cfg->getDefaultDept();

if ($thisclient && $thisclient->isGuest()
    && $cfg->isClientRegistrationEnabled()) { ?>

<div id="msg_info">
    <i class="icon-compass icon-2x pull-left"></i>
    <strong><?php echo __('Looking for your other tickets?'); ?></strong></br>
    <a href="<?php echo ROOT_PATH; ?>login.php?e=<?php
        echo urlencode($thisclient->getEmail());
    ?>" style="text-decoration:underline"><?php echo __('Sign In'); ?></a>
    <?php echo sprintf(__('or %s register for an account %s for the best experience on our help desk.'),
        '<a href="account.php?do=create" style="text-decoration:underline">','</a>'); ?>
    </div>

<?php } ?>

<table width="800" cellpadding="1" cellspacing="0" border="0" id="ticketInfo">
    <tr>
        <td colspan="2" width="100%">
            <h1>
                <?php echo sprintf(__('Ticket #%s'), $ticket->getNumber()); ?> &nbsp;
                <a href="tickets.php?id=<?php echo $ticket->getId(); ?>" title="Reload"><span class="Icon refresh">&nbsp;</span></a>
<?php if ($cfg->allowClientUpdates()
        // Only ticket owners can edit the ticket details (and other forms)
        && $thisclient->getId() == $ticket->getUserId()) { ?>
                <a class="action-button pull-right" href="tickets.php?a=edit&id=<?php
                     echo $ticket->getId(); ?>"><i class="icon-edit"></i> Edit</a>
<?php } ?>
            </h1>
        </td>
    </tr>
    <tr>
        <td width="50%">
            <table class="infoTable" cellspacing="1" cellpadding="3" width="100%" border="0">
                <tr>
                    <th width="100"><?php echo __('Ticket Status');?>:</th>
                    <td><?php echo $ticket->getStatus(); ?></td>
                </tr>
                <?php if(!$manualTicket){ ?>
                <tr>
                    <th><?php echo __('Resubmitted');?>:</th>
                    <td><?php echo ($ticket->getResubmitted())?'Yes':'No'; ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Re-Submitted Date');?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getResubmitted()); ?></td>
                </tr>
                
                <tr>
                    <th><?php echo __('Final Decision');?>:</th>
                    <td><?php echo $ticket->getFinalDecision();?></td>
                </tr>
                <?php }else{ ?>
                    <tr>
                        <th><?php echo __('Final Decision');?>:</th>
                        <td><?php echo $ticket->getManualScanDecision(null,$ticket->getId());?></td>
                    </tr>
                <?php } ?>
                    <!-- Added app name -->
                 <?php $app_name = $ticket->getUsersideAppName(); 
                    if($app_name){
                ?>
                <!--end for language display in staff side-->
                <tr>
                    <th><?php echo __('App Name'); ?>:</th>
                    <td>
                    <?php echo ucfirst($app_name); ?>
                    </td>
                </tr>
                    <?php } ?>
                    <!-- App name end -->
           </table>
       </td>
       <td width="50%">
           <table class="infoTable" cellspacing="1" cellpadding="3" width="100%" border="0">
              <!--  <tr>
                   <th width="100"><?php echo __('Name');?>:</th>
                   <td><?php echo mb_convert_case(Format::htmlchars($ticket->getName()), MB_CASE_TITLE); ?></td>
               </tr>
               <tr>
                   <th width="100"><?php echo __('Email');?>:</th>
                   <td><?php echo Format::htmlchars($ticket->getEmail()); ?></td>
               </tr>
               <tr>
                   <th><?php echo __('Phone');?>:</th>
                   <td><?php echo $ticket->getPhoneNumber(); ?></td>
               </tr> -->
               <tr>
                    <th><?php echo __('Submitted Date');?>:</th>
                    <td><?php echo Format::db_datetime(($ticket->getSubmitDate())?:$ticket->getCreateDate()); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Closed Date');?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getCloseDate()); ?></td>
                </tr>
                <!--added for language display in user side-->

                <?php $language = $ticket->getUsersideLanguage(); 
                    if($language){
                ?>
                <tr>
                    <th><?php echo __('Language'); ?>:</th>
                    <td>
                    <?php echo ucfirst($language); ?>
                    </td>
                </tr>
                    <?php } ?>

                 <?php $app_store = $ticket->getUsersideAppStore(); 
                    if($app_store){
                ?>
                <!--end for language display in staff side-->
                <tr>
                    <th><?php echo __('App Store'); ?>:</th>
                    <td>
                    <?php echo ucfirst($app_store); ?>
                    </td>
                </tr>
                    <?php } ?>
                <!--end for language display in user side-->
                
               <!-- <tr>
                   <th><?php echo __('Severity');?>:</th>
                   <td><?php echo 'P2' ?></td>
               </tr> -->
                 <?php if(!$manualTicket){ ?>
               <?php if($ticket->isClosed()){ ?>
                   <tr>
                       <th><?php echo __('Resolution Time');?>:</th>
                       <td><?php echo round((strtotime($ticket->getCloseDate()) - strtotime(($ticket->getSubmitDate())?:$ticket->getResubmitted()))/3600,2)." Hrs"; ?></td>
                   </tr>
               <?php }} ?>

            </table>
       </td>
    </tr>
    <tr>
<?php
foreach (DynamicFormEntry::forTicket($ticket->getId()) as $idx=>$form) {
    $answers = $form->getAnswers();
    if ($idx > 0 and $idx % 2 == 0) { ?>
        </tr><tr>
    <?php } ?>
    <td width="50%">
        <table class="infoTable" cellspacing="1" cellpadding="3" width="100%" border="0">
    <?php foreach ($answers as $answer) {
        if (in_array($answer->getField()->get('name'), array('name', 'email', 'subject')))
            continue;
        elseif ($answer->getField()->get('private'))
            continue;
        ?>
        <tr>
        <th width="100"><?php echo $answer->getField()->get('label');
            ?>:</th>
        <td><?php echo $answer->display(); ?></td>
        </tr>
    <?php } ?>
    </table></td>
<?php } ?>
</tr>
</table>
<br>
<div class="subject"><?php echo __('Creative Id'); ?>: <strong><?php echo Format::htmlchars($ticket->getSubject()); ?></strong></div>
<div id="response_options">
    <ul class="tabs">
        <li><a id="toggle_ticket_thread" href="#ticket-thread"><?php echo 'Ticket Thread'; ?></a></li>
        <li><a id="toggle_error_threads" href="#errorthread"><?php echo 'Scan Results'; ?></a></li>

    </ul>
    <form id="ticket-thread" name="ticket-thread" action="">
        <div id="ticketThread">
<?php
if($ticket->getThreadCount() && ($thread=$ticket->getClientThread())) {
    $threadType=array('M' => 'message', 'R' => 'response');
    foreach($thread as $entry) {

        //Making sure internal notes are not displayed due to backend MISTAKES!
        if(!$threadType[$entry['thread_type']]) continue;
        $poster = $entry['poster'];
        if($entry['thread_type']=='R' && ($cfg->hideStaffName() || !$entry['staff_id']))
            $poster = ' ';
        ?>
        <table class="thread-entry <?php echo $threadType[$entry['thread_type']]; ?>" cellspacing="0" cellpadding="1" width="100%" border="0">
            <tr><th><div>
<?php echo Format::db_datetime($entry['created']); ?>
                &nbsp;&nbsp;<span class="textra"></span>
                <span><?php echo $poster; ?></span>
            </div>
            </th></tr>
            <tr><td class="thread-body"><div><?php if((strpos($entry['body'], '<script') !== false) || (($threadType[$entry['thread_type']])=='message') && !$manualTicket){
                    echo htmlspecialchars($entry['body']); 
                }
                else{
                    echo $entry['body']->toHtml();
                } ?></div></td></tr>
            <?php
            if($entry['attachments']
                    && ($tentry=$ticket->getThreadEntry($entry['id']))
                    && ($urls = $tentry->getAttachmentUrls())
                    && ($links=$tentry->getAttachmentsLinks())) { ?>
                <tr><td class="info"><?php echo $links; ?></td></tr>
<?php       }
            if ($urls) { ?>
                <script type="text/javascript">
                    $(function() { showImagesInline(<?php echo
                        JsonDataEncoder::encode($urls); ?>); });
                </script>
<?php       } ?>
        </table>
    <?php
    }
}
?>
</div>
</form>
<form id="errorthread" action = "" name = "errorthread" >
    <div id="ticket_thread">
        <!-- <h4>Auto Scan Values</h4>
        <h5>User Security:</h5>
        <span>There is no issues found in User security</span>
        <h5>Creative and Advertiser Quality</h5>
        <table id="ticketTable" class="list" style="width:100%" border="0" cellspacing="2" cellpadding="3">
            <tr>
                <td>Offensive Content</td>
                <td>This may include: partial or full nudity and/or offensive keywords</td>
                <td>CAQ001</td>
                <td>No-Go</td>
            </tr>
            <tr>
                <td>Pop-up</td>
                <td>Pop-up banner/landing page over the user’s current window</td>
                <td>CAQ005</td>
                <td>No-Go</td>
            </tr>
        </table>
        <h5>Operational Verifications</h5>
        <table id="ticketTable" class="list" style="width:100%" border="0" cellspacing="1" cellpadding="3">
            <tr>
                <td>Creative File Size</td>
                <td>Max creative size: (Creative assets only) > 5 mb</td>
                <td>OV001</td>
                <td>No-Go</td>
            </tr>
            <tr>
                <td>1x1 Image</td>
                <td>creative only containing a blank image or a 1x1 pixel image is served (blanks)</td>
                <td>OV007</td>
                <td>No-Go</td>
            </tr>
        </table> -->
        <?php if(!$manualTicket){ ?>
        <h4>Auto Scan Values</h4>
        <table id="ticketTable" class="list" style="width:100%" border="0" cellspacing="1" cellpadding="3">
            <tr>
                <th>Category</th>
                <th>Sub Category</th>
                <th>Scan Type</th>
                <th>Code</th>
            </tr>
        <?php $autocheck=$ticket->getAutocheckValues();
         if($autocheck!=null){ 
        foreach ($autocheck as $key => $value) {
            if($value==null) continue;
            if(strpos($value, 'M') !== false) continue;
            $scan_check = $ticket->getAutoScanCheck($value); ?>
            <tr>
                <td><?php echo $scan_check[0]; ?></td>
                <td><?php echo $scan_check[1]; ?></td>
                <td><?php echo $scan_check[2]; ?></td>
                <td><?php echo $scan_check[3]; ?></td>
            </tr>

        <?php }}else { ?>
            <tr><td colspan="5" align="center"><?php echo "There is no Error in Creative to Display";?></td></tr>
            <?php } ?>
        </table>
        <hr>
        <?php
            //$manual_values = $ticket->getManualScanValues();
            $responseValue = $ticket->getResponse();
            $manualValue = $ticket->getScanValues();
        ?>
        <h4>Manual Scan Values</h4>
        <table id="ticketTable" class="list" style="width:100%" border="0" cellspacing="1" cellpadding="3">
        <tr>
            <th>Category</th>
            <th>Scan Type</th>
            <th>Decision</th>
        </tr>
        <?php if($responseValue){ ?>
            <tr>
                <td>Display</td>
                <td>Is Creative Responsive</td>
                <td><?php echo ($responseValue['is_ad_interactive']==1)?'Yes':'No'; ?></td>
            </tr>
            
       <?php $scan = $ticket->getScanChecks();
                foreach ($scan as $key => $value) { 
                    if((in_array($value[0],$manualValue))){?>
                <tr>
                    <?php
                    foreach ($value as $key => $val) { 
                            if($key == 0)
                                continue;?>
                          <td><?php echo ((in_array($value[0],$manualValue)))?$val:''; ?></td>                     
                    <?php } ?>
                    <td><?php echo 'No-Go'; ?></td>
                </tr>
              <?php  }}
            ?>
            <tr>
                <td>Creative Asset</td>
                <td>Creative has iFrame</td>
                <td><?php echo ($responseValue['is_iframe']==1)?'Yes':'No'; ?></td>
            </tr>
        </table>
        <table>
            
            <tr>
                <td>Creative Information: </td>
            </tr>
            <tr>
                <td></td><td>Language : </td><td><?php echo $responseValue['scan_language']; ?> </td>
            </tr>
            <tr>
                <td></td>
                <td>Object Errors : </td><td><?php echo $responseValue['object_error']; ?></td>
            </tr>
            <tr>
                <td></td>
                <td>Category1 Tier 1 : </td><td><?php echo $responseValue['category1_scan_vertical']; ?></td>
            </tr>
            <tr>
                <td></td>
                <td>Category1 Tier 2 : </td><td><?php echo $responseValue['category1_scan_sub_vertical']; ?></td>
            </tr>
            <tr>
                <td></td>
                <td>Category2 Tier 1 : </td><td><?php echo $responseValue['category2_scan_vertical']; ?></td>
            </tr>
            <tr>
                <td></td>
                <td>Category2 Tier 2 : </td><td><?php echo $responseValue['category2_scan_sub_vertical']; ?></td>
            </tr>

            <tr>
                <td width="145px;">Operational Verification: </td>
            </tr>
            <tr>
                <td></td><td width="180px;">Creative Initial File Size : </td><td><?php echo ($responseValue['creative_initial_file_size'])?$responseValue['creative_initial_file_size']." Kb":"NA"; ?></td>
            </tr>
            <tr>
                <td></td>
                <td> Creative File Size : </td><td><?php echo ($responseValue['creative_file_size'])?$responseValue['creative_file_size']." Kb":"NA"; ?></td>
            </tr>
            <tr>
                <td></td>
                <td>Creative Ad Load Time : </td><td><?php echo ($responseValue['ad_load_time'])?$responseValue['ad_load_time']." Secs":"NA"; ?></td>
            </tr>
            <tr>
                <td></td>
                <td>Creative Max Video Length : </td><td><?php echo ($responseValue['video_max_length'])?$responseValue['video_max_length']." Secs":"NA"; ?></td>
            </tr>
            <tr>
                <td></td>
                <td>Creative Video Frame Rate : </td><td><?php echo ($responseValue['video_frame_rate'])?$responseValue['video_frame_rate']." fps":"NA"; ?></td>
            </tr>
            <tr>
                <td></td>
                <td>Creative Video Bit Rate : </td><td><?php echo ($responseValue['video_bit_rate'])?$responseValue['video_bit_rate']." kbps":"NA"; ?></td>
            </tr>
            <!-- <tr>
                <td></td>
                <td>Creative Vast Video Bit Rate : </td><td><?php echo ($responseValue['vast_video_bitrate'])?$responseValue['vast_video_bitrate']." kbps":"NA"; ?></td>
            </tr> -->
            <!-- <tr>
                <td></td>
                <td>Creative Vast File Size : </td><td><?php echo ($responseValue['vast_file_size'])?$responseValue['vast_file_size']." Kb":"NA"; ?></td>
            </tr>
            <tr>
                <td></td>
                <td>Creative Vast Load From Start:</td><td> <?php echo ($responseValue['vast_load_time'])?$responseValue['vast_load_time']." Secs":"NA"; ?></td>
            </tr> -->
            <!-- <tr>
                <td></td>
                <td>Creative Sound Play :</td><td> <?php echo ($responseValue['autoplay_sound'])?$responseValue['autoplay_sound']:"NA"; ?></td>
            </tr> -->
        <?php } 
        else { ?>
            <tr><td colspan="5" align="center"><?php echo "There is no Error in Creative to Display";?></td></tr>
            <?php } ?>
        </table>
        <?php }else{ 
            $manualAlerts = $ticket->getManualScanValues();
            ?>
            <table id="ticketTable" class="list" style="width:100%" border="0" cellspacing="1" cellpadding="3">
                <tr>
                    <th>Category</th>
                        <th>Scan Type</th>
                        <th>Decision</th>
                        <th>Comments</th>
                    </tr>
            <?php if($manualAlerts[0]!=""){ 
    
                $seq_id = $ticket->getSeqId();
                $errorCodesJSON = $ticket->getComments($seq_id);
                
                 foreach ($manualAlerts as $key => $value) {
                    $details = $ticket->getCodeDetails($value); ?>
                    <tr>
                        <td><?php echo $details[0]; ?></td>
                        <td><?php echo $details[1]; ?></td>
                        <td><?php echo 'No-Go' ?></td>
                        <td><?php echo $errorCodesJSON[$value] ?></td>
                </tr>
            <?php }}?>
        </table>
       <?php } ?>
    </div>
</form>
<div class="clear" style="padding-bottom:10px;"></div>
<?php if($errors['err']) { ?>
    <div id="msg_error"><?php echo $errors['err']; ?></div>
<?php }elseif($msg) { ?>
    <div id="msg_notice"><?php echo $msg; ?></div>
<?php }elseif($warn) { ?>
    <div id="msg_warning"><?php echo $warn; ?></div>
<?php } ?>

<?php

if (!$ticket->isClosed() || $ticket->isReopenable()) { ?>
<form id="reply" action="tickets.php?id=<?php echo $ticket->getId(); ?>#reply" name="reply" method="post" enctype="multipart/form-data">
    <?php csrf_token(); ?>
    <h2><?php echo __('Client Feedback');?></h2>
    <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
    <input type="hidden" name="a" value="reply">
    <table border="0" cellspacing="0" cellpadding="3" style="width:100%">
        <tr>
            <td colspan="2">
                <?php
                if($ticket->isClosed()) {
                    $msg='<b>'.__('Ticket will be reopened on message post').'</b>';
                } else {
                    $msg=__('To best assist you, we request that you be specific and detailed');
                }
                ?>
                <span id="msg"><em><?php echo $msg; ?> </em></span><font class="error">*&nbsp;<?php echo $errors['message']; ?></font>
                <br/>
                <textarea name="message" id="message" cols="50" rows="9" wrap="soft"
                    data-draft-namespace="ticket.client"
                    data-draft-object-id="<?php echo $ticket->getId(); ?>"
                    class="richtext ifhtml draft"><?php echo $info['message']; ?></textarea>
        <?php
        if ($messageField->isAttachmentsEnabled()) { ?>
<?php
            print $attachments->render(true);
            print $attachments->getForm()->getMedia();
?>
        <?php
        } ?>
            </td>
        </tr>
    </table>
    <p style="padding-left:165px;">
        <input type="submit" value="<?php echo __('Submit');?>">
        <input type="reset" value="<?php echo __('Reset');?>">
        <input type="button" value="<?php echo __('Cancel');?>" onClick="window.location=''">
    </p>
</form>
<?php
} ?>

<script>
    $(document).ready(function () {
        $("#errorthread").hide();
        $("#toggle_ticket_thread").addClass('active');
    });
    $("#toggle_ticket_thread").on('click', function (event) {
        event.preventDefault();
        $("#errorthread").hide();
        $("#ticket-thread").show();
        $("#toggle_ticket_thread").addClass('active');
        $("#toggle_error_threads").removeClass('active');
    });
    $("#toggle_error_threads").on('click', function (event) {
        event.preventDefault();
        $("#ticket-thread").hide();
        $("#errorthread").show();
        $("#toggle_error_threads").addClass('active');
        $("#toggle_ticket_thread").removeClass('active');
    });
</script>
