<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Unity</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <!-- Favicons -->
    <link href="images/favicon.png" rel="icon">
    <link href="images/apple-touch-icon.png" rel="apple-touch-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">
    <!-- Bootstrap CSS File -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Libraries CSS Files -->
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="lib/magnific-popup/magnific-popup.css" rel="stylesheet">
    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet">
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <!-- Date picker script -->
    <script src="js/daterangepicker/jquery.min.js"></script>
    <script src="js/daterangepicker/moment.min.js"></script>
    <script src="js/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/daterangepicker/daterangepicker.css" />
    <script src="js/dashboard.js"></script> 

    <style type="text/css">
      #nav {
        height: 25px;
        font-size: 13px;
      }

      #report th {
        padding-left: 3px;
        font-weight: normal;
        text-align: left;
      }

      #report th.required,
      #report td.required {
        font-weight: bold;
        text-align: left;
      }

      #report {
        border: 1px solid #aaa;
        border-left: none;
        border-bottom: none;
      }

      #report caption {
        padding: 5px;
        text-align: left;
        color: #000;
        background: #ddd;
        border: 1px solid #aaa;
        border-bottom: none;
        font-weight: bold;
      }

      #report th {
        height: 24px;
        line-height: 24px;
        background: #e1f2ff;
        border: 1px solid #aaa;
        border-right: none;
        border-top: none;
      }

      #report th a {
        color: #000;
      }

      #report td {
        padding: 2px;
        border: 1px solid #aaa;
        border-right: none;
        border-top: none;
      }

      #report tr.alt td {
        background: #f9f9f9;
      }
    </style>
      
  </head>
  <body>
    
    <main id="main">
      <!--==========================
        Product Advanced Featuress Section
        ============================-->
      <section id="advanced-features">
        <div class="features-row section-bg M4">
          <div class="container">
            <div class="row">
              <div class="col-md-9 col-sm-12">
              </div>
              <div class="col-md-3 col-sm-12">
                <button type="button" style="cursor: pointer;" onclick="downloadDailyReport()"><i class="fa fa-file-excel-o pull-right" aria-hidden="true"> Export-Report</i></button>
              </div>
            </div><br>

            <div class="features-row-B30 section-bg">
              <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="row">
                      <div class="col-md-4 col-sm-12">
                        <b><div class="flex-item" id="flex">Daily Status Tracker</div></b>
                      </div>
                      <div class="col-md-8 col-sm-12">
                        <div class="flex-item" id="flex">
                          <div class="form-group row">
                            <div class="col-sm-5 col-form-label">
                              <b><label for="report_date" >Select Date-Range:</label></b>
                            </div>

                            <div class="col-sm-7">
                              <input style="width: 89%;" type="text" class="form-control" id="report_date" name="report_date" value="">
                            </div>
                          </div>
                      </div>
                    </div>
                   </div>
                    <div style="border-radius: 5px;" id="4" style="min-width: 310px; height: 300px; margin: 0 auto">
                      <table id="report" border="0" cellspacing="0" cellpadding="0">
                          <thead>
                              <tr>
                                  <th width="100">Date</th>
                                  <th width="100">Total No. Of Creatives Received</th>
                                  <th width="100">Total No. Of Creatives Processed</th>
                                  <th width="100">No. Of No Go</th>
                                  <th width="100">No. Of Duplicates</th>
                                  <th width="100">No. Of Failed Tickets</th>
                                  <th width="140">No. Of Creatives didn't appear in Ticketing system</th>
                                </tr>
                          </thead>
                          <tbody>
                          
                          </tbody>
                      </table>
                      <script type="text/javascript">
                        generateDailyReport(REPORT_START_DATE, REPORT_END_DATE);
                      </script>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
       
      </section>
    </main>
    
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    <script src="lib/jquery/jquery-migrate.min.js"></script> 
    <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/superfish/hoverIntent.js"></script>
    <script src="lib/superfish/superfish.min.js"></script>
    <script src="lib/magnific-popup/magnific-popup.min.js"></script>
    <script src="js/main.js"></script>
    <script src="lib/wow/wow.min.js"></script>
  </body>
</html>