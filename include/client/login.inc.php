<?php
/*********************************************************************
    index.php

    Helpdesk landing page. Please customize it to fit your needs.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('client.inc.php');
$section = 'home';
//require(CLIENTINC_DIR.'header.inc.php');

$email=Format::input($_POST['luser']?:$_GET['e']);
$passwd=Format::input($_POST['lpasswd']?:$_GET['t']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Theorem - Ticketing</title>
<link href="css/style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<form action="login.php" method="post" id="clientLogin">
 <?php csrf_token(); ?> 
    <div class="LoginOtr">
        <div class="LogoOtr">
            <img src="images/theoreminc.png" style="height: auto;max-height: 66px;max-width: 300px;"/>
        </div>
        <div class="FormOtr">
            <input id="username" placeholder="Email or Username" type="text" name="luser" size="30" value="<?php echo $email; ?>" class="LoginFormInput" />
            <input id="passwd" placeholder="Password" type="password" name="lpasswd" size="30" value="<?php echo $passwd; ?>" class="LoginFormInput" />
            <input type="submit" value="Sign in" class="LoginFormInputBtn" />
        </div>
    </div>
</form>
</body>
</html>
<?php //require(CLIENTINC_DIR.'footer.inc.php'); ?>
