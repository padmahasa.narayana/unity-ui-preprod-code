<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Unity</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <!-- Favicons -->
    <link href="images/favicon.png" rel="icon">
    <link href="images/apple-touch-icon.png" rel="apple-touch-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">
    <!-- Bootstrap CSS File -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Libraries CSS Files -->
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="lib/magnific-popup/magnific-popup.css" rel="stylesheet">
    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet">
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <!-- Date picker script -->
    <script src="js/daterangepicker/jquery.min.js"></script>
    <script src="js/daterangepicker/moment.min.js"></script>
    <script src="js/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script src="js/dashboard.js"></script> 

    <style type="text/css">
      #nav {
        height: 25px;
        font-size: 13px;
      }

      #creative_status th {
        padding-left: 3px;
        font-weight: normal;
        text-align: left;
      }

      #creative_status th.required,
      #creative_status td.required {
        font-weight: bold;
        text-align: left;
      }

      #creative_status {
        border: 1px solid #aaa;
        border-left: none;
        border-bottom: none;
      }

      #creative_status caption {
        padding: 5px;
        text-align: left;
        color: #000;
        background: #ddd;
        border: 1px solid #aaa;
        border-bottom: none;
        font-weight: bold;
      }

      #creative_status th {
        height: 24px;
        line-height: 24px;
        background: #e1f2ff;
        border: 1px solid #aaa;
        border-right: none;
        border-top: none;
        padding: 0 10px 0 10px;
      }

      #creative_status th a {
        color: #000;
      }

      #creative_status td {
        padding: 0 10px 0 10px;
        border: 1px solid #aaa;
        border-right: none;
        border-top: none;
      }

      #creative_status tr.alt td {
        background: #f9f9f9;
      }

      .my-custom-scrollbar {
        position: relative;
        max-height: 327px;
        overflow: auto;
      }
      .table-wrapper-scroll-y {
        display: block;
      }

      .highcharts-data-table table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
      }

     #dashboard_report th {
        padding-left: 3px;
        font-weight: normal;
        text-align: left;
      }

      #dashboard_report th.required,
      #dashboard_report td.required {
        font-weight: bold;
        text-align: left;
      }

      #dashboard_report {
        border: 1px solid #aaa;
        border-left: none;
        border-bottom: none;
      }

      #dashboard_report caption {
        padding: 5px;
        text-align: left;
        color: #000;
        background: #ddd;
        border: 1px solid #aaa;
        border-bottom: none;
        font-weight: bold;
      }

      #dashboard_report th {
        height: 24px;
        line-height: 0px;
        background: #e1f2ff;
        border: 1px solid #aaa;
        border-right: none;
        border-top: none;
        width: 14%;
      }

      #dashboard_report th a {
        color: #000;
      }

      #dashboard_report td {
        padding: 2px;
        border: 1px solid #aaa;
        border-right: none;
        border-top: none;
      }

      #dashboard_report tr.alt td {
        background: #f9f9f9;
      }

       /*monthly_breakup*/
      #monthly_breakup th {
        padding-left: 3px;
        font-weight: normal;
        text-align: left;
      }
 
      #monthly_breakup th.required,
      #monthly_breakup td.required {
        font-weight: bold;
        text-align: left;
      }
 
      #monthly_breakup {
        border: 1px solid #aaa;
        border-left: none;
        border-bottom: none;
      }
 
      #monthly_breakup caption {
        padding: 5px;
        text-align: left;
        color: #000;
        background: #ddd;
        border: 1px solid #aaa;
        border-bottom: none;
        font-weight: bold;
      }
 
      #monthly_breakup th {
        height: 24px;
        line-height: 0px;
        background: #e1f2ff;
        border: 1px solid #aaa;
        border-right: none;
        border-top: none;
        width: 14%;
      }
 
      #monthly_breakup th a {
        color: #000;
      }
 
      #monthly_breakup td {
        padding: 2px;
        border: 1px solid #aaa;
        border-right: none;
        border-top: none;
      }
 
      #monthly_breakup tr.alt td {
        background: #f9f9f9;
      }


      .btn-info {
          color: #111;
          background-color: #aaa;
          border-color: #aaa;
      }
      
    </style>
      
  </head>
  <body>
    
    <main id="main">
      <!--==========================
        Product Advanced Featuress Section
        ============================-->
      <section id="advanced-features">
        <div class="features-row section-bg M4">
          <div class="containers">
            <div class="row">
              <div class="col-md-7 col-sm-12">
              </div>
              <div class="col-md-5 col-sm-12">
                <div class="row">
                  <div class="col-md-12 col-sm-12">
                    <div id="flex-containers">
                      <div class="flex-item" id="flex">
                        <div class="containers">
                          <div class="form-group row">
                            <div class="col-sm-5 col-form-label">
                              <b><label for="custom_date"  data-toggle="tooltip" title="Select date range for all result!">Custom Date-Range:</label></b>
                            </div>                              
                            
                            <div class="col-sm-7">
                              <input type="text" class="form-control" id="custom_date" name="custom_date" value="">
                            </div>
                          </div>                            
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <hr>

        <div class="features-row-B30 section-bg">
          <div class="containers">
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <div class="shadow wow fadeInLeft">
                  <div class="row">
                    <div class="col-md-12 col-sm-12">
                      <div id="flex-containers">
                        <b><div class="flex-item" id="flex">HTML/VAST - Received vs Processed</div></b>
                      </div>
                    </div>
                    <div class="col-md-11 col-sm-12">
                      <div id="flex-containers">
                        <div class="flex-item" id="flex">
                          <div class="containers">
                            <div class="form-group row" style="margin-right: 13%;">
                              <div class="col-sm-6 col-form-label">
                                <b><label for="input_output_date">Select Date-Range:</label></b>
                              </div>

                              <div class="col-sm-6">
                                <input type="text" style="width: 145%;" class="form-control" id="input_output_date" name="input_output_date" value="">
                              </div>
                            </div>                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div id="1"></div>
                  
                  <script>
                    var start = moment().startOf('day').subtract(FROM_DATE, 'days');
                    var end = moment().startOf('day');
                    generateInputOutputGraph(start, end);
                  </script>
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="shadow wow fadeInLeft">
                  <div class="row">
                    <div class="col-md-12 col-sm-12">
                      <div id="flex-containers">
                        <b><div class="flex-item" id="flex">Manual - Received vs Processed</div></b>
                      </div>
                    </div>
                    <div class="col-md-11 col-sm-12">
                      <div id="flex-containers">
                        <div class="flex-item" id="flex">
                          <div class="containers">
                            <div class="form-group row" style="margin-right: 13%;">
                              <div class="col-sm-6 col-form-label">
                                <b><label for="monthly_report">Select Date-Range:</label></b>
                              </div>

                              <div class="col-sm-6">
                                <input type="text" style="width: 145%;" class="form-control" id="monthly_report" name="monthly_report" value="">
                              </div>
                            </div>                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div id="22"></div>
                  
                  <script>
                    var start = moment().startOf('day').subtract(FROM_DATE, 'days');
                    var end = moment().startOf('day');
                    generateMonthlyReport(start, end);
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr>

        <div class="features-row-B30 section-bg">
          <div class="containers">
            <div class="row">
              <!-- <div class="col-md-6 col-sm-12">
                <div class="shadow wow fadeInLeft">
                  <div class="row">
                    <div class="col-md-4 col-sm-12">
                      <div id="flex-containers">
                        <b><div class="flex-item" id="flex">Manual Tickets</div></b>
                      </div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                      <div id="flex-containers">
                        <div class="flex-item" id="flex">
                          <div class="containers">
                            <div class="form-group row" style="margin-right: 13%;">
                              <div class="col-sm-6 col-form-label">
                                <b><label for="monthly_report" >Select Date-Range:</label></b>
                              </div>

                              <div class="col-sm-6">
                                <input type="text" style="width: 145%;" class="form-control" id="monthly_report" name="monthly_report" value="">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="33" ></div>
                  <table id="datatable" style="display: none" ></table>
                  <script type="text/javascript">
                    var start = moment().startOf('day').subtract(FROM_DATE, 'days');
                    var end = moment().startOf('day');
                    generateMonthlyBarChartTable(start, end);
                  </script>
                </div>
              </div> -->
              <div class="col-md-6 col-sm-12">
                <div class="shadow wow fadeInLeft">
                  <div class="row">
                    <div class="col-md-4 col-sm-12">
                      <div id="flex-containers">
                        <b><div class="flex-item" id="flex">Go vs No-Go</div></b>
                      </div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                      <div id="flex-containers">
                        <div class="flex-item" id="flex">
                          <div class="containers">
                            <div class="form-group row" style="margin-right: 13%;">
                              <div class="col-sm-6 col-form-label">
                                <b><label for="approval_date" >Select Date-Range:</label></b>
                              </div>

                              <div class="col-sm-6">
                                <input type="text" style="width: 145%;" class="form-control" id="approval_date" name="approval_date" value="">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="3" ></div>
                  <table id="datatable" style="display: none" ></table>
                  <script type="text/javascript">
                    var start = moment().startOf('day').subtract(FROM_DATE, 'days');
                    var end = moment().startOf('day');
                    generateBarChartTable(start, end);
                  </script>
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="shadow wow fadeInRight">
                  <div class="row">
                    <div class="col-md-4 col-sm-12">
                      <div id="flex-containers">
                        <b><div class="flex-item" id="flex">No-Go Reasons</div></b>
                      </div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                      <div id="flex-containers">
                        <div class="flex-item" id="flex">
                          <div class="containers">
                            <div class="form-group row" style="margin-right: 13%;">
                              <div class="col-sm-6 col-form-label">
                                <b><label for="rejection_date">Select Date-Range:</label></b>
                              </div>
                              
                              <div class="col-sm-6">
                                <input type="text" style="width: 145%;" class="form-control" id="rejection_date" name="rejection_date" value="">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <div id="2" ></div>
                  <script>
                    // Radialize the colors
                    var start = moment().startOf('day').subtract(FROM_DATE, 'days');
                    var end = moment().startOf('day');
                    generateRejectionReasonGraph(start, end);
                     
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr>
        
        <div class="features-row-B60 section-bg">
          <div class="containers">
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <div id="flex-containers">
                        <b><div class="flex-item" id="flex">Monthly Playable Processed Breakup</div></b>
                      </div>
                      
                <div class="shadow wow fadeInRight">
                  <div class="row">
                    <div class="col-md-6 col-sm-12">

                      <div id="flex-containers">
                        <!-- <b><label for="searchInput">Search:</label></b>
                        <input type="text" size="25%"  id="searchInput" onkeyup="search()" placeholder="Creative ID" style="height: 25px;border-radius: 2px;"> -->
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div id="flex-containers pull-right">
                        <!-- <div class="dropdown pull-right"  id=""><b>Tickets Status:</b> 
                          <select id="monthly_breakup_option" style="height: 25px;">
                            <option value="1">Open Queue</option>
                            <option value="2">Closed</option>
                            <option value="3">Work in Progress</option>
                          </select>
                        </div> -->
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="table-wrapper-scroll-y my-custom-scrollbar" style="margin-top: 1.5%;">
                    <table class="table table-bordered table-striped mb-0 currentStatus" id="monthly_breakup">
                      <thead style="background: #7372720d;">
                        <tr>
                          <th scope="col"><b>Month</b></th>
                          <th scope="col" style="padding-top:0px; text-align:center;"><b>Count</b></th>
                        </tr>
                      </thead>
                      <tbody class="scroll">
                      </tbody>
                    </table>

                    <script type="text/javascript">
                      //var status = $("#monthly_breakup_option option:selected").val();
                      generateMonthlyBreakupTable();

                      /*$('#monthly_breakup_option').on('change',function(){
                          var status = $("#monthly_breakup_option option:selected").val();
                          generateCreativeStatusTable(status);
                      });*/
                    </script>
                  </div>
                  <script></script>
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div id="flex-containers">
                        <b><div class="flex-item" id="flex">Playable Creative Status</div></b>
                      </div>
                      
                <div class="shadow wow fadeInRight">
                  <div class="row">
                    <div class="col-md-6 col-sm-12">

                      <div id="flex-containers">
                        <b><label for="searchInput">Search:</label></b>
                        <input type="text" size="25%"  id="searchInput" onkeyup="search()" placeholder="Creative ID" style="height: 25px;border-radius: 2px;">
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div id="flex-containers pull-right">
                        <div class="dropdown pull-right"  id=""><b>Tickets Status:</b> 
                          <select id="creative_status_option" style="height: 25px;">
                            <option value="1">Open Queue</option>
                            <option value="2">Closed</option>
                            <option value="3">Work in Progress</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-bordered table-striped mb-0 currentStatus" id="creative_status">
                      <thead style="background: #7372720d;">
                        <tr>
                          <th scope="col"><b>Creative ID</b></th>
                          <th scope="col" style="padding-top:0px; text-align:right;"><b>Status</b></th>
                        </tr>
                      </thead>
                      <tbody class="scroll">
                      </tbody>
                    </table>

                    <script type="text/javascript">
                      var status = $("#creative_status_option option:selected").val();
                      generateCreativeStatusTable(status);

                      $('#creative_status_option').on('change',function(){
                          var status = $("#creative_status_option option:selected").val();
                          generateCreativeStatusTable(status);
                      });
                    </script>
                  </div>
                  <script></script>
                </div>
              </div>
             
              
            </div>
          </div>
        </div>

        <!-- <hr>
        <div class="features-row-B60 section-bg">
          <div class="containers">
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div id="flex-containers">
                        <b><div class="flex-item" id="flex">Daily Status Tracker</div></b>
                      </div>
                      <br>
                <div class="shadow wow fadeInRight">
                  <div class="row">
                    <div class="col-md-3 col-sm-12">

                      <div id="flex-containers">
                        <button type="button" onclick="downloadDailyReport()"><i class="fa fa-file-excel-o pull-right" aria-hidden="true"> Export-Report</i></button>
                      </div>
                    </div>
                    <div class="col-md-9 col-sm-12">
                      <div id="flex-containers pull-right">
                        <div class="row">
                          <div class="col-md-5 col-sm-12">
                            <b style="margin-left: 12%;"">Select Date:</b>
                          </div>
                          <div class="col-md-7 col-sm-12">
                            <input style="height: 25px;margin-right: 2%;"  type="text" class="form-control" id="dashboard_report_date" name="dashboard_report_date" value="">
                          </div>
                          
                        </div>
                       
                      </div>
                    </div>
                  </div>

                  <div class="table-wrapper-scroll-y my-custom-scrollbar" id="4" >
                    <table class="table table-bordered table-striped mb-0 currentStatus" id="dashboard_report">
                        <thead style="background: #7372720d;">
                            <tr>
                                <th scope="col" style="width: 16%;"><b>Date</b></th>
                                <th scope="col"><b>Input</b></th>
                                <th scope="col"><b>Output</b></th>
                                <th scope="col"><b>No Go</b></th>
                                <th scope="col"><b>Duplicates</b></th>
                                <th scope="col" style="width: 16%;"><b>Failed</b></th>
                                <th scope="col"><b>Missed</b></th>
                            </tr>
                        </thead>
                        <tbody class="scroll">
                        
                        </tbody>
                    </table>
                    <script type="text/javascript">
                      var start = moment().startOf('day').subtract(FROM_DATE, 'days');
                      var end = moment().startOf('day');
                      generateDailyReport(start, end);
                    </script>
                  </div>
                </div>
              </div>            
              
            </div>
          </div>
        </div> -->
      </section>
      <!-- #advanced-features -->
    </main>
    
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    <!-- JavaScript Libraries -->
    <script src="lib/jquery/jquery-migrate.min.js"></script> 
    <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/wow/wow.min.js"></script>
    <script src="lib/superfish/hoverIntent.js"></script>
    <script src="lib/superfish/superfish.min.js"></script>
    <script src="lib/magnific-popup/magnific-popup.min.js"></script>
    <script src="js/main.js"></script>    

  </body>
</html>