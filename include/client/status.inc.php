<html>
	<body>
		<div style="color: red;margin-top: 2%;"><?php echo $error; ?></div>
		<h1>Upload the csv file with Creative Id's to get the status</h1>
		<form action="status.php" method="post" enctype="multipart/form-data" style="margin-top: 2%;">
			<?php csrf_token(); ?>
			Choose a file : <input type="file" name="creatives" accept=".csv">
			<input type="submit" name="submit" value="Upload">
		</form>
		<?php
			if($download){ 
				echo "<div style='float:right;margin:5px;font-weight:bold'>".$download."</div>";
			}
		?>
		<?php if($tableData){ ?>
			<table id="ticketTable" width="100%" style="margin-top: 2%;">
				<thead>
					<tr>
						<th>Creative ID</th>
						<th>Ticket #</th>
						<th>Tag Scan Failed</th>
						<th>Agent Assigned</th>
						<th>Final Decision</th>
						<th>Error Codes</th>
						<th>Sent to Unity</th>
					</tr>
				</thead>
				<tbody>
					<?php echo $tableData;  ?>
				</tbody>
			</table>
		<?php } ?>
	</body>
</html>
