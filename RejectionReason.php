<?php 
// include DB connection
//include('generalconfig.php');
include('unity_dashboard_reporting.php');
// check start and end date in post request
if($_POST && isset($_POST['start']) && isset($_POST['end'])){
	$start = $_POST['start'];
	$end = $_POST['end'];
	// sql statement to retrive rejection reason data
	$sql = "SELECT SUM(E.creative_count) AS creative_count, 
			E.scan_response_dashboard FROM	
				(SELECT C.scan_code, C.scan_response_dashboard, D.creative_count 
				FROM
					(SELECT scan_code, scan_response_dashboard
					FROM scan_code_details) AS C
				RIGHT JOIN
					(SELECT COUNT(B.creative_id) creative_count, B.scan_error_code 
					FROM	      
						(SELECT
						  A.creative_id,
						  SUBSTRING_INDEX(SUBSTRING_INDEX(A.scan_error_code, ',', numbers.n), ',', -1) scan_error_code
						FROM
						    (SELECT 1 n UNION ALL
						     SELECT 2 UNION ALL 
						     SELECT 3 UNION ALL
						     SELECT 4 UNION ALL 
						     SELECT 5 UNION ALL 
						     SELECT 6 UNION ALL 
						     SELECT 7 UNION ALL 
						     SELECT 8 UNION ALL 
						     SELECT 9) numbers 
						INNER JOIN 
							(SELECT scan_error_code, creative_id 
							FROM creative_scan_response 
							WHERE creative_id IN 
							      (SELECT creative_id 
									 FROM creative_scan_decision 
									 WHERE status='Sent' AND scan_decision='No-Go'
									 AND CAST(response_date AS DATE) 
									 BETWEEN '$start' AND '$end'
							      )
							) AS A
						ON CHAR_LENGTH(A.scan_error_code)
						     -CHAR_LENGTH(REPLACE(A.scan_error_code, ',', ''))>=numbers.n-1
						ORDER BY
						  creative_id, n) AS B 
					GROUP BY scan_error_code) AS D
				ON C.scan_code=D.scan_error_code
			) AS E
			GROUP BY E.scan_response_dashboard";
	
	// execute query
	$result=$conn->query($sql);
	$response_result = array();
	// get data from result query and return response
	if ($result->num_rows > 0) {
		$response_result = array();
	    // get data from each row
	    while($input_row = $result->fetch_assoc()) {
	    	if ($input_row["scan_response_dashboard"]) {
	    		$data['name'] = $input_row["scan_response_dashboard"];
		    	$data['y'] = (int)$input_row["creative_count"];
		    	$response_result[] = $data; 
		    } 
	    }
	    
	    echo json_encode($response_result);
	} else {
		// return empty data if data is not available in DB
		$data['name'] = "No data";
		$data['y'] = 1;
		$response_result[] = $data;
		echo json_encode($response_result);
	}
	mysqli_close($conn);
}
?>
