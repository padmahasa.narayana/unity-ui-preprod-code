<?php
// include DB connection
include('generalconfig.php');
// check start and end date in post request
if($_POST && isset($_POST['start']) && isset($_POST['end'])){
	$start = $_POST['start'];
	$end = $_POST['end'];
	// sql statement to retrive input and output data
	$sql = "SELECT COALESCE(input.input_count, 0) input, 
				COALESCE(output.output_count, 0) output,
				COALESCE(rejection.rejection_count, 0) rejection,
				COALESCE(A.failed_ticket_count, 0) failed_ticket_count, 
				COALESCE(B.duplicate_creative_count, 0) duplicate_creative_count, 
				input.creative_submit_date created,
	(COALESCE(B.duplicate_creative_count,0)+COALESCE(A.failed_ticket_count,0)) AS not_appear	
			FROM
				(SELECT COUNT(creative_id) AS input_count, 
	          			CAST(creative_submit_date AS DATE) creative_submit_date 
	          	FROM creative_tag_details 
	          	WHERE CAST(creative_submit_date AS DATE) BETWEEN '$start' AND '$end'
	          		GROUP BY CAST(creative_submit_date AS DATE)
	          	) AS input
	        LEFT JOIN 
	        	(SELECT COUNT(creative_id) AS output_count, 
	          			CAST(response_date AS DATE) response_date 
	          	FROM creative_scan_decision 
	          	WHERE STATUS='sent' 
	          	AND CAST(response_date AS DATE) BETWEEN '$start' AND '$end'
	          		GROUP BY CAST(response_date AS DATE)
	          		) AS output
			ON input.creative_submit_date=output.response_date
			LEFT JOIN
			(SELECT COUNT(1) failed_ticket_count, 
					CAST(created AS DATE) created 
				FROM ost_ticket
				WHERE status_id = 6 
				AND CAST(created AS DATE) BETWEEN '$start' AND '$end'
				GROUP BY CAST(created AS DATE)) AS A
			ON input.creative_submit_date=A.created
			LEFT JOIN
				(SELECT COUNT(creative_id) duplicate_creative_count,
					CAST(creative_submit_date AS DATE) creative_submit_date
				FROM creative_tag_details
				WHERE fail_code=\"Invalid value for field 'name' - Project name already used\"
				AND CAST(creative_submit_date AS DATE) BETWEEN '$start' AND '$end'
				GROUP BY CAST(`creative_submit_date` AS DATE)) AS B
			ON input.creative_submit_date = B.creative_submit_date
			LEFT JOIN
				(SELECT COUNT(creative_id) AS rejection_count, CAST(response_date AS DATE) AS response_date
				FROM creative_scan_decision
				WHERE CAST(response_date AS DATE) BETWEEN '$start' AND '$end'
				AND STATUS='sent' AND scan_decision='No-Go'
				GROUP BY CAST(response_date AS DATE)) AS rejection
			ON input.creative_submit_date=rejection.response_date
			ORDER BY created DESC";

	// execute query
	$result=$conn->query($sql);
	$response_result = array();
	// get data from result query and return response
	if ($result->num_rows > 0) {
		$response_result = array();
	    // get data from each row
	    while($input_row = $result->fetch_assoc()) {
	    	$data['created_date'] = $input_row["created"];
	    	$data['input'] = $input_row["input"];
	    	$data['output'] = $input_row["output"];
	    	$data['rejection'] = $input_row["rejection"];
	    	$data['duplicate_creative_count'] = $input_row["duplicate_creative_count"];
	    	$data['failed_ticket_count'] = $input_row["failed_ticket_count"];
	    	$data['not_appear'] = $input_row["not_appear"];
	    	//$data['comments'] = '';
	    	$response_result[] = $data;  
	    }
	    echo json_encode($response_result);
	} else {
		echo json_encode($response_result);
	}
	mysqli_close($conn);
}
?>
