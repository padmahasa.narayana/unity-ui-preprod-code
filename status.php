<?php
	
require('secure.inc.php');
if(!is_object($thisclient) || !$thisclient->isValid()) die('Access denied'); //Double check again.

if ($thisclient->isGuest())
    $_REQUEST['id'] = $thisclient->getTicketId();

require_once(INCLUDE_DIR.'class.ticket.php');
require_once(INCLUDE_DIR.'class.json.php');
$msg = "";
$error = "";
$tableData = "";
$failed='No';
$picked='Yes';
$ticketCreated='No';
$ticketClosed='No';
$errorCode='';
$sent='No';
$exportFile=$_GET['file']?:'';
if($_POST){
	if($_FILES['creatives']['type']!="text/csv" && $_FILES['creatives']['type']!="application/vnd.ms-excel"){
		$error .= "Please upload csv file.<br>";
	}
	else{
		$target_dir = "uploads/";
		$filename = trim(addslashes($_FILES['creatives']['name']));
		$filename = preg_replace('/\s+/', '_', $filename);
		$target_file = $target_dir . basename($filename);
		$uploadOk = 1;
		$fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		
		if(isset($_POST["submit"])) {
		    if (file_exists($target_file)) {
			    $error.= "Sorry, file name already exists.";
			    $uploadOk = 0;
			}
			if ($_FILES["creatives"]["size"] > 5000000) {
			    $error.= "Sorry, your file is too large.";
			    $uploadOk = 0;
			}
			$content = file($_FILES["creatives"]["tmp_name"]);
			
			if(empty($content) || (count($content) == 1 && $content[0])){
				$error.= "Sorry, your file is empty.";
			    $uploadOk = 0;
			}
			if ($uploadOk == 0) {
			    $error.= "<br>";
			// if everything is ok, try to upload file
			} else {
				$exportRow=array();
			    if (move_uploaded_file($_FILES["creatives"]["tmp_name"], $target_file)) {
			        $msg= "The file ". basename( $_FILES["creatives"]["name"]). " has been uploaded.";
			       if (($file = fopen($target_file, "r")) !== FALSE){
					    while ($line = fgetcsv($file)){
					        $numcols = count($line);
					        if ($numcols != 1) {
					            $error.= "The number of columns in your file is not correct. The file must contain only 2 columns. ".$numcols;
					            break;
					        }
					    }
					    fclose($file);
					}
					$row = 1;
					if (($handle = fopen($target_file, "r")) !== FALSE) {
						
					  while (($data = fgetcsv($handle)) !== FALSE) {
					    $num = count($data);
					    $row++;
					    for ($c=0; $c < $num; $c++) {
					    	$tableData.="<tr>";
					        //echo $data[$c] . "<br />\n";
						$tagDetailsQuery = db_query("select cd.creative_id,cd.fail_code,t.staff_id as picked,t.number,de.scan_decision,re.scan_error_code,de.status,de.response_date from creative_tag_details cd left join ost_ticket__cdata c on(c.subject=cd.creative_id) left join ost_ticket t on(t.ticket_id=c.ticket_id) left join creative_scan_response re on(re.creative_id=cd.creative_id) left join creative_scan_decision de on(re.creative_id=de.creative_id) where cd.creative_id='".$data[$c]."'");    
						while($tags=db_fetch_array($tagDetailsQuery)){
						    	if($tags['fail_code']!=null)
						    		$failed='Yes';
						    	else
						    		$failed='No';
						    	if($tags['picked']==0)
						    		$picked='No';
						    	else
						    		$picked='Yes';
						    	if($tags['number']!=null)
						    		$ticketCreated =$tags['number'];
						    	else
						    		$ticketCreated = 'No';
						    	if($tags['scan_decision']!=null)
						    		$decision=$tags['scan_decision'];
						    	else
						    		$decision='Null';
						    	if($tags['scan_error_code']!=null)
						    		$errorCode=preg_replace('/[,]+/', ', ',trim($tags['scan_error_code'],','));
						    	else
						    		$errorCode='';
						    	if($tags['status']=='Sent' && $tags['response_date']!=null)
						    		$sent='Yes';
						    	else
						    		$sent='No';
						    	$tableData.="<td>".$tags['creative_id']."</td>";
						    	$tableData.="<td>".$ticketCreated."</td>";
						    	$tableData.='<td title="'.htmlspecialchars($tags['fail_code']).'">'.$failed.'</td>';
						    	$tableData.="<td>".$picked."</td>";
						    	$tableData.="<td>".$decision."</td>";
						    	$tableData.="<td>".$errorCode."</td>";
						    	$tableData.="<td>".$sent."</td>";
						    	array_push($exportRow,array("$tags[creative_id]","$ticketCreated","$failed","$picked","$decision","$errorCode","$sent"));
						    }
						    $tableData.="</tr>";						    		
					    }
					  }
					  fclose($handle);
					}
					$file = fopen($target_file, 'w');
					// output headers so that the file is downloaded rather than displayed
					
					$exportData=array('Creative ID', 'Ticket #', 'Tag Scan Failed', 'Agent Assigned', 'Status','Error Codes','Sent to Unity');
					fputcsv($file, $exportData);
					foreach ($exportRow as $key => $row) {
						fputcsv($file,$row);
					}
					fclose($file);
					$download = "<a href='/Unity/status.php?file=".$target_file."' target='_blank'>Download Report</a>";	
			    } else {
			        $error.= "Sorry, there was an error uploading your file.<br>";
			    }
			}  
		}
	}
}
if(isset($_GET['file'])){
	header('Content-type: text/csv');
	header("Content-Disposition: attachment; filename=$exportFile");
	 
	// do not cache the file
	header('Pragma: no-cache');
	header('Expires: 0');
	 readfile($exportFile);
        exit;
}


$nav->setActiveNav('status');
include(CLIENTINC_DIR.'header.inc.php');
include(CLIENTINC_DIR.'status.inc.php');
include(CLIENTINC_DIR.'footer.inc.php');
?>
